package com.kgict.kds;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kgict.kds.network.NetworkService;
import com.kgict.kds.network.NetworkStreamService;
import com.kgict.kds.network.UpdateService;
import com.kgict.kds.network.VersionService;
import com.kgict.kds.progress.DownloadProgressInterceptor;
import com.kgict.kds.utils.Dlog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;


/**
 * Created by Cheol on 2017-12-04.
 */

public class App extends Application {

    public static boolean DEBUG;

    private Retrofit retrofit;
    private Retrofit retrofitGson;
    private Retrofit retrofitStream;
    NetworkService networkService;
    VersionService versionService;
    UpdateService updateService;
    NetworkStreamService networkStreamService;

    SharedPreferences prefs;    // 프리퍼런스
    SharedPreferences.Editor prefEditor;

    Typeface awesomeFont;

    public boolean isDebug = false;
    Toast toast;

    public SoundPool soundPool;
    public int[] soundId = new int[1];

    public File logoFile = null;

    @Override
    public void onCreate() {
        super.onCreate();
        this.DEBUG = isDebuggable(this);

        // 프리퍼런스
        prefs = getSharedPreferences("pref", MODE_PRIVATE);
        prefEditor = prefs.edit();

        awesomeFont = Typeface.createFromAsset(getAssets(), "fonts/fontawesome-webfont.ttf");

/*
        retrofit = new Retrofit.Builder()
                .baseUrl(Configure.BASE_URL + "/")
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .client(createOkHttpClient())
                .build();

        retrofitGson = new Retrofit.Builder()
                .baseUrl(Configure.BASE_URL + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .client(createOkHttpClient())
                .build();
*/

        retrofitStream = new Retrofit.Builder()
                .baseUrl("http://61.250.198.66:9999" + "/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .client(createOkHttpProgressClient())
                .build();

        networkStreamService = retrofitStream.create(NetworkStreamService.class);

        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            soundPool = new SoundPool.Builder().setAudioAttributes(audioAttributes).setMaxStreams(1).build();
        }
        else {
            soundPool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 0);
        }

        soundId[0] = soundPool.load(this, R.raw.sound1, 1);

        logoFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "logo.png");
    }


    private boolean isDebuggable(Context context) {
        boolean debuggable = false;

        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo appinfo = pm.getApplicationInfo(context.getPackageName(), 0);
            debuggable = (0 != (appinfo.flags & ApplicationInfo.FLAG_DEBUGGABLE));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return debuggable;
    }

    public OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(interceptor);
        return builder.build();
    }

/*
    private OkHttpClient createOkHttpProgressClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(interceptor);
        builder.addInterceptor(new DownloadProgressInterceptor());
        return builder.build();
    }*/

    public NetworkService getNetworkService() {
        return networkService;
    }

    public NetworkStreamService getNetworkStreamService() {
        return networkStreamService;
    }

    public void showToast(String text) {
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setText(text);
        toast.show();
    }

    public Typeface getAwesomeFont() {
        return awesomeFont;
    }

    public Toast getToast() {
        return toast;
    }

    public SharedPreferences.Editor getPrefEditor() {
        return prefEditor;
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    public String getVersionName() {
        try {
            String version;
            PackageInfo i = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = i.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public Retrofit setRetrofitGson(Retrofit retrofit) {
        return this.retrofitGson = retrofit;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public Retrofit getRetrofitGson() {
        return retrofitGson;
    }

    public File getLogoFile() {
        return logoFile;
    }
}
