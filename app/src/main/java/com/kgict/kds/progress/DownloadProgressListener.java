package com.kgict.kds.progress;

public interface DownloadProgressListener {
    void update(String downloadIdentifier, long bytesRead, long contentLength, boolean done);
}
