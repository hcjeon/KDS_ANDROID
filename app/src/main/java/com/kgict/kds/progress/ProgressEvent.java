package com.kgict.kds.progress;

import com.kgict.kds.event.RxKeyEvent;

import io.reactivex.subjects.BehaviorSubject;

public class ProgressEvent {

    private static ProgressEvent mInstance;
    private BehaviorSubject<Integer> mSubject;

    final int progress;
    final long contentLength;
    final String downloadIdentifier;
    final long bytesRead;

    public ProgressEvent(String identifier, long contentLength, long bytesRead) {
        this.contentLength = contentLength;
        this.progress = (int) (bytesRead / (contentLength / 100f));
        downloadIdentifier = identifier;
        this.bytesRead = bytesRead;
        this.mSubject = BehaviorSubject.create();
    }

    public void sendBus(String identifier, long contentLength, long bytesRead) {
//        mSubject.onNext(identifier, contentLength, bytesRead);
    }

    public int getProgress(){
        return progress;
    }

    public String getDownloadIdentifier() {
        return downloadIdentifier;
    }

    public long getBytesRead() {
        return bytesRead;
    }

    public boolean percentIsAvailable() {
        return contentLength > 0;
    }

    @Override
    public String toString() {
        return "ProgressEvent{" +
                "progress=" + progress +
                ", contentLength=" + contentLength +
                ", downloadIdentifier='" + downloadIdentifier + '\'' +
                ", bytesRead=" + bytesRead +
                '}';
    }
}
