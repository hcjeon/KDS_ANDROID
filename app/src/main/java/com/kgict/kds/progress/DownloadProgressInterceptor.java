package com.kgict.kds.progress;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class DownloadProgressInterceptor implements Interceptor {
    public static final String DOWNLOAD_IDENTIFIER_HEADER = "download-identifier";

//    private final SimpleEventBus eventBus;
//
//    public DownloadProgressInterceptor(SimpleEventBus eventBus) {
//        this.eventBus = eventBus;
//    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());
        Response.Builder builder = originalResponse.newBuilder();

        String downloadIdentifier = originalResponse.request().header(DOWNLOAD_IDENTIFIER_HEADER);
        boolean isAStream = originalResponse.header("content-type", "").equals("application/octet-stream");
        boolean fileIdentifierIsSet = downloadIdentifier != null && !downloadIdentifier.isEmpty();

        builder.body(new DownloadProgressResponseBody(downloadIdentifier, originalResponse.body(), (identifier, bytesRead, contentLength, done) -> {
            // we post an event into the Bus !

//                eventBus.post(new ProgressEvent(identifier, contentLength, bytesRead));
//            ProgressEvent progressEvent = new ProgressEvent(identifier, contentLength, bytesRead);
//            progressEvent.sendBus();
        }));

//        if(isAStream && fileIdentifierIsSet) { // someone need progress informations !
//            builder.body(new DownloadProgressResponseBody(downloadIdentifier, originalResponse.body(), (identifier, bytesRead, contentLength, done) -> {
//                // we post an event into the Bus !
//
////                eventBus.post(new ProgressEvent(identifier, contentLength, bytesRead));
//                RxBus.get().send(new ProgressEvent(identifier, contentLength, bytesRead));
//            }));
//        } else { // do nothing if it's not a file with an identifier :)
//            builder.body(originalResponse.body());
//        }

        return builder.build();
    }
}