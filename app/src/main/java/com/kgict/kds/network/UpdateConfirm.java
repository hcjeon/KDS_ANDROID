package com.kgict.kds.network;

public class UpdateConfirm {
    String SOLUTION_CD;     // 솔루션 코드
    String MAC;             // MAC 주소
    String CLIENT_NO;       // 클라이언트(장비) 번호
    String VERSION;         // 현재 버전
    String BRAND_CD;        // 브랜드 코드
    String STORE_CD;        // 매장 코드
    String STORE_NM;        // 매장 명

    public UpdateConfirm(String SOLUTION_CD, String MAC, String CLIENT_NO, String VERSION, String BRAND_CD, String STORE_CD, String STORE_NM){
        this.SOLUTION_CD = SOLUTION_CD;
        this.MAC = MAC;
        this.CLIENT_NO = CLIENT_NO;
        this.VERSION= VERSION;
        this.BRAND_CD = BRAND_CD;
        this.STORE_CD = STORE_CD;
        this.STORE_NM = STORE_NM;
    }

}
