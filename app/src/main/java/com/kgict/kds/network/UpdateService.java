package com.kgict.kds.network;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UpdateService {

    @POST("API/IF_UPDATE_CONFIRM.aspx")
    Single<Map> VersionConfirm(@Body UpdateConfirm body);
}