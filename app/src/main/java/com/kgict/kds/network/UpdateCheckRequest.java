package com.kgict.kds.network;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


public class UpdateCheckRequest {
    String SOLUTION_CD;
    String MAC;
    String VERSION;
    String STORE_CD;
    String BRAND_CD;
    String STORE_NM;

    public UpdateCheckRequest(String SOLUTION_CD, String MAC, String VERSION, String BRAND_CD, String STORE_CD, String STORE_NM) {
        this.SOLUTION_CD = SOLUTION_CD;
        this.MAC = MAC;
        this.VERSION = VERSION;
        this.BRAND_CD = BRAND_CD;
        this.STORE_CD = STORE_CD;
        this.STORE_NM = STORE_NM;
    }
}
