package com.kgict.kds.network;

import com.kgict.kds.object.ItemGrp;
import com.kgict.kds.object.KdsConfigure;
import com.kgict.kds.object.Order;
import com.kgict.kds.object.StoreConfigure;

import java.util.List;

import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface NetworkService {

    // KDS 설정 가져오기
    @FormUrlEncoded
    @POST("/kds/getKdsConfigureNew")
    Single<KdsConfigure> getKdsConfigure(@Field("kdsId") long kdsId);

    // 상점 정보 가져오기
    @FormUrlEncoded
    @POST("/kds/getStoreConfigureNew")
    Single<StoreConfigure> getStoreConfigure(@Field("storeId") long storeId);

    // Category 리스트 가져오기
    @FormUrlEncoded
    @POST("/kds/getCategoryList")
    Single<List<ItemGrp>> getCategoryList(@Field("")String param);

    // 영업일자 가져오기
    @FormUrlEncoded
    @POST("/kds/getSaleYmd")
    Single<String> getSaleYmd(@Field("")String storeId);

    // 버전 정보 업데이트
    @FormUrlEncoded
    @POST("/kds/updateKdsVersion")
    Single<Integer> updateKdsVersion(@Field("kdsId") String kdsId, @Field("version") String version);

    // 주문 가져오기
    @FormUrlEncoded
    @POST("/kds/getAllOrderListNew")
    Single<List<Order>> getAllOrderList(@Field("start") int start, @Field("length") int length);

    // 팩킹주문 가져오기
    @FormUrlEncoded
    @POST("/kds/getAllOrderListPackingNew")
    Single<List<Order>> getAllOrderListPacking(@Field("start") int start, @Field("length") int length, @Field("deliveryFl") int deliveryFl);
    //Single<List<Order>> getAllOrderListPacking(@Field("start") int start, @Field("length") int length);

    // 주방주문 가져오기
    @FormUrlEncoded
    @POST("/kds/getAllOrderListKitchenNew")
    Single<List<Order>> getAllOrderListKitchen(@Field("start") int start, @Field("length") int length);

    // 주문 완료하기
    @FormUrlEncoded
    @POST("/kds/changeProcState")
    Single<Integer> changeProcState(@Field("orderId") String orderId, @Field("saleYmd") String saleYmd, @Field("state") String state);

    // 전체 주문 완료하기
    @POST("/kds/changeAllProcState")
    Single<Integer> changeAllProcState(@Body RequestBody param);

    // 직전주문 복원
    @FormUrlEncoded
    @POST("/kds/revertProcState")
    Single<Integer> revertProcState(@Field("state") String state, @Field("type") String type);

    // 완료 주문 가져오기
    @FormUrlEncoded
    @POST("/kds/getCompleteOrderListNew")
    Single<List<Order>> getCompleteOrderList(@Field("start") int start, @Field("length") int length);

    // Reboot 바꾸기
    @FormUrlEncoded
    @POST("/kds/updateRebootFlag")
    Single<Integer> updateRebootFlag(@Field("kdsId") String kdsId, @Field("flag") String flag);
}
