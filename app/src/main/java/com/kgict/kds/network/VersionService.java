package com.kgict.kds.network;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface VersionService {
    // 패치 URL
    @POST("API/IF_VERSION_CHECK.aspx")
    Single<Map> getLastVersion(@Body UpdateCheckRequest body);
}
