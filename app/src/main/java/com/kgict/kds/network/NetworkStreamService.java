package com.kgict.kds.network;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface NetworkStreamService {

    // 다운로드 파일 (긴파일)
    @GET
    Flowable<ResponseBody> downloadFileWithProgress(@Url String path);

    // 다운로드 파일
    @GET
    Flowable<ResponseBody> downloadFile(@Url String path);
}
