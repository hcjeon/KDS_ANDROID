package com.kgict.kds.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;
import com.kgict.kds.R;
import com.kgict.kds.adapter.CallNumAdapter;
import com.kgict.kds.adapter.CompleteOrderAdapter;
import com.kgict.kds.object.Order;
import com.kgict.kds.utils.Dlog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ManualCallFragment extends BaseFragment {

    private static ManualCallFragment instance = new ManualCallFragment();
    List<Map<String, String>> callHistory = new ArrayList<>();
    CallNumAdapter callNumAdapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.inputText)
    TextView inputText;
    @BindView(R.id.btn01)
    Button btn01;
    @BindView(R.id.btn02)
    Button btn02;
    @BindView(R.id.btn03)
    Button btn03;
    @BindView(R.id.btn04)
    Button btn04;
    @BindView(R.id.btn05)
    Button btn05;
    @BindView(R.id.btn06)
    Button btn06;
    @BindView(R.id.btn07)
    Button btn07;
    @BindView(R.id.btn08)
    Button btn08;
    @BindView(R.id.btn09)
    Button btn09;
    @BindView(R.id.btn00)
    Button btn00;
    @BindView(R.id.btnBackNum)
    Button btnBackNum;
    @BindView(R.id.btnClearNum)
    Button btnClearNum;
    @BindView(R.id.btnCall)
    Button btnCall;


    public ManualCallFragment() {
    }

    public static ManualCallFragment getInstance() {
        if (instance == null)
            instance = new ManualCallFragment();
        return instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_packing_call, container, false);
        ButterKnife.bind(this, view);

        initLayout();
        setListener();

        return view;
    }



    @Override
    public void initLayout() {
        callNumAdapter = new CallNumAdapter(mActivity, callHistory, (callNum) -> {
            inputText.setText(callNum);
        });
        LinearLayoutManager lm = new LinearLayoutManager(mActivity);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(callNumAdapter);
    }

    @Override
    public void setListener() {
        btn01.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "1");
        });
        btn02.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "2");
        });
        btn03.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "3");
        });
        btn04.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "4");
        });
        btn05.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "5");
        });
        btn06.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "6");
        });
        btn07.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "7");
        });
        btn08.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "8");
        });
        btn09.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "9");
        });
        btn00.setOnClickListener(v -> {
            inputText.setText(inputText.getText() + "0");
        });
        btnClearNum.setOnClickListener(v -> {
            inputText.setText("");
        });
        btnBackNum.setOnClickListener(v -> {
            if(inputText.getText().length() > 0) {
                StringBuilder sb = new StringBuilder(inputText.getText());
                sb.deleteCharAt(sb.length()-1);
                inputText.setText(sb.toString());
            }
        });

        btnCall.setOnClickListener(v -> {
            if(callHistory.size() > 10) {
                callHistory.remove(callHistory.size()-1);
            }
            Dlog.i("btnCall");
            if(inputText.getText().length() > 0) {
                Map<String, String> map = new HashMap<>();
                if(mActivity.getOrderDataList() == null || mActivity.getOrderDataList().size() == 0) {
                    mActivity.sendCallNum(inputText.getText().toString(),  "");
                    map.put("callNum", inputText.getText().toString());
                    inputText.setText("");
                    callHistory.add(0, map);
                    callNumAdapter.notifyDataSetChanged();
                    return;
                } else {
                    // 주문이 있는 것일 때 호출하고 해당 주문 완료처리
                    for(Order order : mActivity.getOrderDataList()) {
                        if(order.getCallNum().equals(inputText.getText().toString())) {
                            mApp.getNetworkService().changeProcState(order.getOrderId(), order.getSaleYmd(), "4")
                                    .compose(bindToLifecycle())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(retMap -> {
                                        // 취소된 메뉴일시 대기번호 호출X
                                            if(order.isCancel() == 0)
                                            mActivity.sendCallNum(order.getCallNum(), order.getReceiptNum());
                                        mActivity.downloadOrderList(false);
                                    }, throwable -> {
                                        Dlog.e(throwable.getMessage(), throwable);
                                        mApp.showToast(throwable.getMessage());
                                    });
                            map.put("callNum", inputText.getText().toString());
                            map.put("receiptNum", order.getReceiptNum());
                            inputText.setText("");
                            callHistory.add(0, map);
                            callNumAdapter.notifyDataSetChanged();
                            return;
                        }
                    }
                    mActivity.sendCallNum(inputText.getText().toString(), "");
                    map.put("callNum", inputText.getText().toString());
                    inputText.setText("");
                    callHistory.add(0, map);
                    callNumAdapter.notifyDataSetChanged();
                }
            }
        });
    }
}
