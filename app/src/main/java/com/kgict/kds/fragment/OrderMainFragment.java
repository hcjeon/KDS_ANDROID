package com.kgict.kds.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.R;
import com.kgict.kds.adapter.PageAdapter;
import com.kgict.kds.adapter.SummaryAdapter;
import com.kgict.kds.event.RxKeyEvent;
import com.kgict.kds.event.RxPageRefreshEvent;
import com.kgict.kds.object.Order;
import com.kgict.kds.object.OrderMenu;
import com.kgict.kds.object.SubItem;
import com.kgict.kds.utils.Dlog;
import com.kgict.kds.utils.Utils;

import org.apache.commons.collections4.MapUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderMainFragment extends BaseFragment {

    PageAdapter pageAdapter;

    // 메인내용
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.bottomBarLayout)
    LinearLayout bottomBarLayout;
    @BindView(R.id.summaryRecyclerView)
    RecyclerView summaryRecyclerView;
    @BindView(R.id.summaryTitle)
    TextView summaryTitle;
    @BindView(R.id.summaryLayout)
    ConstraintLayout summaryLayout;

    private static OrderMainFragment instance = new OrderMainFragment();

    public OrderMainFragment() {

    }

    public static OrderMainFragment getInstance() {
        if (instance == null)
            instance = new OrderMainFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_main, container, false);
        ButterKnife.bind(this, view);

        initLayout();
        setListener();

        return view;
    }

    @Override
    public void initLayout() {

        summaryTitle.setText(Configure.summaryTitle);

        pageAdapter = new PageAdapter(mActivity);
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                RxPageRefreshEvent.getInstance().sendBus(position);
                mActivity.setCurrentPage(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        viewPager.setOffscreenPageLimit(3);
        viewPager.setCurrentItem(0);

        if(Configure.summaryType.equals("0") || Configure.summaryType.equals("2")) {
            summaryLayout.setVisibility(View.GONE);
        }
        else {
            summaryLayout.setVisibility(View.VISIBLE);
        }

        // 하단바 생성
        LinearLayout.LayoutParams saleRectParam = new LinearLayout.LayoutParams(Utils.dimenToPx(R.dimen._8sdp), Utils.dimenToPx(R.dimen._8sdp));
        saleRectParam.setMargins(Utils.dimenToPx(R.dimen._20sdp), 0,0, 0);
        LinearLayout.LayoutParams saleTextParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        saleTextParam.setMargins(Utils.dimenToPx(R.dimen._4sdp), 0,0,0);
        int colorWhite = ContextCompat.getColor(mActivity, R.color.fontWhite);
        String[] saleTypeTitle = { Configure.saleTypeTitle1, Configure.saleTypeTitle2, Configure.deliveryTitle, Configure.saleTypeTitle4, Configure.saleTypeTitle5,
                Configure.saleTypeTitle6, Configure.saleTypeTitle7, Configure.saleTypeTitle8, Configure.saleTypeTitle9, Configure.saleTypeTitle10, "취소", "복원" };
        int[] saleBackground = { Configure.saleTypeBackgroundColor1, Configure.saleTypeBackgroundColor2, Configure.deliveryBackgroundColor, Configure.saleTypeBackgroundColor4, Configure.saleTypeBackgroundColor5,
                Configure.saleTypeBackgroundColor6, Configure.saleTypeBackgroundColor7, Configure.saleTypeBackgroundColor8, Configure.saleTypeBackgroundColor9, Configure.saleTypeBackgroundColor10,
        Configure.cancelBackgroundColor, Configure.revertBackgroundColor};

        for(int i=0; i<12; i++) {
            if(null != saleTypeTitle[i] && saleTypeTitle[i].length() > 0) {
                View saleRect = new View(mActivity);
                saleRect.setLayoutParams(saleRectParam);
                saleRect.setBackgroundColor(saleBackground[i]);
                bottomBarLayout.addView(saleRect);

                TextView saleText = new TextView(mActivity);
                saleText.setLayoutParams(saleTextParam);
                saleText.setText(saleTypeTitle[i]);
                saleText.setTextColor(colorWhite);
                bottomBarLayout.addView(saleText);
            }
        }
    }

    @Override
    public void setListener() {
        RxKeyEvent.getInstance().getBus()
                .compose(bindToLifecycle())
                .subscribe(keyCode -> {
                    onKeyUp(keyCode);
                });
    }

    public void onKeyUp(int keyCode) {
        switch (keyCode) {
            case Constants.KEY_DIC_LEFT:
                if (mActivity.getCurrentPage() > 0)
                    viewPager.setCurrentItem(mActivity.getCurrentPage() - 1);
                break;
            case Constants.KEY_DIC_RIGHT:
                if (mActivity.getPageAmount() - 1 > mActivity.getCurrentPage())
                    viewPager.setCurrentItem(mActivity.getCurrentPage() + 1);
                break;
            case Constants.KEY_DIC_UP:
                break;
            case Constants.KEY_DIC_DOWN:
                break;
            case Constants.KEY_DIC_CENTER:
                break;
            case Constants.KEY_FUNC_F1:
                break;
            case Constants.KEY_FUNC_F3:
                break;
            case Constants.KEY_FUNC_F4:
                break;
            case Constants.KEY_FUNC_F5:
                break;
            case Constants.KEY_FUNC_F6:
                break;
            case Constants.KEY_NUM_1:
            case Constants.KEY_KEYBOARD_NUM_1:
            case Constants.KEY_NUM_2:
            case Constants.KEY_KEYBOARD_NUM_2:
            case Constants.KEY_NUM_3:
            case Constants.KEY_KEYBOARD_NUM_3:
            case Constants.KEY_NUM_4:
            case Constants.KEY_KEYBOARD_NUM_4:
            case Constants.KEY_NUM_5:
            case Constants.KEY_KEYBOARD_NUM_5:
                break;
            case Constants.KEY_NUM_6:
            case Constants.KEY_KEYBOARD_NUM_6:
                break;
            case Constants.KEY_NUM_7:
            case Constants.KEY_KEYBOARD_NUM_7:
                break;
            case Constants.KEY_NUM_8:
            case Constants.KEY_KEYBOARD_NUM_8:
                break;
            case Constants.KEY_NUM_9:
            case Constants.KEY_KEYBOARD_NUM_9:
                break;
            case Constants.KEY_NUM_0:
            case Constants.KEY_KEYBOARD_NUM_0:
                break;
            case Constants.KEY_FUNC_F2:
                break;
        }
    }

    public PageAdapter getPageAdapter() {
        return pageAdapter;
    }

    /**
     * 썸머리창 새로고침
     */
    public void fetchSummary() {

        Map<String, Integer> summaryMap = new HashMap<>();
        Map<String, String> categoryMap = new HashMap<>();
        for (Order order : mActivity.orderDataList) {
            if (order.isCancel() == 0) {
                for (OrderMenu menu : order.getList()) {
                    // 자재 표기
                    if(Configure.summaryMenuType.equals("1")) {
                        if (menu.getQty() > 0) {
                            // 자재 있을 시
                            if(menu.getSetItemList().size() > 0) {
                                for(SubItem subItem : menu.getSetItemList()) {
                                    String subName = subItem.getName();
                                    Dlog.i(">>>" + subItem.toString());
                                    if (subName == null || subName.length() == 0)
                                        subName = subItem.getItemCd();
                                    int count = MapUtils.getInteger(summaryMap, subName, 0);
                                    summaryMap.put(subName, count + (menu.getQty() * subItem.getQty()));
//                                    categoryMap.put(subName, subItem.getGrpCd());
                                    categoryMap.put(subName, menu.getItemGrpCd());
                                }
                            } else {// 자재 없을시
                                /*
                                String name = menu.getName();
                                if (name == null || name.length() == 0)
                                    name = menu.getItemCode();
                                int count = MapUtils.getInteger(summaryMap, name, 0);
                                summaryMap.put(name, count + menu.getQty());
                                categoryMap.put(name, menu.getItemGrpCd());
                                */
                            }
                        }
                    } else {    // 메뉴 표기
                        if (menu.getQty() > 0) {
                            String name = menu.getName();
                            if (name == null || name.length() == 0)
                                name = menu.getItemCode();
                            int count = MapUtils.getInteger(summaryMap, name, 0);
                            summaryMap.put(name, count + menu.getQty());
                            categoryMap.put(name, menu.getItemGrpCd());
                        }
                    }
                }
            }
        }

        List<OrderMenu> menuList = new ArrayList<>();
        int sumCount = 0;
        Iterator it = sortByValue(summaryMap).iterator();
        while (it.hasNext()) {
            String name = (String) it.next();
            int qty = summaryMap.get(name);
            String itemGrpCode = categoryMap.get(name);
            menuList.add(new OrderMenu(name, 0, summaryMap.get(name), itemGrpCode, 1));
            sumCount += qty;
        }

        for(OrderMenu orderMenu : menuList) {
            Dlog.i(orderMenu.toString());
        }

        SummaryAdapter adapter = new SummaryAdapter(mActivity, menuList);
        LinearLayoutManager lm = new LinearLayoutManager(mActivity);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        summaryRecyclerView.setLayoutManager(lm);
        summaryRecyclerView.setAdapter(adapter);
    }

    public List sortByValue(final Map map) {

        List<String> list = new ArrayList();

        list.addAll(map.keySet());
        Collections.sort(list, new Comparator() {

            public int compare(Object o1, Object o2) {
                Object v1 = map.get(o1);
                Object v2 = map.get(o2);
                return ((Comparable) v2).compareTo(v1);
            }
        });
//        Collections.reverse(list); // 주석시 오름차순
        return list;
    }
}
