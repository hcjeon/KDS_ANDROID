package com.kgict.kds.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.R;
import com.kgict.kds.adapter.CompleteOrderAdapter;
import com.kgict.kds.object.Order;
import com.kgict.kds.utils.Dlog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CompleteFragment extends BaseFragment {

    private static CompleteFragment instance = new CompleteFragment();

    List<Order> orderList = new ArrayList<>();
    CompleteOrderAdapter completeOrderAdapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.scrollBtnLayout)
    LinearLayout scrollBtnLayout;

    @BindView(R.id.btnUp)
    Button btnUp;

    @BindView(R.id.btnDown)
    Button btnDown;

    public CompleteFragment() {
    }

    public static CompleteFragment getInstance() {
        if (instance == null)
            instance = new CompleteFragment();
        return instance;
    }

    @Override
    public void initLayout() {
        scrollBtnLayout.setVisibility(Constants.PACK_MUCH.equals(Configure.PackingType) ? View.VISIBLE : View.GONE);

        completeOrderAdapter = new CompleteOrderAdapter(mActivity, order -> {
            mActivity.sendCallNum(order.getCallNum(), order.getReceiptNum());
        });
        completeOrderAdapter.setOrderList(orderList);

        if(Constants.PACK_MUCH.equals(Configure.PackingType)) {
            //팩킹 타입이 주문 스크롤 상하
            GridLayoutManager gm = new GridLayoutManager(mActivity, 5);
            recyclerView.setLayoutManager(gm);

        } else {
            LinearLayoutManager lm = new LinearLayoutManager(mActivity);
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(lm);
        }

        recyclerView.setAdapter(completeOrderAdapter);

        notifyList();
    }

    @Override
    public void setListener() {
        int h = (int) mActivity.getResources().getDimension(R.dimen.packingMenuHeightS) + (int) mActivity.getResources().getDimension(R.dimen.temp_packing);

        btnUp.setOnClickListener(v -> recyclerView.smoothScrollBy(0, -h));
        btnDown.setOnClickListener(v -> recyclerView.smoothScrollBy(0, h));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_packing_order, container, false);
        ButterKnife.bind(this, view);

        initLayout();
        setListener();

        return view;
    }

    public void notifyList() {
        if (mApp == null)
            return;
        mApp.getNetworkService().getCompleteOrderList(0, 30)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .map(orders -> {
                    Dlog.i("complete >> " + orders.toString());
                    completeOrderAdapter.setOrderList(orders);
                    return Single.just(orders);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(orders -> {    // 페이지정보 화면 그리기
                    completeOrderAdapter.notifyDataSetChanged();
                }, onError -> {
                    onError.printStackTrace();
                });
    }

    /*
    public void fetchOrder(boolean isSound) {

        Dlog.i("OrderFragment >>> fetchOrder >> " + pageIndex);
        int orderViewIndex = pageIndex * Configure.getOrderMaxAmount();

        for (int i = orderViewIndex, j = 0; i < orderViewIndex + Configure.getOrderMaxAmount(); i++, j++) {
            OrderView orderView = (OrderView) gridLayout.getChildAt(j);
            final int jx = j;
            orderView.setOrderNum(String.valueOf(jx + 1));

            // 빈칸 채우기
            if (mActivity.getOrderViewData().size() - 1 < i) {
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK) {
                    Dlog.i("blankOrder >> " + pageIndex + "   orderViewIndex >> " + i);
                    orderView.setBlankOrder();
                }
                continue;
            } else {

                // 기존의 주문 화면과 데이터가 다를 경우 뷰 체인지
                OrderViewData orderViewData = mActivity.getOrderViewData().get(i);
                Order order = mActivity.getOrderDataList().get(orderViewData.getOrderDataIndex());
                if (orderView.getOrderViewData() == null || orderView.getOrder() == null || !order.isSame(orderView.getOrder())
                        || orderView.displayType == Constants.DISPLAY_TYPE_BLANK || orderView.displayType != orderViewData.getDisplayType()) {
                    Dlog.i("setOrder)) pageIndex >> " + pageIndex + "   ViewIndexInPge >> " + j + "     orderViewIndex >> " + i);
                    orderView.setOrderViewData(orderViewData);
                    orderView.setOrder(order);
                }
            }
        }

        //  Summary
        if (Configure.isShowSummary) {
            Dlog.i("summary>>>>>>" + pageIndex);
            OrderView summary = (OrderView) gridLayout.getChildAt(Configure.getOrderMaxAmount());
            summary.setPageIndex(pageIndex);
            summary.setSummary(mActivity.orderDataList, isSound);
        }
    }*/

    @Override
    public void onResume() {
        Dlog.i("dddddd");
        super.onResume();
    }

    @Override
    public void onStart() {
        Dlog.i("dddddd2222");
        super.onStart();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Dlog.i("dddddd3333");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        Dlog.i("dddddd4444");
        super.onAttach(activity);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        Dlog.i("dddddd55555");
        super.onHiddenChanged(hidden);
    }
}
