package com.kgict.kds.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.R;
import com.kgict.kds.event.RxKeyEvent;
import com.kgict.kds.event.RxNetworkStateEvent;
import com.kgict.kds.event.RxPageRefreshEvent;
import com.kgict.kds.object.Order;
import com.kgict.kds.utils.Dlog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class MainFragment extends BaseFragment {

    public OrderMainFragment orderMainFragment;
    public PackingMainFragment packingMainFragment;
    public CompleteFragment completeFragment;
    public ManualCallFragment manualCallFragment;

    BaseFragment currentFragment;

    // 상단바
    @BindView(R.id.kitchenText)
    TextView kitchenText;
    @BindView(R.id.rightPageIcon)
    TextView rightPageIcon;
    @BindView(R.id.rightPageText)
    TextView rightPageText;
    @BindView(R.id.leftPageIcon)
    TextView leftPageIcon;
    @BindView(R.id.leftPageText)
    TextView leftPageText;
    @BindView(R.id.storeText)
    TextView storeText;
    @BindView(R.id.networkView)
    View networkView;
    @BindView(R.id.showWaitingButton)
    TextView showWaitingButton;
    @BindView(R.id.showCompleteButton)
    TextView showCompleteButton;
    @BindView(R.id.showManualCall)
    TextView showManualCall;
    @BindView(R.id.deleteOrderText)
    TextView deleteOrderText;
    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.saleYmd)
    TextView saleYmdText;
    @BindView(R.id.nowTime)
    TextView nowTimeText;
    @BindView(R.id.version)
    TextView version;

    @BindView(R.id.debugInfoText)
    TextView debugInfoText;

    Timer timeTimer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        rightPageIcon.setTypeface(mApp.getAwesomeFont());
        leftPageIcon.setTypeface(mApp.getAwesomeFont());

        initLayout();
        setListener();

        return view;
    }

    @Override
    public void initLayout() {
        Dlog.i("========================mainFragment initLayout========================");
        version.setText(mApp.getVersionName());
        storeText.setText(Configure.storeName);

        showCompleteButton.setOnClickListener(v -> {
            mActivity.orderDataList.clear();
            fetchOrder(false);
        });

        showWaitingButton.setOnClickListener(v -> {
//            createDummy();
            fetchOrder(false);
        });

        // KDS 타입에 따른 레이아웃 변경
        if (Configure.kdsType == Constants.KDS_TYPE_PACKING) {
            showWaitingButton.setVisibility(View.VISIBLE);
            showCompleteButton.setVisibility(View.VISIBLE);
            showManualCall.setVisibility(View.VISIBLE);
            kitchenText.setVisibility(View.GONE);
            deleteOrderText.setVisibility(View.VISIBLE);
            version.setVisibility(View.GONE);
        } else if (Configure.kdsType == Constants.KDS_TYPE_COUNTER) {
            showWaitingButton.setVisibility(View.INVISIBLE);
            showCompleteButton.setVisibility(View.INVISIBLE);
            showManualCall.setVisibility(View.INVISIBLE);
            kitchenText.setVisibility(View.GONE);
            deleteOrderText.setVisibility(View.GONE);
            version.setVisibility(View.VISIBLE);
        } else if (Configure.kdsType == Constants.KDS_TYPE_KITCHEN) {
            showWaitingButton.setVisibility(View.INVISIBLE);
            showCompleteButton.setVisibility(View.INVISIBLE);
            showManualCall.setVisibility(View.INVISIBLE);
            kitchenText.setVisibility(View.VISIBLE);
            deleteOrderText.setVisibility(View.GONE);
            version.setVisibility(View.VISIBLE);
        }

        // 일반 KDS(카운터용, 주방용)
        if (Configure.kdsType == Constants.KDS_TYPE_COUNTER
                || Configure.kdsType == Constants.KDS_TYPE_KITCHEN) {

            orderMainFragment = OrderMainFragment.getInstance();
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, orderMainFragment, "orderMainFragment");
            fragmentTransaction.commit();

            currentFragment = orderMainFragment;

        } else {
            packingMainFragment = PackingMainFragment.getInstance();
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, packingMainFragment, "packingMainFragment");
            fragmentTransaction.commit();

            currentFragment = packingMainFragment;
        }

        mActivity.checkSaleYmd();
    }

    public void setListener() {

        rightPageIcon.setOnClickListener(v -> {
            RxKeyEvent.getInstance().sendKeyCode(Constants.KEY_DIC_RIGHT);
        });
        rightPageText.setOnClickListener(v -> {
            RxKeyEvent.getInstance().sendKeyCode(Constants.KEY_DIC_RIGHT);
        });

        leftPageIcon.setOnClickListener(v -> {
            RxKeyEvent.getInstance().sendKeyCode(Constants.KEY_DIC_LEFT);
        });
        leftPageText.setOnClickListener(v -> {
            RxKeyEvent.getInstance().sendKeyCode(Constants.KEY_DIC_LEFT);
        });

        showWaitingButton.setOnClickListener(v -> {
            packingMainFragment = PackingMainFragment.getInstance();
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, packingMainFragment, "packingMainFragment");
            fragmentTransaction.commit();

            deleteOrderText.setVisibility(View.VISIBLE);

            mActivity.setIsUpdating(false);
            currentFragment = packingMainFragment;
            packingMainFragment.notifyList();

        });

        showCompleteButton.setOnClickListener(v -> {
            // 프레그먼트 생성
            completeFragment = CompleteFragment.getInstance();
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, completeFragment, "completeFragment");
            fragmentTransaction.commit();

            mActivity.setIsUpdating(false);
            currentFragment = completeFragment;
            completeFragment.notifyList();

            deleteOrderText.setVisibility(View.INVISIBLE);

        });

        showManualCall.setOnClickListener(v -> {
            // 프레그먼트 생성
            manualCallFragment = ManualCallFragment.getInstance();
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, manualCallFragment, "manualCallFragment");
            fragmentTransaction.commit();

            mActivity.setIsUpdating(false);
            currentFragment = manualCallFragment;

            deleteOrderText.setVisibility(View.INVISIBLE);
        });

        deleteOrderText.setOnClickListener(v -> {
            packingMainFragment.removeView(4);
        });

        RxPageRefreshEvent.getInstance().getBus()
                .compose(bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(position -> {
                    if (position > 0) {
                        leftPageIcon.setVisibility(View.VISIBLE);
                        leftPageText.setVisibility(View.VISIBLE);
                        leftPageText.setText(String.valueOf(position));
                    } else {
                        leftPageIcon.setVisibility(View.INVISIBLE);
                        leftPageText.setVisibility(View.INVISIBLE);
                    }

                    if (mActivity.getPageAmount() - 1 > position) {
                        rightPageIcon.setVisibility(View.VISIBLE);
                        rightPageText.setVisibility(View.VISIBLE);
                        rightPageText.setText(String.valueOf(position + 2));
                    } else {
                        rightPageIcon.setVisibility(View.INVISIBLE);
                        rightPageText.setVisibility(View.INVISIBLE);
                    }
                });

        RxNetworkStateEvent.getInstance().getBus()
                .compose(bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> {
                    switch (integer) {
                        case Constants.NETWORK_STATE_FAIL:
                            setNetworkView(ContextCompat.getColor(mActivity, R.color.red));
                            break;
                        case Constants.NETWORK_STATE_SUCCESS:
                            setNetworkView(ContextCompat.getColor(mActivity, R.color.darkGreen));
                            break;
                        case Constants.NETWORK_STATE_LOGIC_ERROR:
                            setNetworkView(ContextCompat.getColor(mActivity, R.color.yellow));
                            break;
                    }
                });
    }

    public void refreshPageView() {
        if (mApp.isDebug) {
            debugInfoText.setText("ip >> " + Configure.BASE_URL + "\n");
        }
        fetchPageIcon();
    }

    public void fetchOrder(boolean isSound) {
        if (currentFragment instanceof OrderMainFragment)
            ((OrderMainFragment) currentFragment).getPageAdapter().fetchOrder(isSound);
    }

    public void fetchPageIcon() {
        int position = mActivity.getCurrentPage();
        if (position > 0) {
            leftPageIcon.setVisibility(View.VISIBLE);
            leftPageText.setVisibility(View.VISIBLE);
            leftPageText.setText(String.valueOf(position));
        } else {
            leftPageIcon.setVisibility(View.INVISIBLE);
            leftPageText.setVisibility(View.INVISIBLE);
        }

        if (mActivity.getPageAmount() - 1 > position) {
            rightPageIcon.setVisibility(View.VISIBLE);
            rightPageText.setVisibility(View.VISIBLE);
            rightPageText.setText(String.valueOf(position + 2));
        } else {
            rightPageIcon.setVisibility(View.INVISIBLE);
            rightPageText.setVisibility(View.INVISIBLE);
        }
    }

    public void setSaleYmdText() {
        if(Configure.saleYmd == null || Configure.saleYmd.length() != 8) {
            return;
        }
        saleYmdText.setText(Configure.saleYmd.substring(0, 4) + "-" + Configure.saleYmd.substring(4,6) + "-" + Configure.saleYmd.substring(6,8));

        if (timeTimer == null){
            timeTimer = new Timer();
            timeTimer.schedule(timeTimerTask, 0, 1000);
        }
    }

    TimerTask timeTimerTask = new TimerTask(){
        public void run() {

            long now = System.currentTimeMillis();
            Date date = new Date(now);
            SimpleDateFormat sdfNow = new SimpleDateFormat("HH:mm:ss");
            String formatDate = sdfNow.format(date);

            if (formatDate == null || formatDate.length() == 0)
                return;

            Handler timeHandler = nowTimeText.getHandler();
            timeHandler.post(new Runnable() {
                public void run() {
                    nowTimeText.setText(formatDate);
                }
            });
        }
    }; //end TimerTask

    public List<Order> getOrderList() {
        return mActivity.orderDataList;
    }

    public List<Order> getTmpOrderList() {
        return mActivity.tmpOrderDataList;
    }

    public void setNetworkView(int color) {
        this.networkView.setBackgroundColor(color);
    }
}
