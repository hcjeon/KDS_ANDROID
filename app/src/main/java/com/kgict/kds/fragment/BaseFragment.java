package com.kgict.kds.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.kgict.kds.App;
import com.kgict.kds.MainActivity;
import com.trello.rxlifecycle2.components.support.RxFragment;

public abstract class BaseFragment extends RxFragment {
    public abstract void initLayout();
    public abstract void setListener();

    App mApp;
    MainActivity mActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mActivity = (MainActivity)getActivity();
        mApp = (App) getContext().getApplicationContext();
        super.onCreate(savedInstanceState);
    }

    public void showToast(String text) {
        mApp.getToast().setText(text);
        mApp.getToast().show();
    }
}
