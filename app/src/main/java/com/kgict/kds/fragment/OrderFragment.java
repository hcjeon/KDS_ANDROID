package com.kgict.kds.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.kgict.kds.App;
import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.MainActivity;
import com.kgict.kds.R;
import com.kgict.kds.adapter.PageAdapter;
import com.kgict.kds.event.RxKeyEvent;
import com.kgict.kds.object.Order;
import com.kgict.kds.object.OrderView;
import com.kgict.kds.object.OrderViewData;
import com.kgict.kds.utils.Dlog;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;

public class OrderFragment extends BaseFragment {

    int columnCount = 5;    // 가로줄
    int rowCount = 2;       // 세로줄

    PageAdapter pageAdapter;
    int pageIndex = -1;

    @BindView(R.id.gridLayout)
    GridLayout gridLayout;
    @BindView(R.id.textView)
    TextView textView;

    public OrderFragment() {
    }

    public void setParentAdapter(PageAdapter parentAdapter) {
        this.pageAdapter = parentAdapter;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, view);

        mActivity = (MainActivity) getActivity();
        mApp = (App) mActivity.getApplication();

        textView.setText(pageIndex + "번째");

        initLayout();
        setListener();
        fetchOrder(false);

        Dlog.i("OrderFragment initView >>>>>>>>>>>>>>>>>>   " + pageIndex);

        return view;
    }

    @Override
    public void initLayout() {
        columnCount = Configure.orderColumnCount;
        rowCount = Configure.orderRowCount;
        gridLayout.setColumnCount(columnCount);
        gridLayout.setRowCount(rowCount);
//        gridLayout.setBackgroundColor(ContextCompat.getColor(mApp, R.color.black));
        gridLayout.setBackgroundColor(Color.parseColor("#" + Configure.backgroundColor));

        for (int i = 0; i < Configure.getOrderMaxAmount(); i++) {
            Dlog.i(">>>>>>" + i + "    " + pageIndex);
            OrderView orderView = new OrderView(mActivity);

            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
            layoutParams.height = 0;
            layoutParams.width = 0;
            layoutParams.rowSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);
            layoutParams.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);

            orderView.setLayoutParams(layoutParams);
            gridLayout.addView(orderView);

            if(i== Configure.getOrderMaxAmount()-1) {
                ViewTreeObserver vto = orderView.getViewTreeObserver();
                ViewTreeObserver.OnPreDrawListener gll = () -> {

                    if (mActivity.getCurrentPage() == pageIndex) {
                        Dlog.i("vto >>> " + mActivity.isUpdating + "   >>>>>   sound:" + "  pageIndex>>  " + pageIndex + "  activity >> " + mActivity.getCurrentPage());
                        if (mActivity.isUpdating) {
                            if (mActivity.isSound && Configure.kdsType != Constants.KDS_TYPE_PACKING) {
                                mActivity.mApp.soundPool.play(mActivity.mApp.soundId[0], .3f, .3f, 0, 0, 1f);
                                mActivity.isSound = false;
                            }
                            mActivity.setIsUpdating(false);
                        }
                    }
                    return true;
                };
                vto.addOnPreDrawListener(gll);
            }
        }

        if(Configure.summaryType.equals("2")) {
            OrderView summaryView = new OrderView(mActivity);

            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
            layoutParams.height = 0;
            layoutParams.width = 0;
            layoutParams.rowSpec = GridLayout.spec(Configure.orderRowCount - 1, 1f);
            layoutParams.columnSpec = GridLayout.spec(Configure.orderColumnCount - 1, 1f);

            summaryView.setLayoutParams(layoutParams);
            gridLayout.addView(summaryView);
        }
    }

    public void setListener() {
        RxKeyEvent.getInstance().getBus()
                .compose(bindToLifecycle())
                .subscribe(keyCode -> {
                    if (pageIndex == mActivity.getCurrentPage()) {
                        Dlog.i(keyCode + "   " + mActivity.getIsUpdating() + "   " + mActivity.isUpdating + "    ");
                        if (mActivity.isUpdating)
                            return;
                        onKeyMapping(keyCode);
                    }
                });
    }

    public void fetchOrder(boolean isSound) {

        Dlog.i("OrderFragment >>> fetchOrder >> " + pageIndex);
        if (gridLayout == null)
            return;
        Dlog.i("gridLayout.childCount >> " + gridLayout.getChildCount() != null ? String.valueOf(gridLayout.getChildCount()) : "-1");
        int orderViewIndex = pageIndex * Configure.getOrderMaxAmount();

        // 삭제된 뷰가 있을시 빈칸으로 채우기
        for (int i = 0; i < Configure.orderRowCount * Configure.orderColumnCount; i++) {
            if (gridLayout.getChildCount() >= Configure.orderRowCount * Configure.orderColumnCount)
                break;
            OrderView orderView = new OrderView(mActivity);
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
            layoutParams.height = 0;
            layoutParams.width = 0;
            layoutParams.rowSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);
            layoutParams.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);
            orderView.setLayoutParams(layoutParams);

            if (Configure.summaryType.equals("2"))
                gridLayout.addView(orderView, gridLayout.getChildCount() - 1);
            else {
                gridLayout.addView(orderView, gridLayout.getChildCount());
            }

            ViewTreeObserver vto = orderView.getViewTreeObserver();
            ViewTreeObserver.OnPreDrawListener gll = () -> {

                if (mActivity.getCurrentPage() == pageIndex) {
                    Dlog.i("vto >>> " + mActivity.isUpdating + "   >>>>>   sound:" + isSound + "  pageIndex>>  " + pageIndex + "  activity >> " + mActivity.getCurrentPage());
                    if (mActivity.isUpdating) {
                        if (mActivity.isSound && Configure.kdsType != Constants.KDS_TYPE_PACKING) {
                            mActivity.mApp.soundPool.play(mActivity.mApp.soundId[0], .3f, .3f, 0, 0, 1f);
                            mActivity.isSound = false;
                        }
                        mActivity.setIsUpdating(false);
                    }
                }
                return true;
            };
            vto.addOnPreDrawListener(gll);

        }

        for (int i = orderViewIndex, j = 0; i < orderViewIndex + Configure.getOrderMaxAmount(); i++, j++) {
            OrderView orderView = (OrderView) gridLayout.getChildAt(j);
            final int jx = j;
            orderView.setOrderNum(String.valueOf(jx + 1));

            // 빈칸 채우기
            if (mActivity.getOrderViewData().size() - 1 < i) {
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK) {
                    Dlog.i("blankOrder >> " + pageIndex + "   orderViewIndex >> " + i);
                    orderView.setBlankOrder();
                }
                continue;
            } else {

                // 기존의 주문 화면과 데이터가 다를 경우 뷰 체인지
                OrderViewData orderViewData = mActivity.getOrderViewData().get(i);
                Order order = mActivity.getOrderDataList().get(orderViewData.getOrderDataIndex());
                if (orderView.getOrderViewData() == null || orderView.getOrder() == null || !order.isSame(orderView.getOrder())
                        || orderView.displayType == Constants.DISPLAY_TYPE_BLANK || orderView.displayType != orderViewData.getDisplayType()) {
                    Dlog.i("setOrder)) pageIndex >> " + pageIndex + "   ViewIndexInPge >> " + j + "     orderViewIndex >> " + i);
                    orderView.setOrderViewData(orderViewData);
                    orderView.setOrder(order);
                }
            }
        }

        //  Summary
        if (Configure.summaryType.equals("2")) {
            Dlog.i("summary>>>>>>" + pageIndex);
            OrderView summary = (OrderView) gridLayout.getChildAt(Configure.getOrderMaxAmount());
            summary.setPageIndex(pageIndex);
            summary.setSummary(mActivity.orderDataList, isSound);
            summary.displayType = Constants.DISPLAY_TYPE_SUMMARY;
        }
    }

    public void removeView(int removePosition) {

        mActivity.setIsUpdating(true);

        OrderView removeOrderView = (OrderView) gridLayout.getChildAt(removePosition);
        OrderViewData orderViewData = removeOrderView.getOrderViewData();

        // 한칸짜리 주문일 때
        if (orderViewData.getDisplayType() == Constants.DISPLAY_TYPE_ORDER_NORMAL) {
            gridLayout.removeViewAt(removePosition);
        } else {
            // 다량 주문일 때 여러개 지우기
            int startPosition = removePosition - orderViewData.getManyMenuIndex();      // 해당 다량주문의 시작 Position
            int removeCount = 0;
            for (int i = 0; i < orderViewData.getManyMenuMaxCount(); i++) {
                if (startPosition + i > Configure.getOrderMaxAmount() - 1) {
                    break;
                }
                removeCount++;
            }
            gridLayout.removeViews(startPosition, removeCount);
        }

        // 순번 새로고침
        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            ((OrderView) gridLayout.getChildAt(i)).setOrderNum(String.valueOf(i + 1));
        }

        String state = "3";
        if (Configure.kdsType == Constants.KDS_TYPE_KITCHEN) {
            state = "2";
        } else if (removeOrderView.getOrder().getSaleType().equals("H") || removeOrderView.getOrder().isCancel() == 1) {
            state = "4";
        }
        mApp.getNetworkService().changeProcState(removeOrderView.getOrder().getOrderId(), removeOrderView.getOrder().getSaleYmd(), state)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(retMap -> {
                    if(Configure.kdsType == Constants.KDS_TYPE_KITCHEN && Configure.useKitchenPrint.equals("1")) {
                        mActivity.updateCallState(removeOrderView.getOrder().getReceiptNum(), "6");
                    }
                    // 바뀐 메뉴 한번 확인하기
                    mActivity.downloadOrderList(false);
                }, throwable -> {
                    Dlog.e(throwable.getMessage(), throwable);
                    mApp.showToast(throwable.getMessage());
                });
    }

    /**
     * 전체 완료(삭제)
     */
    public void removeAllView() {
        if (gridLayout == null) {
            return;
        }

        mActivity.setIsUpdating(true);

        try {
            JSONObject job = new JSONObject();

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < gridLayout.getChildCount(); i++) {
                if (gridLayout.getChildAt(i) == null)
                    break;
                OrderView orderView = (OrderView) gridLayout.getChildAt(i);
                if (orderView.displayType == Constants.DISPLAY_TYPE_SUMMARY
                        || orderView.displayType == Constants.DISPLAY_TYPE_BLANK
                        || orderView.displayType == Constants.DISPLAY_TYPE_NONE)
                    break;

                Order order = ((OrderView) gridLayout.getChildAt(i)).getOrder();
                JSONObject jb = new JSONObject();
                jb.put("orderId", order.getOrderId());
                jb.put("saleYmd", order.getSaleYmd());

                String state = "3";
                if (Configure.kdsType == Constants.KDS_TYPE_KITCHEN) {
                    state = "2";
                } else if (order.getSaleType().equals("H") || order.isCancel() == 1) {
                    state = "4";
                }
                jb.put("state", state);
                jsonArray.put(jb);
            }
            job.put("items", jsonArray);

            Dlog.i("removeAllView >>> " + job.toString());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), job.toString());
            mApp.getNetworkService().changeAllProcState(body)
                    .compose(bindToLifecycle())
                    .subscribeOn(Schedulers.io())
                    .subscribe(retMap -> {
                        // 바뀐 메뉴 한번 확인하기
                        mActivity.downloadOrderList(false);
                    }, throwable -> {
                        Dlog.e(throwable.getMessage(), throwable);
                        mApp.showToast(throwable.getMessage());
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 주문 복원
     */
    public void revertOrder() {
        Dlog.i("revert>>>>>>>>>>>>>>>>>>>>>>>");
        mApp.getNetworkService().revertProcState("1", String.valueOf(Configure.kdsType))
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(retMap -> {
                    // 바뀐 메뉴 한번 확인하기
                    mActivity.downloadOrderList(false);
                }, throwable -> {
                    mApp.showToast(throwable.getMessage());
                    Dlog.e(throwable.getMessage(), throwable);
                });
    }


    public void refreshTime() {
        for (int i = 0; i < Configure.getOrderMaxAmount(); i++) {
            OrderView orderView = ((OrderView) gridLayout.getChildAt(i));
            if (orderView != null
                    && (orderView.displayType == Constants.DISPLAY_TYPE_ORDER_NORMAL
                    || orderView.displayType == Constants.DISPLAY_TYPE_ORDER_END))
                orderView.refreshTime();
        }
    }

    public void onKeyMapping(int keyCode) {

        long interval = Configure.keyInterval;
        if (mActivity.getOrderDataList() != null
                && mActivity.getOrderDataList().size() > Configure.getOrderMaxAmount() * 2)
            interval = Configure.keyInterval2;

        if (System.currentTimeMillis() - mActivity.getLastInputKeyTime() <= interval)
            return;

        mActivity.setLastInputKeyTime();

        OrderView orderView;
        switch (keyCode) {
            case Constants.KEY_FUNC_F1:
                removeAllView();
                break;
            // 주문 복원
            case Constants.KEY_FUNC_F2:
                revertOrder();
                break;
            case Constants.KEY_NUM_1:
            case Constants.KEY_KEYBOARD_NUM_1:
                orderView = (OrderView) gridLayout.getChildAt(0);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(0);
                break;
            case Constants.KEY_NUM_2:
            case Constants.KEY_KEYBOARD_NUM_2:
                orderView = (OrderView) gridLayout.getChildAt(1);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(1);
                break;
            case Constants.KEY_NUM_3:
            case Constants.KEY_KEYBOARD_NUM_3:
                orderView = (OrderView) gridLayout.getChildAt(2);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(2);
                break;
            case Constants.KEY_NUM_4:
            case Constants.KEY_KEYBOARD_NUM_4:
                orderView = (OrderView) gridLayout.getChildAt(3);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(3);
                break;
            case Constants.KEY_NUM_5:
            case Constants.KEY_KEYBOARD_NUM_5:
                orderView = (OrderView) gridLayout.getChildAt(4);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(4);
                break;
            case Constants.KEY_NUM_6:
            case Constants.KEY_KEYBOARD_NUM_6:
                orderView = (OrderView) gridLayout.getChildAt(5);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(5);
                break;
            case Constants.KEY_NUM_7:
            case Constants.KEY_KEYBOARD_NUM_7:
                orderView = (OrderView) gridLayout.getChildAt(6);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(6);
                break;
            case Constants.KEY_NUM_8:
            case Constants.KEY_KEYBOARD_NUM_8:
                orderView = (OrderView) gridLayout.getChildAt(7);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(7);
                break;
            case Constants.KEY_NUM_9:
            case Constants.KEY_KEYBOARD_NUM_9:
                orderView = (OrderView) gridLayout.getChildAt(8);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(8);
                break;
            case Constants.KEY_NUM_0:
            case Constants.KEY_KEYBOARD_NUM_0:
                if (Configure.summaryType.equals("2"))
                    break;
                orderView = (OrderView) gridLayout.getChildAt(9);
                if (orderView.displayType != Constants.DISPLAY_TYPE_BLANK)
                    removeView(9);
                break;
        }
    }


}
