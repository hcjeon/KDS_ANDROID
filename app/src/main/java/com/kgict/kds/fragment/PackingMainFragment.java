package com.kgict.kds.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.R;
import com.kgict.kds.adapter.PackingOrderAdapter;
import com.kgict.kds.object.Order;
import com.kgict.kds.utils.Dlog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;

public class PackingMainFragment extends BaseFragment {

    private static PackingMainFragment instance = new PackingMainFragment();

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.scrollBtnLayout)
    LinearLayout scrollBtnLayout;

    @BindView(R.id.btnUp)
    Button btnUp;

    @BindView(R.id.btnDown)
    Button btnDown;

    private PackingOrderAdapter packingOrderAdapter;

    private Parcelable recyclerViewState;

    private List<Order> orderList = new ArrayList<>();

    public PackingMainFragment() {
    }

    public static PackingMainFragment getInstance() {
        if (instance == null)
            instance = new PackingMainFragment();
        return instance;
    }

    @Override
    public void initLayout() {
        init();
        notifyList();
    }

    @Override
    public void setListener() {
        int h = (int) mActivity.getResources().getDimension(R.dimen.packingMenuHeightS) + (int) mActivity.getResources().getDimension(R.dimen.temp_packing);

        btnUp.setOnClickListener(v -> recyclerView.smoothScrollBy(0, -h));
        btnDown.setOnClickListener(v -> recyclerView.smoothScrollBy(0, h));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_packing_order, container, false);
        ButterKnife.bind(this, view);

        initLayout();
        setListener();

        return view;
    }

    private void init() {
        scrollBtnLayout.setVisibility(Constants.PACK_MUCH.equals(Configure.PackingType) ? View.VISIBLE : View.GONE);

        packingOrderAdapter = new PackingOrderAdapter(mActivity, new PackingOrderAdapter.OnClickListener() {
            @Override
            public void onClickListener(Order order, int position) {
                Dlog.i("11111");
                mApp.getNetworkService().changeProcState(order.getOrderId(), order.getSaleYmd(), "4")
                        .compose(bindToLifecycle())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(retMap -> {
                            // 취소된 메뉴일시 대기번호 호출X
//                        if(order.isCancel() == 0 && (!order.getSaleType().equals("H") && order.getRegDevice() != 2))
                            Dlog.i("2222222222222");
                            if (order.isCancel() == 0)
                                mActivity.sendCallNum(order.getCallNum(), order.getReceiptNum());

                            if(Constants.PACK_MUCH.equals(Configure.PackingType)) {
                                if(packingOrderAdapter != null) {
                                    packingOrderAdapter.removeItem(order);
                                    packingOrderAdapter.notifyDataSetChanged();
                                }
                            } else {
                                mActivity.downloadOrderList(false);
                            }
                        }, throwable -> {
                            Dlog.e(throwable.getMessage(), throwable);
                            mApp.showToast(throwable.getMessage());
                        });

            }

            @Override
            public void onClickCompleteNoCall(Order order) {
                mApp.getNetworkService().changeProcState(order.getOrderId(), order.getSaleYmd(), "4")
                        .compose(bindToLifecycle())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(retMap -> {
                            // 취소된 메뉴일시 대기번호 호출X
//                        if(order.isCancel() == 0 && (!order.getSaleType().equals("H") && order.getRegDevice() != 2))
                            if (order.isCancel() == 0)
                                mActivity.updateCallState(order.getReceiptNum(), "8");

                            mActivity.downloadOrderList(false);
                        }, throwable -> {
                            Dlog.e(throwable.getMessage(), throwable);
                            mApp.showToast(throwable.getMessage());
                        });
            }
        });
        packingOrderAdapter.setOrderList(orderList);

        if(Constants.PACK_MUCH.equals(Configure.PackingType)) {
            //팩킹 타입이 주문 스크롤 상하
            GridLayoutManager gm = new GridLayoutManager(mActivity, 5);
            recyclerView.setLayoutManager(gm);

            recyclerView.setHasFixedSize(true);
            recyclerViewState = recyclerView.getLayoutManager().onSaveInstanceState();

        } else {
            LinearLayoutManager lm = new LinearLayoutManager(mActivity);
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(lm);
        }

        recyclerView.setAdapter(packingOrderAdapter);
    }

    public void notifyList() {
        if (mApp == null || mActivity.getOrderDataList() == null)
            return;

        if(packingOrderAdapter != null) {
            packingOrderAdapter.setOrderList(mActivity.getOrderDataList());
            packingOrderAdapter.notifyDataSetChanged();

            if(Constants.PACK_MUCH.equals(Configure.PackingType))
                recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
        }

        mActivity.setIsUpdating(false);
    }

    /**
     * 전체 완료(삭제)
     */
    public void removeView(int count) {

        Dlog.i(">>>>>>>>> " + mActivity.getOrderDataList().size());
        if (mActivity.getOrderDataList().size() == 0)
            return;

        mActivity.setIsUpdating(true);

        try {
            JSONObject job = new JSONObject();

            JSONArray jsonArray = new JSONArray();
            List<String> receiptList = new ArrayList<>();
            for (int i = 0; i < count; i++) {
                if (mActivity.getOrderDataList().size() - 1 < i)
                    break;

                Order order = mActivity.getOrderDataList().get(i);
                JSONObject jb = new JSONObject();
                jb.put("orderId", order.getOrderId());
                jb.put("saleYmd", order.getSaleYmd());
                jb.put("state", "4");
                jsonArray.put(jb);

                receiptList.add(order.getReceiptNum());
            }
            job.put("items", jsonArray);

            for (String receiptNum : receiptList) {
                mActivity.updateCallState(receiptNum, "8");
            }

            Dlog.i("removeAllView >>> " + job.toString());
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), job.toString());

            mApp.getNetworkService().changeAllProcState(body)
                    .compose(bindToLifecycle())
                    .subscribeOn(Schedulers.io())
                    .subscribe(retMap -> {
                        // 바뀐 메뉴 한번 확인하기
                        mActivity.downloadOrderList(false);
                    }, throwable -> {
                        Dlog.e(throwable.getMessage(), throwable);
                        mApp.showToast(throwable.getMessage());
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
