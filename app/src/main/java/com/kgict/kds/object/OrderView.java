package com.kgict.kds.object;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.MainActivity;
import com.kgict.kds.R;
import com.kgict.kds.adapter.MenuListAdapter;
import com.kgict.kds.adapter.SummaryAdapter;
import com.kgict.kds.event.RxRefreshTimeEvent;
import com.kgict.kds.utils.Dlog;
import com.kgict.kds.utils.Utils;

import org.apache.commons.collections4.MapUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class OrderView extends ConstraintLayout {

    MainActivity activity;

    ConstraintLayout blankCoverLayout;
    ConstraintLayout summaryLayout;
    ConstraintLayout orderLayout;
    ConstraintLayout layout;
    TextView orderNumText;
    LinearLayout barLayout;
    ConstraintLayout barLayout2;
    TextView posNumText;
    TextView stateText;
    TextView deviceType;
    TextView timeText;
    TextView takeText;
    TextView callNumText;
    RecyclerView orderListView;
    View listBackground;
    TextView cancelText;
    ImageView imageView;
    View barLine1;
    View barLine2;
    View barLayoutLine;

    TextView summaryCountText;

    @BindView(R.id.debugText)
    TextView debugText;

    MenuListAdapter adapter;

    public OrderViewData orderViewData;
    public Order order;
    public int displayType = Constants.DISPLAY_TYPE_NONE;
    public int orderIndex = -1;        // 들어가는 아이템의 index
    public int manyIndex = -1;         // 다량주문일경우 다량내에서의 index
    public int pageIndex = -1;

    public int colorWhite;

    public OrderView(MainActivity context) {
        super(context);
        this.activity = context;
        initView();
    }

    public OrderView(MainActivity context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = context;
        initView();
    }

    public OrderView(MainActivity context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.activity = context;
        initView();
    }

    public void initView() {

        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = li.inflate(R.layout.layout_order, this, false);
        addView(view);

        layout = findViewById(R.id.layout);
        blankCoverLayout = findViewById(R.id.blankCoverLayout);
        summaryLayout = findViewById(R.id.summaryLayout);
        orderLayout = findViewById(R.id.orderLayout);
        orderNumText = findViewById(R.id.orderNum);
        barLayout = findViewById(R.id.barLayout1);
        barLayout2 = findViewById(R.id.barLayout2);
        posNumText = findViewById(R.id.posNum);
        deviceType = findViewById(R.id.deviceType);
        stateText = findViewById(R.id.state);
        timeText = findViewById(R.id.time);
        takeText = findViewById(R.id.takeText);
        callNumText = findViewById(R.id.callNum);
        listBackground = findViewById(R.id.listBackground);
        orderListView = findViewById(R.id.recyclerView);
        imageView = findViewById(R.id.imageView);
        cancelText = findViewById(R.id.cancelText);
        summaryCountText = findViewById(R.id.summaryCount);
        barLine1 = findViewById(R.id.barLine1);
        barLine2 = findViewById(R.id.barLine2);
        barLayoutLine = findViewById(R.id.barLayoutLine);
        debugText = findViewById(R.id.debugText);

        adapter = new MenuListAdapter(activity);
        LinearLayoutManager lm = new LinearLayoutManager(activity);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        orderListView.setLayoutManager(lm);
        orderListView.setAdapter(adapter);

        if (Configure.getOrderMaxAmount() > 10) {
            timeText.setTextSize(Utils.dimenToPx(R.dimen._7ssp));
            timeText.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) deviceType.getLayoutParams();
            lp.width = Utils.dimenToPx(R.dimen._15sdp);
            deviceType.setTextSize(Utils.dimenToPx(R.dimen.fontS));
        } else {
            timeText.setTextSize(Utils.dimenToPx(R.dimen.fontM));
            timeText.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            deviceType.setTextSize(Utils.dimenToPx(R.dimen.fontM));
        }

        if (Configure.logoFl == null || (Configure.logoFl != null && Configure.logoFl.equals("0")))
            imageView.setImageResource(0);
        else if (activity.mApp.getLogoFile() != null && activity.mApp.getLogoFile().length() != 0)
            Glide.with(this).load(activity.mApp.getLogoFile()).into(imageView);
        else
            Glide.with(this).load(R.drawable.kfc_logo).into(imageView);

        RxRefreshTimeEvent.getInstance().getBus()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(str -> {
                    if (displayType == Constants.DISPLAY_TYPE_ORDER_NORMAL
                            || displayType == Constants.DISPLAY_TYPE_ORDER_START
                            || displayType == Constants.DISPLAY_TYPE_ORDER_CENTER
                            || displayType == Constants.DISPLAY_TYPE_ORDER_END)
                        refreshTime();
                });

        colorWhite = ContextCompat.getColor(activity, R.color.fontWhite);
    }

    public void setOrderNum(String orderNum) {
        orderNumText.setText(orderNum);
    }

    /**
     * 주문뷰 만들기
     *
     * @param order
     */
    public void setOrder(Order order) {

        manyIndex = orderViewData.getManyMenuIndex();
        displayType = orderViewData.getDisplayType();

        orderLayout.setVisibility(VISIBLE);
        orderListView.setVisibility(VISIBLE);
        listBackground.setVisibility(VISIBLE);
        summaryLayout.setVisibility(GONE);
        blankCoverLayout.setVisibility(GONE);

        if (activity.mApp.isDebug) {
            debugText.setVisibility(VISIBLE);
            debugText.setText(" id>>" + order.getOrderId() + "  saleYmd>>" + order.getSaleYmd() + "\n"
                    + " callNum>>" + order.getCallNum() + " device>>" + order.getRegDevice() + "\n"
                    + " displayType>>" + displayType + " manyIndex>>" + manyIndex);
        } else {
            debugText.setVisibility(GONE);
        }

        // 배경색
        switch (order.getRegDevice()) {
            case Constants.DEVICES_TYPE_1:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor1);
                deviceType.setText(Configure.saleTypeTitle1);
                break;
            case Constants.DEVICES_TYPE_2:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor2);
                deviceType.setText(Configure.saleTypeTitle2);
                break;
            case Constants.DEVICES_TYPE_3:
                layout.setBackgroundColor(Configure.deliveryBackgroundColor);
                deviceType.setText(Configure.deliveryTitle);
                break;
            case Constants.DEVICES_TYPE_4:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor4);
                deviceType.setText(Configure.saleTypeTitle4);
                break;
            case Constants.DEVICES_TYPE_5:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor5);
                deviceType.setText(Configure.saleTypeTitle5);
                break;
            case Constants.DEVICES_TYPE_6:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor6);
                deviceType.setText(Configure.saleTypeTitle6);
                break;
            case Constants.DEVICES_TYPE_7:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor7);
                deviceType.setText(Configure.saleTypeTitle7);
                break;
            case Constants.DEVICES_TYPE_8:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor8);
                deviceType.setText(Configure.saleTypeTitle8);
                break;
            case Constants.DEVICES_TYPE_9:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor9);
                deviceType.setText(Configure.saleTypeTitle9);
                break;
            case Constants.DEVICES_TYPE_10:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor10);
                deviceType.setText(Configure.saleTypeTitle10);
                break;
            default:
                layout.setBackgroundColor(Configure.saleTypeBackgroundColor1);
                break;
        }
        if (order.isRevert() == 1) {
            layout.setBackgroundColor(Configure.revertBackgroundColor);
        }
        // 취소 표시
        if (order.isCancel() == 1) {
            layout.setBackgroundColor(Configure.cancelBackgroundColor);
            cancelText.setTextColor(Configure.cancelBackgroundColor);
            cancelText.setVisibility(VISIBLE);
        } else {
            cancelText.setVisibility(GONE);
        }

        // 포스번호 표시
        posNumText.setText(String.valueOf(order.getRegDeviceNum()) + "-" + order.getReceiptNum());

        stateText.setVisibility(INVISIBLE);
        // 현재 상태 표시
        if (order.getOrderState() == 0) {
            stateText.setTextColor(ContextCompat.getColor(activity, R.color.sky));
            stateText.setText("주문");
        } else if (order.getOrderState() == 1) {
            stateText.setTextColor(ContextCompat.getColor(activity, R.color.darkYellow));
            stateText.setText("결제");
        } else if (order.getOrderState() == 2) {
            stateText.setTextColor(ContextCompat.getColor(activity, R.color.green));
            stateText.setText("조리완료");
        } else if (order.getOrderState() == 3) {
            stateText.setTextColor(ContextCompat.getColor(activity, R.color.lightPurple));
            stateText.setText("포장완료");
        } else {
            stateText.setTextColor(ContextCompat.getColor(activity, R.color.green));
            stateText.setText("완료");
        }

        // 포장 여부 표시
        if (Configure.takeType != null && Configure.takeType.equals("1")) {
            takeText.setText(order.getKitchenMemo());
            takeText.setVisibility(VISIBLE);
        } else {
            takeText.setBackgroundResource(R.drawable.rounded_corners_25dp);
            GradientDrawable takeDrawable = (GradientDrawable) takeText.getBackground();
            switch (order.getSaleType()) {
                case "T":
                    takeDrawable.setColor(Configure.takeTextBackgroundColor);
                    takeText.setTextColor(0xff143005);
                    takeText.setText("포장");
                    break;
                case "H":
                case "D":
                case "C":
                default:
                    takeText.setVisibility(INVISIBLE);
                    break;
            }
        }

        // 영수 번호 표시
        if (order.getCallNum().length() > 4)
            callNumText.setText("000");
        else
            callNumText.setText(order.getCallNum());

        // 경과시간 표시
        SimpleDateFormat dt = new SimpleDateFormat("mm:ss");
        SimpleDateFormat dt2 = new SimpleDateFormat("MM-dd HH:mm:ss");
        Date now = new Date();
        Date diffDate = new Date(now.getTime() - order.getRegDate().getTime());
//        timeText.setText(dt.format(diffDate));

        long sec = diffDate.getTime() / 1000 % 60;
        long min = diffDate.getTime() / (1000 * 60);

        timeText.setText(String.format("%02d", min) + ":" + String.format("%02d", sec));

        orderListView.setBackgroundColor(ContextCompat.getColor(activity, R.color.black));

        if ((diffDate.getTime() / 1000) > Configure.delaySecond4) {
            if (Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor4);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor4);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond3) {
            if (Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor3);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor3);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond2) {
            if (Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor2);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor2);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond1) {
            if (Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor1);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor1);
            }
        } else {
            timeText.setTextColor(colorWhite);
            orderListView.setBackgroundColor(ContextCompat.getColor(activity, R.color.black));
        }

        // 다량메뉴 표기
        setManyMenuOrder(order);


        /////////////////////////////// 아이템 리스트 갱신 //////////////////////////////////
        // 일반 주문일 경우
        if (displayType == Constants.DISPLAY_TYPE_ORDER_NORMAL && manyIndex < 0) {
            adapter.setDataList(order.getList());
        }
        // 다량 주문일 경우
        else {
            List<OrderMenu> list = new ArrayList<>();
            for (int i = 0; i < Configure.menuMaximumCount; i++) {
                if (i + manyIndex * Configure.menuMaximumCount >= order.getList().size())
                    break;
                list.add(order.getList().get(i + manyIndex * Configure.menuMaximumCount));
            }
            adapter.setDataList(list);
        }

        if (order.getOrderState() == 4) {
            adapter.setFontColor(Configure.menuFontColor);
        } else {
            adapter.setFontColor(Configure.menuFontColor);
        }
        adapter.notifyDataSetChanged();
        this.order = order;

        if (Configure.interfaceType.equals("T"))
            setListener();
    }

    /**
     * 다량메뉴 표기
     *
     * @param order
     */
    public void setManyMenuOrder(Order order) {
//        Dlog.i("currentIndex >>> " + getIndex() + "    rootIndex >>> " + order.getManyMenuRootIndex());

        LayoutParams recyclerLp = (LayoutParams) orderListView.getLayoutParams();
        ConstraintLayout.LayoutParams layoutLp = (ConstraintLayout.LayoutParams) layout.getLayoutParams();
        LayoutParams barLayout2Lp = (LayoutParams) barLayout2.getLayoutParams();

        // 다량메뉴 시작일때
        if (displayType == Constants.DISPLAY_TYPE_ORDER_START) {
            recyclerLp.setMargins(1, recyclerLp.bottomToTop, 1, 0);
            layoutLp.setMargins(layoutLp.leftMargin, layoutLp.topMargin, 0, layoutLp.bottomMargin);
            barLayout2Lp.setMargins(1, barLayout2Lp.topMargin, 1, barLayout2Lp.bottomMargin);
            if(Configure.useOrderNum.equals("1"))
                orderNumText.setVisibility(VISIBLE);
            else
                orderNumText.setVisibility(GONE);
            posNumText.setVisibility(VISIBLE);
//            stateText.setVisibility(VISIBLE);
            takeText.setVisibility(INVISIBLE);
            callNumText.setVisibility(INVISIBLE);
            timeText.setVisibility(INVISIBLE);
            deviceType.setVisibility(INVISIBLE);
            barLine1.setVisibility(INVISIBLE);
            barLine2.setVisibility(INVISIBLE);
            displayType = Constants.DISPLAY_TYPE_ORDER_START;
        }
        //다량메뉴 끝일때
        else if (displayType == Constants.DISPLAY_TYPE_ORDER_END) {
            recyclerLp.setMargins(1, recyclerLp.bottomToTop, 1, 0);
            layoutLp.setMargins(0, layoutLp.topMargin, layoutLp.rightMargin, layoutLp.bottomMargin);
            barLayout2Lp.setMargins(1, barLayout2Lp.bottomToTop, 1, barLayout2Lp.bottomMargin);
            if(Configure.useOrderNum.equals("1"))
                orderNumText.setVisibility(INVISIBLE);
            else
                orderNumText.setVisibility(GONE);
            posNumText.setVisibility(INVISIBLE);
//            stateText.setVisibility(INVISIBLE);
            callNumText.setVisibility(VISIBLE);
            barLine1.setVisibility(VISIBLE);
            barLine2.setVisibility(VISIBLE);
            timeText.setVisibility(VISIBLE);
            deviceType.setVisibility(VISIBLE);
            if (order.getSaleType() != null && order.getSaleType().equals("T")
                    && Configure.takeType != null && !Configure.takeType.equals("1"))
                takeText.setVisibility(VISIBLE);
            displayType = Constants.DISPLAY_TYPE_ORDER_END;
        }
        // 다량메뉴 중간일 때 (양쪽 마진 다 없앰)
        else if (displayType == Constants.DISPLAY_TYPE_ORDER_CENTER) {
            recyclerLp.setMargins(1, recyclerLp.bottomToTop, 1, 0);
            layoutLp.setMargins(0, layoutLp.topMargin, 0, layoutLp.bottomMargin);
            barLayout2Lp.setMargins(1, barLayout2Lp.bottomToTop, 1, barLayout2Lp.bottomMargin);
            displayType = Constants.DISPLAY_TYPE_ORDER_CENTER;
            if(Configure.useOrderNum.equals("1"))
                orderNumText.setVisibility(INVISIBLE);
            else
                orderNumText.setVisibility(GONE);
            posNumText.setVisibility(INVISIBLE);
            stateText.setVisibility(INVISIBLE);
            takeText.setVisibility(INVISIBLE);
            callNumText.setVisibility(INVISIBLE);
            timeText.setVisibility(INVISIBLE);
            deviceType.setVisibility(INVISIBLE);
            barLine1.setVisibility(INVISIBLE);
            barLine2.setVisibility(INVISIBLE);
        }
        // 다량 아닌 메뉴일 때
        else {
            resetMargin();
            if(Configure.useOrderNum.equals("1"))
                orderNumText.setVisibility(VISIBLE);
            else
                orderNumText.setVisibility(GONE);
            posNumText.setVisibility(VISIBLE);
//            stateText.setVisibility(VISIBLE);
            if (order.getSaleType() != null && order.getSaleType().equals("T")
                    && Configure.takeType != null && !Configure.takeType.equals("1"))
                takeText.setVisibility(VISIBLE);
            callNumText.setVisibility(VISIBLE);
            timeText.setVisibility(VISIBLE);
            deviceType.setVisibility(VISIBLE);
            barLine1.setVisibility(VISIBLE);
            barLine2.setVisibility(VISIBLE);
        }


        listBackground.setLayoutParams(recyclerLp);
        orderListView.setLayoutParams(recyclerLp);
        layout.setLayoutParams(layoutLp);
        barLayout2.setLayoutParams(barLayout2Lp);
    }

    /**
     * 빈주문칸 만들기
     */
    public void setBlankOrder() {
        if (displayType != Constants.DISPLAY_TYPE_BLANK || displayType != Constants.DISPLAY_TYPE_ORDER_NORMAL)
            resetMargin();
        layout.setBackgroundColor(ContextCompat.getColor(activity, R.color.gray50));

        blankCoverLayout.setBackgroundColor(Color.parseColor("#" + Configure.backgroundColor));

        blankCoverLayout.setVisibility(VISIBLE);
        summaryLayout.setVisibility(GONE);
        orderLayout.setVisibility(GONE);
        orderListView.setVisibility(GONE);
        listBackground.setVisibility(GONE);
        cancelText.setVisibility(GONE);

        displayType = Constants.DISPLAY_TYPE_BLANK;
    }

    /**
     * Summary 화면 만들기
     *
     * @param orderList
     */
    public void setSummary(List<Order> orderList, boolean isSound) {

        // Summary레이아웃 그리기
        if (displayType != Constants.DISPLAY_TYPE_SUMMARY) {
            resetMargin();
            blankCoverLayout.setVisibility(GONE);
            orderLayout.setVisibility(GONE);
            cancelText.setVisibility(GONE);
            summaryLayout.setVisibility(VISIBLE);
            orderListView.setVisibility(VISIBLE);
            listBackground.setVisibility(VISIBLE);

            // BarLayoutLine 반으로 줄이기
            LayoutParams barLayoutLineLp = (LayoutParams) barLayoutLine.getLayoutParams();
            int half = Utils.dimenToPx(R.dimen.orderBarHeightHalf);
            barLayoutLineLp.setMargins(0, half, 0, 0);
            barLayoutLine.setLayoutParams(barLayoutLineLp);

            // 테두리, 마진 값, 패딩 없애기
            LayoutParams recyclerLp = (LayoutParams) orderListView.getLayoutParams();
            LayoutParams layoutLp = (LayoutParams) layout.getLayoutParams();
            LayoutParams barLayout2Lp = (LayoutParams) barLayout2.getLayoutParams();
            int layoutMargin = Utils.dimenToPx(R.dimen._1sdp);
            recyclerLp.setMargins(0, 0, 0, 0);
            layoutLp.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
            barLayout2Lp.setMargins(0, 0, 0, 0);
            orderListView.setLayoutParams(recyclerLp);
            listBackground.setLayoutParams(recyclerLp);
            layout.setLayoutParams(layoutLp);
            barLayout2.setLayoutParams(barLayout2Lp);

            layout.setPadding(0, 0, 0, 0);

            orderListView.setBackgroundColor(ContextCompat.getColor(activity, R.color.summary));
            layout.setBackgroundColor(ContextCompat.getColor(activity, R.color.summary));
            displayType = Constants.DISPLAY_TYPE_SUMMARY;

            ViewTreeObserver vto = getViewTreeObserver();
            ViewTreeObserver.OnPreDrawListener gll = () -> {

                if (activity.getCurrentPage() == pageIndex) {
                    Dlog.i("vto >>> " + activity.isUpdating + "   >>>>>   sound:" + isSound + "  pageIndex>>  " + pageIndex + "  activity >> " + activity.getCurrentPage());
                    if (activity.isUpdating) {
                        if (activity.isSound && Configure.kdsType != Constants.KDS_TYPE_PACKING) {
                            activity.mApp.soundPool.play(activity.mApp.soundId[0], .3f, .3f, 0, 0, 1f);
                            activity.isSound = false;
                        }
                        activity.setIsUpdating(false);
                        if (activity.getPageAmount() > 1) {
//                            activity.mainFragment.fetchOtherFragment();
                        }
                    }
                }
                return true;
            };
            vto.addOnPreDrawListener(gll);
        }

        if (orderList == null) {
            SummaryAdapter adapter = new SummaryAdapter(activity, new ArrayList<>());
            LinearLayoutManager lm = new LinearLayoutManager(activity);
            lm.setOrientation(LinearLayoutManager.VERTICAL);
            orderListView.setLayoutManager(lm);
            orderListView.setAdapter(adapter);
            return;
        }

        // 합친 메뉴 표시
        Map<String, String> categoryMap = new HashMap<>();
        Map<String, Integer> summaryMap = new HashMap<>();
        for (Order order : orderList) {
            if (order.isCancel() == 0) {
                for (OrderMenu menu : order.getList()) {
                    // 자재 표기
                    if(Configure.summaryMenuType.equals("1")) {
                        if (menu.getQty() > 0) {
                            // 자재 있을 시
                            if(menu.getSetItemList().size() > 0) {
                                for(SubItem subItem : menu.getSetItemList()) {
                                    String subName = subItem.getName();
                                    Dlog.i(">>>" + subItem.toString());
                                    if (subName == null || subName.length() == 0)
                                        subName = subItem.getItemCd();
                                    int count = MapUtils.getInteger(summaryMap, subName, 0);
                                    summaryMap.put(subName, count + (menu.getQty() * subItem.getQty()));
//                                    categoryMap.put(subName, subItem.getGrpCd());
                                    categoryMap.put(subName, menu.getItemGrpCd());
                                }
                            } else {// 자재 없을시
                                /*
                                String name = menu.getName();
                                if (name == null || name.length() == 0)
                                    name = menu.getItemCode();
                                int count = MapUtils.getInteger(summaryMap, name, 0);
                                summaryMap.put(name, count + menu.getQty());
                                categoryMap.put(name, menu.getItemGrpCd());
                                */
                            }
                        }
                    } else {    // 메뉴 표기
                        if (menu.getQty() > 0) {
                            String name = menu.getName();
                            if (name == null || name.length() == 0)
                                name = menu.getItemCode();
                            int count = MapUtils.getInteger(summaryMap, name, 0);
                            summaryMap.put(name, count + menu.getQty());
                            categoryMap.put(name, menu.getItemGrpCd());
                        }
                    }
                }
            }
        }


        List<OrderMenu> menuList = new ArrayList<>();
        int sumCount = 0;
        Iterator it = sortByValue(summaryMap).iterator();
        while (it.hasNext()) {
            String name = (String) it.next();
            int qty = summaryMap.get(name);
            String itemGrpCode = categoryMap.get(name);
            menuList.add(new OrderMenu(name, 0, summaryMap.get(name), itemGrpCode, 1));
            sumCount += qty;
        }

        // 합계 표시
        summaryCountText.setText(String.valueOf("합계 " + sumCount));
        summaryCountText.setVisibility(View.GONE);

        SummaryAdapter adapter = new SummaryAdapter(activity, menuList);
        LinearLayoutManager lm = new LinearLayoutManager(activity);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        orderListView.setLayoutManager(lm);
        orderListView.setAdapter(adapter);
    }

    /**
     * margin 복구
     */
    public void resetMargin() {
        LayoutParams recyclerLp = (LayoutParams) orderListView.getLayoutParams();
        ConstraintLayout.LayoutParams layoutLp = (ConstraintLayout.LayoutParams) layout.getLayoutParams();
        LayoutParams barLayout2Lp = (LayoutParams) barLayout2.getLayoutParams();
        int layoutMargin = Utils.dimenToPx(R.dimen._1sdp);
        int barLayout2Margin = Utils.dimenToPx(R.dimen._1sdp);
        recyclerLp.setMargins(1, 0, 1, 0);
        layoutLp.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
        barLayout2Lp.setMargins(1, 0, 1, 0);
        orderListView.setLayoutParams(recyclerLp);
        layout.setLayoutParams(layoutLp);
        barLayout2.setLayoutParams(barLayout2Lp);
    }

    public void refreshTime() {
        // 경과시간 표시);
        Date now = new Date();
        Date diffDate = new Date(now.getTime() - order.getRegDate().getTime());

        long sec = diffDate.getTime() / 1000 % 60;
        long min = diffDate.getTime() / (1000 * 60);

        timeText.setText(String.format("%02d", min) + ":" + String.format("%02d", sec));
        if ((diffDate.getTime() / 1000) > Configure.delaySecond4) {
            if (Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor4);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor4);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond3) {
            if (Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor3);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor3);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond2) {
            if (Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor2);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor2);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond1) {
            if (Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor1);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor1);
            }
        } else {
            timeText.setTextColor(colorWhite);
            orderListView.setBackgroundColor(ContextCompat.getColor(activity, R.color.black));
        }
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    //별도의 스태틱 함수로 구현

    public List sortByValue(final Map map) {

        List<String> list = new ArrayList();

        list.addAll(map.keySet());
        Collections.sort(list, new Comparator() {

            public int compare(Object o1, Object o2) {
                Object v1 = map.get(o1);
                Object v2 = map.get(o2);
                return ((Comparable) v2).compareTo(v1);
            }
        });
//        Collections.reverse(list); // 주석시 오름차순
        return list;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrderViewData(OrderViewData orderViewData) {
        this.orderViewData = orderViewData;
    }

    public OrderViewData getOrderViewData() {
        return orderViewData;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public OrderView getBlackOrderViewInstance() {
        OrderView orderView = new OrderView(activity);
        if (displayType != Constants.DISPLAY_TYPE_BLANK || displayType != Constants.DISPLAY_TYPE_ORDER_NORMAL)
            resetMargin();
        layout.setBackgroundColor(ContextCompat.getColor(activity, R.color.gray50));

        blankCoverLayout.setBackgroundColor(Color.parseColor("#" + Configure.backgroundColor));

        blankCoverLayout.setVisibility(VISIBLE);
        summaryLayout.setVisibility(GONE);
        orderLayout.setVisibility(GONE);
        orderListView.setVisibility(GONE);
        cancelText.setVisibility(GONE);

        displayType = Constants.DISPLAY_TYPE_BLANK;
        return orderView;
    }

    public void setListener() {
        if (getOrder() == null) {
            return;
        }

        orderLayout.setOnClickListener(v -> {
            String state = "3";
            if (Configure.kdsType == Constants.KDS_TYPE_KITCHEN) {
                state = "2";
            } else if (getOrder().getSaleType().equals("H") || getOrder().isCancel() == 1) {
                state = "4";
            }
            activity.mApp.getNetworkService().changeProcState(getOrder().getOrderId(), getOrder().getSaleYmd(), state)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(retMap -> {
                        if(Configure.kdsType == Constants.KDS_TYPE_KITCHEN && Configure.useKitchenPrint.equals("1")) {
                            activity.updateCallState(order.receiptNum, "6");
                        }
                        // 바뀐 메뉴 한번 확인하기
                        activity.downloadOrderList(false);
                    }, throwable -> {
                        Dlog.e(throwable.getMessage(), throwable);
                        activity.mApp.showToast(throwable.getMessage());
                    });
        });
    }

    @Override
    public String toString() {
        return "OrderView{" +
                "activity=" + activity +
                ", blankCoverLayout=" + blankCoverLayout +
                ", summaryLayout=" + summaryLayout +
                ", orderLayout=" + orderLayout +
                ", layout=" + layout +
                ", orderNumText=" + orderNumText +
                ", barLayout=" + barLayout +
                ", barLayout2=" + barLayout2 +
                ", posNumText=" + posNumText +
                ", stateText=" + stateText +
                ", deviceType=" + deviceType +
                ", timeText=" + timeText +
                ", takeText=" + takeText +
                ", callNumText=" + callNumText +
                ", orderListView=" + orderListView +
                ", cancelText=" + cancelText +
                ", barLine1=" + barLine1 +
                ", barLine2=" + barLine2 +
                ", barLayoutLine=" + barLayoutLine +
                ", summaryCountText=" + summaryCountText +
                ", debugText=" + debugText +
                ", adapter=" + adapter +
                ", orderViewData=" + orderViewData +
                ", displayType=" + displayType +
                ", orderIndex=" + orderIndex +
                ", manyIndex=" + manyIndex +
                '}';
    }
}
