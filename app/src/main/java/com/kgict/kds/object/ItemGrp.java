package com.kgict.kds.object;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItemGrp {
    @JsonProperty("itemGrpCode")
    String code;

    @JsonProperty("itemGrpNm")
    String name;

    @JsonProperty("itemGrpOrd")
    int order;

    @JsonProperty("itemGrpBg")
    String bg;

    int bgColor = -1;

    @Override
    public String toString() {
        return "ItemGrp{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", order=" + order +
                ", bg='" + bg + '\'' +
                '}';
    }

    public ItemGrp() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public int getColor() {
        try {
            if (bgColor < 0) {
                bgColor = Color.parseColor("#" + bg);
            }
        } catch (Exception e) {
            bgColor = -1;
            e.printStackTrace();
        }
        return bgColor;
    }
}
