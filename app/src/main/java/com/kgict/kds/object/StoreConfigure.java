package com.kgict.kds.object;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StoreConfigure {

    @JsonProperty("kdsCtlId")
    long kdsCtlId;         // 매장코드

    @JsonProperty("storeNm")
    String storeNm;          // 매장이름

    @JsonProperty("kdsRefreshCycle")
    int kdsRefreshCycle;          // 화면 주기

    @JsonProperty("storeYmd")
    String saleYmd;

    @JsonProperty("summaryTitle")
    String summaryTitle;

    @JsonProperty("logoFl")
    String logoFl;      // 로고 사용여부

    @JsonProperty("lastUpdateDt")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyyMMddHHmmss", timezone="GMT+9")
    Date lastUpdateDt;          // 상품 업데이트 날짜

    @JsonProperty("saleTypeNm1")
    String saleTypeNm1;               // 주문채널1 이름
    @JsonProperty("saleTypeNm2")
    String saleTypeNm2;               // 주문채널2 이름
    @JsonProperty("saleTypeNm3")
    String saleTypeNm3;               // 주문채널3 이름
    @JsonProperty("saleTypeNm4")
    String saleTypeNm4;               // 주문채널4 이름
    @JsonProperty("saleTypeNm5")
    String saleTypeNm5;               // 주문채널5 이름
    @JsonProperty("saleTypeNm6")
    String saleTypeNm6;               // 주문채널6 이름
    @JsonProperty("saleTypeNm7")
    String saleTypeNm7;               // 주문채널7 이름
    @JsonProperty("saleTypeNm8")
    String saleTypeNm8;               // 주문채널8 이름
    @JsonProperty("saleTypeNm9")
    String saleTypeNm9;               // 주문채널9 이름
    @JsonProperty("saleTypeNm10")
    String saleTypeNm10;               // 주문채널10 이름
    @JsonProperty("saleTypeFl1")
    String saleTypeFl1;               // 주문채널1 사용여부
    @JsonProperty("saleTypeFl2")
    String saleTypeFl2;               // 주문채널2 사용여부
    @JsonProperty("saleTypeFl3")
    String saleTypeFl3;               // 주문채널3 사용여부
    @JsonProperty("saleTypeFl4")
    String saleTypeFl4;               // 주문채널4 사용여부
    @JsonProperty("saleTypeFl5")
    String saleTypeFl5;               // 주문채널5 사용여부
    @JsonProperty("saleTypeFl6")
    String saleTypeFl6;               // 주문채널6 사용여부
    @JsonProperty("saleTypeFl7")
    String saleTypeFl7;               // 주문채널7 사용여부
    @JsonProperty("saleTypeFl8")
    String saleTypeFl8;               // 주문채널8 사용여부
    @JsonProperty("saleTypeFl9")
    String saleTypeFl9;               // 주문채널9 사용여부
    @JsonProperty("saleTypeFl10")
    String saleTypeFl10;               // 주문채널10 사용여부
    @JsonProperty(value = "deliveryFl")
    int deliveryFl;     //딜리버리 사용 여부, ("Y" 경우에만 패킹 KDS에 (배달)주문정보 표시)
    @JsonProperty(value = "deliveryAgencyFl")
    int deliveryAgencyFl;       //딜리버리 대행 사용 여부 Y의 경우에만 요청/픽업 버튼 활성화


    @Override
    public String toString() {
        return "StoreConfigure{" +
                "kdsCtlId=" + kdsCtlId +
                ", storeNm='" + storeNm + '\'' +
                ", kdsRefreshCycle=" + kdsRefreshCycle +
                ", saleYmd='" + saleYmd + '\'' +
                ", summaryTitle='" + summaryTitle + '\'' +
                ", logoFl='" + logoFl + '\'' +
                ", lastUpdateDt=" + lastUpdateDt +
                ", saleTypeNm1='" + saleTypeNm1 + '\'' +
                ", saleTypeNm2='" + saleTypeNm2 + '\'' +
                ", saleTypeNm3='" + saleTypeNm3 + '\'' +
                ", saleTypeNm4='" + saleTypeNm4 + '\'' +
                ", saleTypeNm5='" + saleTypeNm5 + '\'' +
                ", saleTypeNm6='" + saleTypeNm6 + '\'' +
                ", saleTypeNm7='" + saleTypeNm7 + '\'' +
                ", saleTypeNm8='" + saleTypeNm8 + '\'' +
                ", saleTypeNm9='" + saleTypeNm9 + '\'' +
                ", saleTypeNm10='" + saleTypeNm10 + '\'' +
                ", saleTypeFl1='" + saleTypeFl1 + '\'' +
                ", saleTypeFl2='" + saleTypeFl2 + '\'' +
                ", saleTypeFl3='" + saleTypeFl3 + '\'' +
                ", saleTypeFl4='" + saleTypeFl4 + '\'' +
                ", saleTypeFl5='" + saleTypeFl5 + '\'' +
                ", saleTypeFl6='" + saleTypeFl6 + '\'' +
                ", saleTypeFl7='" + saleTypeFl7 + '\'' +
                ", saleTypeFl8='" + saleTypeFl8 + '\'' +
                ", saleTypeFl9='" + saleTypeFl9 + '\'' +
                ", saleTypeFl10='" + saleTypeFl10 + '\'' +
                ", deliveryFl'" + deliveryFl +
                ", deliveryAgencyFl='" + deliveryAgencyFl +
                '}';
    }

    public long getKdsCtlId() {
        return kdsCtlId;
    }

    public void setKdsCtlId(long kdsCtlId) {
        this.kdsCtlId = kdsCtlId;
    }

    public String getStoreNm() {
        return storeNm;
    }

    public void setStoreNm(String storeNm) {
        this.storeNm = storeNm;
    }

    public String getSaleYmd() {
        return saleYmd;
    }

    public void setSaleYmd(String saleYmd) {
        this.saleYmd = saleYmd;
    }

    public String getSummaryTitle() {
        return summaryTitle;
    }

    public void setSummaryTitle(String summaryTitle) {
        this.summaryTitle = summaryTitle;
    }

    public String getLogoFl() {
        return logoFl;
    }

    public void setLogoFl(String logoFl) {
        this.logoFl = logoFl;
    }

    public int getKdsRefreshCycle() {
        return kdsRefreshCycle;
    }

    public void setKdsRefreshCycle(int kdsRefreshCycle) {
        this.kdsRefreshCycle = kdsRefreshCycle;
    }

    public Date getLastUpdateDt() {
        return lastUpdateDt;
    }

    public void setLastUpdateDt(Date lastUpdateDt) {
        this.lastUpdateDt = lastUpdateDt;
    }

    public String getSaleTypeNm1() {
        return saleTypeNm1;
    }

    public void setSaleTypeNm1(String saleTypeNm1) {
        this.saleTypeNm1 = saleTypeNm1;
    }

    public String getSaleTypeNm2() {
        return saleTypeNm2;
    }

    public void setSaleTypeNm2(String saleTypeNm2) {
        this.saleTypeNm2 = saleTypeNm2;
    }

    public String getSaleTypeNm3() {
        return saleTypeNm3;
    }

    public void setSaleTypeNm3(String saleTypeNm3) {
        this.saleTypeNm3 = saleTypeNm3;
    }

    public String getSaleTypeNm4() {
        return saleTypeNm4;
    }

    public void setSaleTypeNm4(String saleTypeNm4) {
        this.saleTypeNm4 = saleTypeNm4;
    }

    public String getSaleTypeNm5() {
        return saleTypeNm5;
    }

    public void setSaleTypeNm5(String saleTypeNm5) {
        this.saleTypeNm5 = saleTypeNm5;
    }

    public String getSaleTypeNm6() {
        return saleTypeNm6;
    }

    public void setSaleTypeNm6(String saleTypeNm6) {
        this.saleTypeNm6 = saleTypeNm6;
    }

    public String getSaleTypeNm7() {
        return saleTypeNm7;
    }

    public void setSaleTypeNm7(String saleTypeNm7) {
        this.saleTypeNm7 = saleTypeNm7;
    }

    public String getSaleTypeNm8() {
        return saleTypeNm8;
    }

    public void setSaleTypeNm8(String saleTypeNm8) {
        this.saleTypeNm8 = saleTypeNm8;
    }

    public String getSaleTypeNm9() {
        return saleTypeNm9;
    }

    public void setSaleTypeNm9(String saleTypeNm9) {
        this.saleTypeNm9 = saleTypeNm9;
    }

    public String getSaleTypeNm10() {
        return saleTypeNm10;
    }

    public void setSaleTypeNm10(String saleTypeNm10) {
        this.saleTypeNm10 = saleTypeNm10;
    }

    public String getSaleTypeFl1() {
        return saleTypeFl1;
    }

    public void setSaleTypeFl1(String saleTypeFl1) {
        this.saleTypeFl1 = saleTypeFl1;
    }

    public String getSaleTypeFl2() {
        return saleTypeFl2;
    }

    public void setSaleTypeFl2(String saleTypeFl2) {
        this.saleTypeFl2 = saleTypeFl2;
    }

    public String getSaleTypeFl3() {
        return saleTypeFl3;
    }

    public void setSaleTypeFl3(String saleTypeFl3) {
        this.saleTypeFl3 = saleTypeFl3;
    }

    public String getSaleTypeFl4() {
        return saleTypeFl4;
    }

    public void setSaleTypeFl4(String saleTypeFl4) {
        this.saleTypeFl4 = saleTypeFl4;
    }

    public String getSaleTypeFl5() {
        return saleTypeFl5;
    }

    public void setSaleTypeFl5(String saleTypeFl5) {
        this.saleTypeFl5 = saleTypeFl5;
    }

    public String getSaleTypeFl6() {
        return saleTypeFl6;
    }

    public void setSaleTypeFl6(String saleTypeFl6) {
        this.saleTypeFl6 = saleTypeFl6;
    }

    public String getSaleTypeFl7() {
        return saleTypeFl7;
    }

    public void setSaleTypeFl7(String saleTypeFl7) {
        this.saleTypeFl7 = saleTypeFl7;
    }

    public String getSaleTypeFl8() {
        return saleTypeFl8;
    }

    public void setSaleTypeFl8(String saleTypeFl8) {
        this.saleTypeFl8 = saleTypeFl8;
    }

    public String getSaleTypeFl9() {
        return saleTypeFl9;
    }

    public void setSaleTypeFl9(String saleTypeFl9) {
        this.saleTypeFl9 = saleTypeFl9;
    }

    public String getSaleTypeFl10() { return saleTypeFl10; }

    public void setSaleTypeFl10(String saleTypeFl10) { this.saleTypeFl10 = saleTypeFl10; }

    public int getDeliveryFl() { return deliveryFl; }

    public int getDeliveryAgencyFl() { return deliveryAgencyFl; }
}
