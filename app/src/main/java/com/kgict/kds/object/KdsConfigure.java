package com.kgict.kds.object;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KdsConfigure {

    @JsonProperty("kdsId")
    long kdsId;         // KDS id

    @JsonProperty("kdsTplId")
    long kdsTplId;          // 템플릿 id

    @JsonProperty("brandCd")        // 브랜드 코드
    String brandCd;

    @JsonProperty("storeCd")        // 매장 코드
    String kdsStoreCd;

    @JsonProperty("storeNm")        // 매장 명
    String kdsStoreNm;

    @JsonProperty("kdsUpdateUrl")       // KDS 패치 URL
    String kdsUpdateUrl;

    @JsonProperty("kdsNm")
    String kdsNm;          // KDS이름

    @JsonProperty("kdsClass")
    String kdsClass;          // KDS 타입 (0:매장용, 1:주방용, 2:팩킹용)

    @JsonProperty("displayCnt")
    int displayCnt;

    @JsonProperty("didFl")
    String didFl;

    @JsonProperty("didIp")
    String didIp;

    @JsonProperty("didPort1")
    String didPort1;
    @JsonProperty("didPort2")
    String didPort2;

    @JsonProperty("kdsVersion")
    String kdsVersion;

    @JsonProperty("lastRefreshTime")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyyMMddHHmmss", timezone="GMT+9")
    Date lastRefreshTime;     // 마지막 갱신시간(1분주기)

    @JsonProperty("rebootFl")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyyMMddHHmmss", timezone="GMT+9")
    String rebootFl;     // 재실행 여부 (0:미적용 1:재실행 - 1분마다 검사)

    @JsonProperty("fontSize")
    int fontSize;

    @JsonProperty("fontColor")
    String fontColor;

    @JsonProperty("useSummaryFl")
    int useSummaryFl;

    @JsonProperty("takeType")
    String takeType;

    @JsonProperty("summaryType")
    String summaryMenuType;

    @JsonProperty("useOrderNum")
    String useOrderNum;

    @JsonProperty("useKitchenPrint")
    String useKitchenPrint;

    @JsonProperty("categoryOrderFl")
    String categoryOrderFl;

    @JsonProperty("interfaceType")
    String interfaceType;      // 인터페이스타입 (B:범프키 T:터치), PACKING인경우 B:스크롤좌우, T:스크롤상하

    @JsonProperty("itemOrderType")
    String itemOrderType;

    @JsonProperty("singleItemFl")
    String singleItemFl;

    @JsonProperty("delayAlarmType")
    String delayType;
    @JsonProperty("delayAlarmFl")
    String delayAlarmFl;

    @JsonProperty("delayColor1")
    String delayColor1;
    @JsonProperty("delaySec1")
    String delaySec1;
    @JsonProperty("delayColor2")
    String delayColor2;
    @JsonProperty("delaySec2")
    String delaySec2;
    @JsonProperty("delayColor3")
    String delayColor3;
    @JsonProperty("delaySec3")
    String delaySec3;
    @JsonProperty("delayColor4")
    String delayColor4;
    @JsonProperty("delaySec4")
    String delaySec4;

    @JsonProperty("rvColor")
    String rvColor;
    @JsonProperty("cancelColor")
    String cancelColor;

    @JsonProperty("saleTypeColor1")
    String saleTypeColor1;
    @JsonProperty("saleTypeColor2")
    String saleTypeColor2;
    @JsonProperty("saleTypeColor3")
    String saleTypeColor3;
    @JsonProperty("saleTypeColor4")
    String saleTypeColor4;
    @JsonProperty("saleTypeColor5")
    String saleTypeColor5;
    @JsonProperty("saleTypeColor6")
    String saleTypeColor6;
    @JsonProperty("saleTypeColor7")
    String saleTypeColor7;
    @JsonProperty("saleTypeColor8")
    String saleTypeColor8;
    @JsonProperty("saleTypeColor9")
    String saleTypeColor9;
    @JsonProperty("saleTypeColor10")
    String saleTypeColor10;

    @JsonProperty("modDt")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyyMMddHHmmss", timezone="GMT+9")
    Date modDt;     // 주문 들어온 시간

    @Override
    public String toString() {
        return "KdsConfigure{" +
                "kdsId=" + kdsId +
                ", kdsTplId=" + kdsTplId +
                ", storeCd=" + kdsStoreCd + '\'' +
                ", storeNm=" + kdsStoreNm + '\'' +
                ", brandCd=" + brandCd + '\'' +
                ", kdsNm='" + kdsNm + '\'' +
                ", kdsUpdateUrl='" + kdsUpdateUrl + '\'' +
                ", kdsClass='" + kdsClass + '\'' +
                ", displayCnt=" + displayCnt +
                ", didFl='" + didFl + '\'' +
                ", didIp='" + didIp + '\'' +
                ", didPort1='" + didPort1 + '\'' +
                ", didPort2='" + didPort2 + '\'' +
                ", kdsVersion='" + kdsVersion + '\'' +
                ", lastRefreshTime=" + lastRefreshTime +
                ", rebootFl='" + rebootFl + '\'' +
                ", fontSize=" + fontSize +
                ", fontColor='" + fontColor + '\'' +
                ", useSummaryFl=" + useSummaryFl +
                ", takeType='" + takeType + '\'' +
                ", summaryMenuType='" + summaryMenuType + '\'' +
                ", useOrderNum='" + useOrderNum + '\'' +
                ", useKitchenPrint='" + useKitchenPrint + '\'' +
                ", categoryOrderFl='" + categoryOrderFl + '\'' +
                ", interfaceType='" + interfaceType + '\'' +
                ", itemOrderType='" + itemOrderType + '\'' +
                ", singleItemFl='" + singleItemFl + '\'' +
                ", delayType='" + delayType + '\'' +
                ", delayAlarmFl='" + delayAlarmFl + '\'' +
                ", delayColor1='" + delayColor1 + '\'' +
                ", delaySec1='" + delaySec1 + '\'' +
                ", delayColor2='" + delayColor2 + '\'' +
                ", delaySec2='" + delaySec2 + '\'' +
                ", delayColor3='" + delayColor3 + '\'' +
                ", delaySec3='" + delaySec3 + '\'' +
                ", delayColor4='" + delayColor4 + '\'' +
                ", delaySec4='" + delaySec4 + '\'' +
                ", rvColor='" + rvColor + '\'' +
                ", cancelColor='" + cancelColor + '\'' +
                ", saleTypeColor1='" + saleTypeColor1 + '\'' +
                ", saleTypeColor2='" + saleTypeColor2 + '\'' +
                ", saleTypeColor3='" + saleTypeColor3 + '\'' +
                ", saleTypeColor4='" + saleTypeColor4 + '\'' +
                ", saleTypeColor5='" + saleTypeColor5 + '\'' +
                ", saleTypeColor6='" + saleTypeColor6 + '\'' +
                ", saleTypeColor7='" + saleTypeColor7 + '\'' +
                ", saleTypeColor8='" + saleTypeColor8 + '\'' +
                ", saleTypeColor9='" + saleTypeColor9 + '\'' +
                ", saleTypeColor10='" + saleTypeColor10 + '\'' +
                ", modDt=" + modDt +
                '}';
    }

    public KdsConfigure() {
    }

    public void setBrandCd(String brandCd) { this.brandCd = brandCd; }

    public String getBrandCd() { return  brandCd; }

    public String getKdsStoreCd() { return kdsStoreCd; }

    public void setKdsStoreCd(String kdsStoreCd) { this.kdsStoreCd = kdsStoreCd; }

    public String getKdsStoreNm() { return  kdsStoreNm; }

    public void setKdsStoreNm(String kdsStoreNm) { this.kdsStoreNm = kdsStoreNm; }

    public String getKdsUpdateUrl() { return  kdsUpdateUrl;}

    public void setKdsUpdateUrl(String kdsUpdateUrl) {this.kdsUpdateUrl = kdsUpdateUrl;}

    public long getKdsId() {
        return kdsId;
    }

    public void setKdsId(long kdsId) {
        this.kdsId = kdsId;
    }

    public long getKdsTplId() {
        return kdsTplId;
    }

    public void setKdsTypeId(long kdsTypeId) {
        this.kdsTplId = kdsTypeId;
    }

    public String getKdsNm() {
        return kdsNm;
    }

    public void setKdsNm(String kdsNm) {
        this.kdsNm = kdsNm;
    }

    public String getKdsClass() {
        return kdsClass;
    }

    public void setKdsClass(String kdsClass) {
        this.kdsClass = kdsClass;
    }

    public void setKdsTplId(long kdsTplId) {
        this.kdsTplId = kdsTplId;
    }

    public int getDisplayCnt() {
        return displayCnt;
    }

    public void setDisplayCnt(int displayCnt) {
        this.displayCnt = displayCnt;
    }

    public int getUseSummaryFl() {
        return useSummaryFl;
    }

    public void setUseSummaryFl(int useSummaryFl) {
        this.useSummaryFl = useSummaryFl;
    }

    public String getSummaryMenuType() {
        return summaryMenuType;
    }

    public void setSummaryMenuType(String summaryMenuType) {
        this.summaryMenuType = summaryMenuType;
    }

    public String getUseOrderNum() {
        return useOrderNum;
    }

    public void setUseOrderNum(String useOrderNum) {
        this.useOrderNum = useOrderNum;
    }

    public String getUseKitchenPrint() {
        return useKitchenPrint;
    }

    public void setUseKitchenPrint(String useKitchenPrint) {
        this.useKitchenPrint = useKitchenPrint;
    }

    public String getCategoryOrderFl() {
        if(categoryOrderFl == null) {
            return "0";
        }
        return categoryOrderFl;
    }

    public void setCategoryOrderFl(String categoryOrderFl) {
        this.categoryOrderFl = categoryOrderFl;
    }

    public String getDidFl() {
        return didFl;
    }

    public void setDidFl(String didFl) {
        this.didFl = didFl;
    }

    public String getDidIp() {
        return didIp;
    }

    public void setDidIp(String didIp) {
        this.didIp = didIp;
    }

    public String getDidPort1() {
        return didPort1;
    }

    public void setDidPort1(String didPort1) {
        this.didPort1 = didPort1;
    }

    public String getDidPort2() {
        return didPort2;
    }

    public void setDidPort2(String didPort2) {
        this.didPort2 = didPort2;
    }

    public String getKdsVersion() {
        return kdsVersion;
    }

    public void setKdsVersion(String kdsVersion) {
        this.kdsVersion = kdsVersion;
    }

    public Date getLastRefreshTime() {
        return lastRefreshTime;
    }

    public void setLastRefreshTime(Date lastRefreshTime) {
        this.lastRefreshTime = lastRefreshTime;
    }

    public String getRebootFl() {
        return rebootFl;
    }

    public void setRebootFl(String rebootFl) {
        this.rebootFl = rebootFl;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public String getInterfaceType() {
        return interfaceType;
    }

    public void setInterfaceType(String interfaceType) {
        this.interfaceType = interfaceType;
    }

    public String getSaleTypeColor6() {
        return saleTypeColor6;
    }

    public void setSaleTypeColor6(String saleTypeColor6) {
        this.saleTypeColor6 = saleTypeColor6;
    }

    public String getSaleTypeColor7() {
        return saleTypeColor7;
    }

    public void setSaleTypeColor7(String saleTypeColor7) {
        this.saleTypeColor7 = saleTypeColor7;
    }

    public String getSaleTypeColor8() {
        return saleTypeColor8;
    }

    public void setSaleTypeColor8(String saleTypeColor8) {
        this.saleTypeColor8 = saleTypeColor8;
    }

    public String getSaleTypeColor9() {
        return saleTypeColor9;
    }

    public void setSaleTypeColor9(String saleTypeColor9) {
        this.saleTypeColor9 = saleTypeColor9;
    }

    public String getSaleTypeColor10() {
        return saleTypeColor10;
    }

    public void setSaleTypeColor10(String saleTypeColor10) {
        this.saleTypeColor10 = saleTypeColor10;
    }

    public String getTakeType() {
        return takeType;
    }

    public void setTakeType(String takeType) {
        this.takeType = takeType;
    }

    public String getItemOrderType() {
        return itemOrderType;
    }

    public void setItemOrderType(String itemOrderType) {
        this.itemOrderType = itemOrderType;
    }

    public String getSingleItemFl() {
        return singleItemFl;
    }

    public void setSingleItemFl(String singleItemFl) {
        this.singleItemFl = singleItemFl;
    }

    public String getDelayType() {
        return delayType;
    }

    public void setDelayType(String delayType) {
        this.delayType = delayType;
    }

    public String getDelayAlarmFl() {
        return delayAlarmFl;
    }

    public void setDelayAlarmFl(String delayAlarmFl) {
        this.delayAlarmFl = delayAlarmFl;
    }

    public String getDelayColor1() {
        return delayColor1;
    }

    public void setDelayColor1(String delayColor1) {
        this.delayColor1 = delayColor1;
    }

    public String getDelaySec1() {
        return delaySec1;
    }

    public void setDelaySec1(String delaySec1) {
        this.delaySec1 = delaySec1;
    }

    public String getDelayColor2() {
        return delayColor2;
    }

    public void setDelayColor2(String delayColor2) {
        this.delayColor2 = delayColor2;
    }

    public String getDelaySec2() {
        return delaySec2;
    }

    public void setDelaySec2(String delaySec2) {
        this.delaySec2 = delaySec2;
    }

    public String getDelayColor3() {
        return delayColor3;
    }

    public void setDelayColor3(String delayColor3) {
        this.delayColor3 = delayColor3;
    }

    public String getDelaySec3() {
        return delaySec3;
    }

    public void setDelaySec3(String delaySec3) {
        this.delaySec3 = delaySec3;
    }

    public String getDelayColor4() {
        return delayColor4;
    }

    public void setDelayColor4(String delayColor4) {
        this.delayColor4 = delayColor4;
    }

    public String getDelaySec4() {
        return delaySec4;
    }

    public void setDelaySec4(String delaySec4) {
        this.delaySec4 = delaySec4;
    }

    public String getRvColor() {
        return rvColor;
    }

    public void setRvColor(String rvColor) {
        this.rvColor = rvColor;
    }

    public String getCancelColor() {
        return cancelColor;
    }

    public void setCancelColor(String cancelColor) {
        this.cancelColor = cancelColor;
    }

    public String getSaleTypeColor1() {
        return saleTypeColor1;
    }

    public void setSaleTypeColor1(String saleTypeColor1) {
        this.saleTypeColor1 = saleTypeColor1;
    }

    public String getSaleTypeColor2() {
        return saleTypeColor2;
    }

    public void setSaleTypeColor2(String saleTypeColor2) {
        this.saleTypeColor2 = saleTypeColor2;
    }

    public String getSaleTypeColor3() {
        return saleTypeColor3;
    }

    public void setSaleTypeColor3(String saleTypeColor3) {
        this.saleTypeColor3 = saleTypeColor3;
    }

    public String getSaleTypeColor4() {
        return saleTypeColor4;
    }

    public void setSaleTypeColor4(String saleTypeColor4) {
        this.saleTypeColor4 = saleTypeColor4;
    }

    public String getSaleTypeColor5() {
        return saleTypeColor5;
    }

    public void setSaleTypeColor5(String saleTypeColor5) {
        this.saleTypeColor5 = saleTypeColor5;
    }

    public Date getModDt() {
        return modDt;
    }

    public void setModDt(Date modDt) {
        this.modDt = modDt;
    }
}
