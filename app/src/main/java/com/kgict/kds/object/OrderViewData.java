package com.kgict.kds.object;

import com.kgict.kds.Constants;

public class OrderViewData {
    int OrderDataIndex;     // order data list의 인덱스 (0,1,2,..)
    int ManyMenuMaxCount;   //  다량시 최대 오더화면 개수 (1,2,3,4...)
    int ManyMenuIndex;      // 다량시 이 오더의 다량내 인덱스 (0,1,2,...)
    int displayType = Constants.DISPLAY_TYPE_BLANK;

    public OrderViewData(int orderDataIndex, int manyMenuMaxCount, int manyMenuIndex) {
        OrderDataIndex = orderDataIndex;
        ManyMenuMaxCount = manyMenuMaxCount;
        ManyMenuIndex = manyMenuIndex;
        if(ManyMenuMaxCount == 1)
            displayType = Constants.DISPLAY_TYPE_ORDER_NORMAL;
        else if(ManyMenuMaxCount > 1 && ManyMenuIndex == 0)
            displayType = Constants.DISPLAY_TYPE_ORDER_START;
        else if(ManyMenuMaxCount > 1 && ManyMenuIndex == ManyMenuMaxCount -1)
            displayType = Constants.DISPLAY_TYPE_ORDER_END;
        else if(ManyMenuMaxCount > 1 && ManyMenuIndex < ManyMenuMaxCount -1)
            displayType = Constants.DISPLAY_TYPE_ORDER_CENTER;
    }

    public int getOrderDataIndex() {
        return OrderDataIndex;
    }

    public int getManyMenuMaxCount() {
        return ManyMenuMaxCount;
    }

    public int getManyMenuIndex() {
        return ManyMenuIndex;
    }

    public int getDisplayType() {
        return displayType;
    }

    public boolean isSame(OrderViewData orderViewData) {
        if(this.OrderDataIndex != orderViewData.getOrderDataIndex())
            return false;
        if(this.ManyMenuMaxCount != orderViewData.getManyMenuMaxCount())
            return false;
        if(this.ManyMenuIndex != orderViewData.getManyMenuIndex())
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "OrderViewData{" +
                "OrderDataIndex=" + OrderDataIndex +
                ", ManyMenuMaxCount=" + ManyMenuMaxCount +
                ", ManyMenuIndex=" + ManyMenuIndex +
                ", displayType=" + displayType +
                '}';
    }
}
