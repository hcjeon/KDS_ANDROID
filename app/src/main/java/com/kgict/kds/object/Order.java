package com.kgict.kds.object;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order {

    int OrderNum;

    @JsonProperty("orderId")
    String orderId;     // 주문 id

    @JsonProperty("saleYmd")
    String saleYmd;     // 영업일

    @JsonProperty("receiptNum")
    String receiptNum;       // 영수증번호

    @JsonProperty("procSt")
    int orderState;      // '주문:0, 결제:1, 조리완료:2, 포장완료:3, 제공완료:4', -- 주문상태

    @JsonProperty("regDt")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyyMMddHHmmss", timezone="GMT+9")
    Date regDate;     // 주문 들어온 시간

    @JsonProperty("modDt")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyyMMddHHmmss", timezone="GMT+9")
    Date modDate;     // 주문 변경 시간

    @JsonProperty("endDt")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyyMMddHHmmss", timezone="GMT+9")
    Date endDt;     // 주문 변경 시간

    @JsonProperty("regDeviceGb")
    int regDevice;     // 등록기기(POS:0, KIOSK:1 배달:2, DriveThru:3, 모바일:4)

    @JsonProperty("regDeviceNum")
    String regDeviceNum;      // 주문 받은 기기 넘버

    @JsonProperty("callNum")
    String callNum;      // 영수번호

    @JsonProperty("saleGb")
    String saleType;       // E:매장, T:포장, H:배달, D:드라이브쓰루, C:케이터링

    @JsonProperty(value = "kitchenMemo", defaultValue = "")
    String kitchenMemo;      // 주방메모

    @JsonProperty(value = "cancelFl", defaultValue = "0")
    int isCancel = 0;    // 주문 취소 여부

    @JsonProperty(value = "rvFl", defaultValue = "0")
    int isRevert = 0;    // 주문 취소 여부

    @JsonProperty("orderItemList")
    List<OrderMenu> list = new ArrayList<>();    // 주문 리스트

    public Order() {
    }

    public Order(String orderId, String saleYmd, int orderNum, Date regDate, Date modDate, int orderState, int regDevice, String regDeviceNum, String callNum, String saleType, int isCancel, List<OrderMenu> list) {
        this.orderId = orderId;
        this.saleYmd = saleYmd;
        this.OrderNum = orderNum;
        this.regDate = regDate;
        this.modDate = modDate;
        this.orderState = orderState;
        this.regDevice = regDevice;
        this.regDeviceNum = regDeviceNum;
        this.callNum = callNum;
        this.saleType = saleType;
        this.isCancel = isCancel;
        this.list = list;
    }

    public Order(Order order) {
        this.orderId = order.getOrderId();
        this.saleYmd = order.getSaleYmd();
        this.OrderNum = order.getOrderNum();
        this.regDate = order.getRegDate();
        this.modDate = order.getModDate();
        this.orderState = order.getOrderState();
        this.regDevice = order.regDevice;
        this.regDeviceNum = order.regDeviceNum;
        this.callNum = order.getCallNum();
        this.saleType = order.getSaleType();
        this.isCancel = order.isCancel();
        if(order.getList() != null)
            this.list.addAll(order.getList());
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    public String getSaleYmd() {
        return saleYmd;
    }

    public void setSaleYmd(String saleYmd) {
        this.saleYmd = saleYmd;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public int getOrderNum() {
        return OrderNum;
    }

    public void setOrderNum(int orderNum) {
        OrderNum = orderNum;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    public int getOrderState() {
        return orderState;
    }

    public void setOrderState(int orderState) {
        this.orderState = orderState;
    }

    public int getRegDevice() {
        return regDevice;
    }

    public void setRegDevice(int regDevice) {
        this.regDevice = regDevice;
    }

    public String getRegDeviceNum() {
        return regDeviceNum;
    }

    public void setRegDeviceNum(String regDeviceNum) {
        this.regDeviceNum = regDeviceNum;
    }

    public String getCallNum() {
        return callNum;
    }

    public void setCallNum(String callNum) {
        this.callNum = callNum;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public Date getEndDt() {
        return endDt;
    }

    public void setEndDt(Date endDt) {
        this.endDt = endDt;
    }

    public int isCancel() {
        return isCancel;
    }

    public void setCancel(int cancel) {
        isCancel = cancel;
    }

    public List<OrderMenu> getList() {
        return list;
    }

    public void setList(List<OrderMenu> list) {
        this.list = list;
    }

    public String getKitchenMemo() {
        return kitchenMemo;
    }

    public void setKitchenMemo(String kitchenMemo) {
        this.kitchenMemo = kitchenMemo;
    }

    public Order copyInstance() {
        Order tmpOrder = new Order(this);
        return tmpOrder;
    }

    public boolean isSame(Order order) {
        if(order == null)
            return false;

        if(!this.orderId.equals(order.getOrderId()))
            return false;
        if(this.modDate.getTime() != order.modDate.getTime())
            return false;
        if(!this.saleYmd.equals(order.getSaleYmd()))
            return false;
        if(this.isCancel != order.isCancel)
            return false;
        if(!this.callNum.equals(order.callNum))
            return false;
        if(this.orderState != order.orderState)
            return false;
        if(this.list.size() != order.getList().size())
            return false;
        for(int i=0; i<list.size(); i++) {
            if(!list.get(i).isSame(order.getList().get(i)))
                return false;
        }
        return true;
    }

    public int isRevert() {
        return isRevert;
    }

    public void setIsRevert(int isRevert) {
        this.isRevert = isRevert;
    }

    @Override
    public String toString() {
        return "Order{" +
                "OrderNum=" + OrderNum +
                ", orderId='" + orderId + '\'' +
                ", saleYmd='" + saleYmd + '\'' +
                ", orderState=" + orderState +
                ", regDate=" + regDate +
                ", modDate=" + modDate +
                ", endDt=" + endDt +
                ", regDevice=" + regDevice +
                ", regDeviceNum='" + regDeviceNum + '\'' +
                ", callNum='" + callNum + '\'' +
                ", saleType='" + saleType + '\'' +
                ", kitchenMemo='" + kitchenMemo + '\'' +
                ", isCancel=" + isCancel +
                ", list=" + list +
                '}';
    }
}
