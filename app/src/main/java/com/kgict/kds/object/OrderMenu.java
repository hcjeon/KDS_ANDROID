package com.kgict.kds.object;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderMenu extends Item{

  @JsonProperty("itemNm1")
  String name;        // 메뉴 이름

  @JsonProperty("itemOrd")
  int itemOrd;   // 메뉴 표시순서

  @JsonProperty("qty")
  int qty;        // 수량

  @JsonProperty("itemGb")
  int menuType;   // '싱글:1, 세트:2, 구성:3, 컨디먼트:4, 주방메모:5'

  @JsonProperty("parentItemOrd")
  int parentItemOrd;  // 메뉴 순서

  @JsonProperty("saleGb")
  String saleType;       // E:매장, T:포장, H:배달, D:드라이브쓰루, C:케이터링

  @JsonProperty("itemGrpCd")
  String itemGrpCd;       // 카테고리 코드

  @JsonProperty(value = "itemCd",defaultValue = "99999")
  String itemCode;

  @JsonProperty(value = "itemNm",defaultValue = "")
  String itemName;

  @JsonProperty("setItemList")
  List<SubItem> setItemList;

  @JsonProperty(value = "price", defaultValue = "0")
  float price;

  public OrderMenu() {
  }

  public OrderMenu(String name, int itemOrd, int qty, int menuType) {
    this.name = name;
    this.itemOrd = itemOrd;
    this.qty = qty;
    this.menuType = menuType;
  }

  public OrderMenu(String name, int itemOrd, int qty, String itemGrpCd, int menuType) {
    this.name = name;
    this.itemOrd = itemOrd;
    this.qty = qty;
    this.itemGrpCd = itemGrpCd;
    this.menuType = menuType;
  }

  public OrderMenu(String name, int itemOrd, int qty, int menuType, int parentItemOrd) {
    this.name = name;
    this.itemOrd = itemOrd;
    this.qty = qty;
    this.menuType = menuType;
    this.parentItemOrd = parentItemOrd;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getName() {
    if(itemName != null && itemName.length() > 0)
      return itemName;
    else
      return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getItemOrd() {
    return itemOrd;
  }

  public void setItemOrd(int itemOrd) {
    this.itemOrd = itemOrd;
  }

  public String getItemGrpCd() {
    return itemGrpCd;
  }

  public void setItemGrpCd(String itemGrpCd) {
    this.itemGrpCd = itemGrpCd;
  }

  public String getSaleType() {
    return saleType;
  }

  public void setSaleType(String saleType) {
    this.saleType = saleType;
  }

  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }

  public int getMenuType() {
    return menuType;
  }

  public void setMenuType(int menuType) {
    this.menuType = menuType;
  }

  public int getParentItemOrd() {
    return parentItemOrd;
  }

  public void setParentItemOrd(int parentItemOrd) {
    this.parentItemOrd = parentItemOrd;
  }

  public List<SubItem> getSetItemList() {
    return setItemList;
  }

  public void setSetItemList(List<SubItem> setItemList) {
    this.setItemList = setItemList;
  }

  public boolean isSame(OrderMenu orderMenu) {
    if (!itemCode.equals(orderMenu.getItemCode()))
      return false;
    if (itemOrd != orderMenu.getItemOrd())
      return false;
    if (qty != orderMenu.getQty())
      return false;
    if (menuType != orderMenu.getMenuType())
      return false;
    if(parentItemOrd != orderMenu.getParentItemOrd())
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "OrderMenu{" +
            "name='" + name + '\'' +
            ", itemOrd=" + itemOrd +
            ", qty=" + qty +
            ", menuType=" + menuType +
            ", parentItemOrd=" + parentItemOrd +
            ", saleType='" + saleType + '\'' +
            ", itemGrpCd='" + itemGrpCd + '\'' +
            ", itemCode='" + itemCode + '\'' +
            ", itemName='" + itemName + '\'' +
            ", setItemList=" + setItemList +
            ", price=" + price +
            '}';
  }
}
