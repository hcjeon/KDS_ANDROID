package com.kgict.kds.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubItem extends Item{

    @JsonProperty("subItemNm1")
    String name;        // 메뉴 이름

    @JsonProperty("subItemQty")
    int qty;

    @JsonProperty("subItemCd")
    String ItemCd;        // 메뉴 코드

    @JsonProperty("subItemGrpCd")
    String grpCd;        // 카테고리 코드

    @JsonProperty("subItemPrice")
    int price;        // 메뉴 이름

    public SubItem() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getItemCd() {
        return ItemCd;
    }

    public void setItemCd(String itemCd) {
        ItemCd = itemCd;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getGrpCd() {
        return grpCd;
    }

    public void setGrpCd(String grpCd) {
        this.grpCd = grpCd;
    }

    @Override
    public String toString() {
        return "SubItem{" +
                "name='" + name + '\'' +
                ", qty=" + qty +
                ", ItemCd='" + ItemCd + '\'' +
                ", grpCd='" + grpCd + '\'' +
                ", price=" + price +
                '}';
    }
}
