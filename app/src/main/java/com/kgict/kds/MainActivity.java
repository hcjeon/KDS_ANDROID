package com.kgict.kds;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.kgict.kds.event.RxKeyEvent;
import com.kgict.kds.event.RxNetworkStateEvent;
import com.kgict.kds.event.RxRefreshTimeEvent;
import com.kgict.kds.fragment.MainFragment;
import com.kgict.kds.object.Order;
import com.kgict.kds.object.OrderMenu;
import com.kgict.kds.object.OrderViewData;
import com.kgict.kds.utils.Dlog;
import com.kgict.kds.utils.Utils;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.reactivestreams.Subscription;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.kgict.kds.Configure.isDummy;

public class MainActivity extends RxAppCompatActivity {

    public App mApp;
    public MainFragment mainFragment;

    Fragment currentFragment;

    public List<Order> orderDataList;
    public List<Order> tmpOrderDataList = new ArrayList<>();

    List<OrderViewData> orderViewData = new ArrayList<>();

    int currentPage = 0;
    int pageAmount = 0;

    public int updateCount = 0;
    public boolean isUpdating = false;      // 현재 화면 업데이트 중일 때
    public boolean isSound = false;

    public boolean isStop = false;

    int timeRefreshMillSum = 0;
    long refreshMillTime60 = 0;
    long refreshMillTime600 = 0;

    long lastInputKeyTime =  -1;

    Calendar lastRefreshCal = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_main);

            ButterKnife.bind(this);

            mApp = (App) getApplication();
            Utils.setActivity(this);

            Dlog.i("maxCount >>> " + Configure.orderColumnCount +">>> " + Configure.orderRowCount );
            if(Configure.orderColumnCount * Configure.orderRowCount > 14) {
                Configure.menuMaximumCount = 6;
            } else {
                Configure.menuMaximumCount = 9;
            }
            Configure.kdsType = mApp.getPrefs().getInt("kdsType", Configure.kdsType);

//        applyKdsConfigure();
            createLayout();

            if (isDummy)
                createDummy();

            Dlog.i("----------------play----------------------111");
            if (isDummy)
                fetchOrder(true, false);
            else {
                Flowable.just(true)
                        .compose(bindToLifecycle())
                        .delay(3, TimeUnit.SECONDS)
                        .subscribe(aBoolean -> {
                            startFetch();
                        });
            }
        }
        catch (Exception ex){

        }
    }

    /**
     * 일정 주기마다 오더 받아오기
     */

    public void startFetch() {
        try{
            Subscription subscription;
            // 꾸준히 데이터 받아오는 쓰레드
//        disposable = Observable.just(true)
            Flowable.interval(Configure.interval, TimeUnit.MILLISECONDS, Schedulers.io())
                    .compose(bindToLifecycle())
//                .takeWhile(aLong -> !isStop)
                    .subscribe(
                            onNext -> {
                                timeRefreshMillSum += Configure.interval;
                                refreshMillTime60 += Configure.interval;
                                refreshMillTime600 += Configure.interval;
                                long diff = Calendar.getInstance().getTimeInMillis() - lastRefreshCal.getTimeInMillis();
                                if(diff > 1000*10) {
                                    Dlog.i("Network Error >>> " + isUpdating);
                                    if(isUpdating)
                                        RxNetworkStateEvent.getInstance().sendNetworkState(Constants.NETWORK_STATE_LOGIC_ERROR);
                                    else
                                        RxNetworkStateEvent.getInstance().sendNetworkState(Constants.NETWORK_STATE_FAIL);
                                }
                                if (!isUpdating)
                                    downloadOrderList(true);
                                if (timeRefreshMillSum > 30 * 1000) {
                                    timeRefreshMillSum = 0;
                                    if (orderDataList != null && orderDataList.size() > 0)
                                        RxRefreshTimeEvent.getInstance().sendBus(0);
                                }
                                if (refreshMillTime60 > 1000 * 60) {
                                    Dlog.i("60second >>>>   ");
                                    refreshMillTime60 = 0;
                                    checkReboot();
                                }
                                if (refreshMillTime600 > 1000 * 60 * 10) {
//                            if (refreshMillTime600 > 1000 * 30) {
                                    Dlog.i("600second >>>>   ");
                                    refreshMillTime600 = 0;
                                    checkSaleYmd();
                                }
                            },
                            onError -> {
                                mApp.showToast(onError.getMessage());
                                Dlog.i("error_1111");
                                onError.printStackTrace();
                            }
                    );
/*
        // 시간초 쓰레드
        Flowable.interval(1, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .compose(bindToLifecycle())
//                .takeWhile(aLong -> !isStop)
                .subscribe(
                        onNext -> {
                            if (orderDataList != null && orderDataList.size() > 0)
                                RxRefreshTimeEvent.getInstance().sendBus(0);
                        },
                        onError -> {
                            mApp.showToast(onError.getMessage());
                            Dlog.i("error_2222");
                            onError.printStackTrace();
                        }
                );
        */
        }
        catch (Exception ex){

        }
    }

    @Override
    protected void onPause() {
        try{
            isStop = true;
            super.onPause();
        }
        catch (Exception ex){

        }
    }

    @Override
    protected void onResume() {
        try{
            isStop = false;
            super.onResume();
        }
        catch (Exception ex){

        }
    }

    /**
     * 새 오더리스트 받아오기
     */
    public void downloadOrderList(boolean isSoundOn) {
        try{
            Dlog.i("getAllOrderList >>> isUpdating:" + isUpdating);
            Single<List<Order>> ff;
            if (Configure.kdsType == Constants.KDS_TYPE_KITCHEN)
                ff = mApp.getNetworkService().getAllOrderListKitchen(0, 40);
            else if(Configure.kdsType == Constants.KDS_TYPE_PACKING){
                Dlog.i("패킹주문건 조회");
                ff = mApp.getNetworkService().getAllOrderListPacking(0, 40, Configure.deliveryFl);
            }
            else
                ff = mApp.getNetworkService().getAllOrderList(0, 40);

            ff.compose(bindToLifecycle())
                    .subscribeOn(Schedulers.io())
                    .map(orders -> {
                        Dlog.i("getAllOrderList >>> delivery:" + orders.toString());
                        orderDataList = orders;
                        boolean needUpdate = fetchOrder(false, isSoundOn);
                        lastRefreshCal = Calendar.getInstance();
                        RxNetworkStateEvent.getInstance().sendNetworkState(Constants.NETWORK_STATE_SUCCESS);
                        if (!needUpdate) {
                            throw new Exception("NotUpdate");
                        }
                        return Single.just(orders);
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(orders -> {  // 각 페이지별 주문 화면 계산 및 그리기
                        if (Configure.kdsType == Constants.KDS_TYPE_COUNTER || Configure.kdsType == Constants.KDS_TYPE_KITCHEN) {
                            Dlog.i("counter fetchOrder");
                            mainFragment.refreshPageView();
                            if(Configure.summaryType.equals("1"))
                                mainFragment.orderMainFragment.fetchSummary();
                            mainFragment.orderMainFragment.getPageAdapter().fetchOrder(isSoundOn);
                        } else {
                            Dlog.i("packing fetchOrder");
                            // 팩킹 KDS
                            mainFragment.packingMainFragment.notifyList();
                        }
                    }, onError -> {
                        if (onError.getMessage() instanceof String) {
                            if (onError.getMessage().equals("NotUpdate"))
                                return;
                        }
                        mApp.showToast(onError.getMessage());
                        Dlog.i("error_3333");
                        onError.printStackTrace();
                    });
        }
        catch (Exception ex){

        }
    }

    /**
     * 새로운 주문이 있는지 검사 하고, 있을 경우 페이지 계산
     *
     * @param needUpdate
     * @param isSoundOn
     * @return
     */
    public boolean fetchOrder(boolean needUpdate, boolean isSoundOn) {
        try{
            // 데이터값 확인해서 새로고침 할 것이냐
            if (tmpOrderDataList.size() < orderDataList.size() && isSoundOn)
                isSound = true;
            if (tmpOrderDataList.size() != orderDataList.size())
                needUpdate = true;
            else if (tmpOrderDataList.size() > 0 && orderDataList.size() > 0) {
                for (int i = 0; i < tmpOrderDataList.size(); i++) {
                    if (tmpOrderDataList.get(i).getModDate().compareTo(orderDataList.get(i).getModDate()) != 0) {
                        needUpdate = true;
                        break;
                    }
                }
            }

            if ((!needUpdate && tmpOrderDataList.size() != 0))
                return needUpdate;

            Dlog.i("===================================fetchOrder=========================================");

            // 페이지 계산
            setIsUpdating(true);
            orderViewData.clear();

            // 페이지 및 주문 표시 스타일 계산
            int orderWeightSum = 0;
            int viewIndex = 0;
            Dlog.i(">>>>>>maxCount >> " + Configure.menuMaximumCount);
            for (int i = 0; i < orderDataList.size(); i++) {
                Order order = orderDataList.get(i);
                int orderWeight = order.getList().size() / (Configure.menuMaximumCount + 1) + 1;
                for (int k = 0; k < orderWeight; k++) {
                    orderViewData.add(new OrderViewData(i, orderWeight, k));
                }
                orderWeightSum += orderWeight;
            }
            if (orderWeightSum > 0)
                pageAmount = (orderWeightSum - 1) / (Configure.orderColumnCount * Configure.orderRowCount - 1) + 1;

            Dlog.i("PageAmount : " + pageAmount + "   CurrentPage : " + currentPage + "   orderWeight : " + orderWeightSum);
            if (tmpOrderDataList != null && orderDataList != null)
                Dlog.i("tmpOrderDataList >> " + tmpOrderDataList.size() + "     newOrderList >> " + orderDataList.size());

            if (tmpOrderDataList != null) {
                tmpOrderDataList.clear();
                tmpOrderDataList.addAll(orderDataList);
            }

            if (isDummy) {
                runOnUiThread(() -> {
                    mainFragment.refreshPageView();
                    mainFragment.orderMainFragment.getPageAdapter().fetchOrder(isSoundOn);
                });
            }

            return true;
        }
        catch (Exception ex){
            return  false;
        }
    }

    public void createLayout() {
        try{
            // 프레그먼트 생성
            mainFragment = new MainFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment, mainFragment, "mainFragment");
            fragmentTransaction.commit();

            currentFragment = mainFragment;
        }
        catch (Exception ex){

        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        try{
            Dlog.i("keycode >>> " + keyCode);

            if (Constants.KEY_BACK == keyCode)
                finishApp();

            if (event.getAction() == KeyEvent.ACTION_UP) {
                if (currentFragment == mainFragment) {
                    Dlog.i("MainActivity >> " + keyCode);
                    RxKeyEvent.getInstance().sendKeyCode(keyCode);
                    isUpdating = true;
                    downloadOrderList(true);
                }
            }

            return false;
        }
        catch (Exception ex){
            return false;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        try{
            if (event.getAction() == KeyEvent.ACTION_UP) {
                return super.dispatchKeyEvent(event);
            } else
                return false;
        }
        catch (Exception ex){
            return false;
        }
    }

    public void createDummy() {
        try{
            // dummy data
            orderDataList = new ArrayList<>();

            List<OrderMenu> list1 = new ArrayList<>();
            list1.add(new OrderMenu("징거세트", 0, 1, 2));
            list1.add(new OrderMenu("타워세트", 0, 2, 2));
            list1.add(new OrderMenu("롱치즈스틱", 0, 10, 1));
            Order orderData1 = new Order("1", "20190313", 1, new Date(), new Date(), 1, 0, "01", "0001", "E", 0, list1);
            orderDataList.add(orderData1);

            List<OrderMenu> list2 = new ArrayList<>();
            list2.add(new OrderMenu("징거세트", 0, 1, 2));
            list2.add(new OrderMenu("블랙라벨싱글세트", 0, 2, 2));
            list2.add(new OrderMenu("치킨너겟4개", 0, 10, 1));
            Order orderData2 = new Order("1", "20190313", 2, new Date(), new Date(), 0, 1, "01", "0002", "H", 0, list2);
            orderDataList.add(orderData2);

            List<OrderMenu> list3 = new ArrayList<>();
            list3.add(new OrderMenu("블랙라벨박스", 0, 1, 2));
            list3.add(new OrderMenu("트위스터팩", 0, 2, 2));
            Calendar cal3 = Calendar.getInstance();
            cal3.setTime(new Date());
            cal3.add(Calendar.MINUTE, -1);
            Order orderData3 = new Order("1", "20190313", 3, cal3.getTime(), cal3.getTime(), 1, 0, "01", "0003", "E", 0, list3);
            orderDataList.add(orderData3);
        }
        catch (Exception ex){

        }
    }

    public int getPageAmount() {
        return pageAmount;
    }

    public void setPageAmount(int pageAmount) {
        this.pageAmount = pageAmount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public List<OrderViewData> getOrderViewData() {
        return orderViewData;
    }

    public List<Order> getOrderDataList() {
        return orderDataList;
    }

    public void setOrderDataList(List<Order> orderDataList) {
        this.orderDataList = orderDataList;
    }

    public void setIsUpdating(boolean isUpdating) {
        Dlog.i("setIsUpdate >>>>>>>>>>>>>>>>> " + isUpdating);
        this.isUpdating = isUpdating;
    }

    public boolean getIsUpdating() {
        return isUpdating;
    }

    public void plusUpdatePageCount() {
        this.updateCount++;
    }

    public void resetUpdateCount() {
        this.updateCount = 0;
    }

    public void sendCallNum(String callNum, String receiptNum) {
        try{
            // 콜넘버 전송
            Flowable.just(true)
                    .compose(bindToLifecycle())
                    .observeOn(Schedulers.io())
                    .subscribe(aBoolean -> {
                        try {
                            Dlog.i("call>> " + Configure.DID_URL + "   " + Configure.callNumPort);
                            Dlog.i("call >>> " + callNum);
                            Socket socket = new Socket(Configure.DID_URL, Configure.callNumPort);
                            BufferedWriter networkWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                            PrintWriter out = new PrintWriter(networkWriter, true);
                            String return_msg = callNum;
                            out.println(return_msg);
                            socket.close();
                        } catch (IOException e) {
                            System.out.println(e);
                            e.printStackTrace();
                        }
                    });

            if (receiptNum == null || receiptNum.length() == 0)
                return;
            else
                updateCallState(receiptNum, "8");
        }
        catch (Exception ex){

        }
/*
        try {
            String STX = "<STX>";
            String ETX = "<ETX>";
            JSONObject param = new JSONObject();
            JSONArray headerJar = new JSONArray();
            JSONObject headerJob = new JSONObject();
            headerJob.put("COMPANY", "HEETECH");
            headerJob.put("TYPE", "1.0.0");
            headerJob.put("SERVICE_CD", "HTECH_KDS_SC010");
            headerJob.put("CMD_TP", "3");
            headerJar.put(headerJob);
            param.put("header", headerJar);

            JSONArray bodyJar = new JSONArray();
            JSONObject bodyJob = new JSONObject();
            bodyJob.put("STATUS_CD", "8");
            bodyJob.put("RECEIPT_NO", "72912");
            bodyJar.put(bodyJob);
            param.put("body", bodyJar);

            Dlog.i("socket 222");
            StringBuffer buff = new StringBuffer();
            buff.append(STX + param.toString() + ETX);

            new Thread() {
                public void run() {
                    NetClient nc = new NetClient(Configure.callNumIp, Configure.callNumPort2, ""); //mac address maybe not for you
                    nc.sendDataWithString(buff.toString());
                    String r = nc.receiveDataFromServer();
                    Dlog.i("socket result >> " + r);
                }
            }.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
*/
    }

    public void updateCallState(String receiptNum, String status) {
        try{
            Dlog.i("updateCallState >>>  " + Configure.DID_URL+ "     " + Configure.callNumPort2 + "   "  + receiptNum);

            Flowable.just(true)
                    .compose(bindToLifecycle())
                    .observeOn(Schedulers.io())
                    .subscribe(aBoolean -> {
                        try {
                            Dlog.i("11111");
                            Socket socket = new Socket(Configure.DID_URL, Configure.callNumPort2);
                            Dlog.i("22222");
                            BufferedWriter networkWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
//                    DataInputStream inputStream = new DataInputStream(socket.getInputStream());
                            PrintWriter out = new PrintWriter(networkWriter, true);
                            String STX = new String("<STX>".getBytes("utf-8"), "utf-8");
                            String ETX = new String("<ETX>".getBytes("utf-8"), "utf-8");
                            JSONObject param = new JSONObject();
                            JSONArray headerJar = new JSONArray();
                            JSONObject headerJob = new JSONObject();
                            headerJob.put("COMPANY", "HEETECH");
                            headerJob.put("TYPE", "1.0.0");
                            headerJob.put("SERVICE_CD", "HTECH_KDS_SC010");
                            headerJob.put("CMD_TP", "3");
                            headerJar.put(headerJob);
                            param.put("header", headerJar);

                            JSONArray bodyJar = new JSONArray();
                            JSONObject bodyJob = new JSONObject();
                            bodyJob.put("STATUS_CD", status);
                            bodyJob.put("RECEIPT_NO", receiptNum);
                            bodyJar.put(bodyJob);

                            Dlog.i("9999");

                            param.put("body", bodyJar);

                            Dlog.i("updateCall >> " + param.toString());
                            String paramString = new String(param.toString().getBytes("utf-8"), "utf-8");

                            StringBuffer buff = new StringBuffer();
                            buff.append(STX + paramString + ETX);

                            out.println(buff.toString());
                            Dlog.i("socket >> " + buff.toString());

//                    String result = inputStream.readLine();
                            socket.close();
                        } catch (IOException e) {
                            Dlog.i("socket >> ioException");
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
        }
        catch (Exception ex){

        }
    }

    public void finishApp() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public long getLastInputKeyTime() {
        return lastInputKeyTime;
    }

    public void setLastInputKeyTime() {
        lastInputKeyTime = System.currentTimeMillis();
    }

    /**
     * 재실행 체크
     */
    public void checkReboot() {
        try{
            mApp.getNetworkService().getKdsConfigure(Integer.parseInt(mApp.prefs.getString(Constants.PREF_KDS_ID, "")))
                    .compose(bindToLifecycle())
                    .observeOn(Schedulers.io())
                    .subscribe(kdsConfigure -> {
                        Dlog.i("reboot >>> " + kdsConfigure.getRebootFl());
                        String rebootString = kdsConfigure.getRebootFl();
                        if(rebootString.equals("1")) {
                            Intent mStartActivity = new Intent(this, SplashActivity.class);
                            int mPendingIntentId = 123456;
                            PendingIntent mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, mStartActivity,
                                    PendingIntent.FLAG_CANCEL_CURRENT);
                            AlarmManager mgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                            System.exit(0);
                        }
                    }, onError -> {
                        Dlog.i("onError >> " + onError.getMessage());
                    });
        }
        catch (Exception ex){

        }
    }

    /**
     * 영업일자 가져오기
     */
    public void checkSaleYmd() {
        try{
            mApp.getNetworkService().getSaleYmd("")
                    .compose(bindToLifecycle())
                    .subscribeOn(Schedulers.io())
                    .map(saleYmd -> {
                        Dlog.i("getSaleYmd >>> " + saleYmd);
                        if(!Configure.saleYmd.equals(saleYmd)) {
                            Configure.saleYmd = saleYmd;
                        }
                        return saleYmd;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(saleYmd -> {
                        mainFragment.setSaleYmdText();
                    }, onError -> {
                        Dlog.i("onError >> " + onError.getMessage());
                        mApp.showToast(onError.getMessage());
                    });
        }
        catch (Exception ex){

        }
    }
}

