package com.kgict.kds.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.opengl.Visibility;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.R;
import com.kgict.kds.event.RxRefreshTimeEvent;
import com.kgict.kds.object.Order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static android.view.View.VISIBLE;

public class PackingOrderAdapter extends RecyclerView.Adapter<PackingOrderAdapter.ViewHolder> {

    Context context;
    List<Order> orderList = new ArrayList<>();
    PackingOrderMenuListAdapter adapter;
    OnClickListener onClickListener;

    public int colorWhite;

    public interface OnClickListener {
        void onClickListener(Order order, int position);
        void onClickCompleteNoCall(Order orer);
    }

    public PackingOrderAdapter(Context context, OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
        colorWhite = ContextCompat.getColor(context, R.color.fontWhite);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_packing_order, parent, false);

        if(Constants.PACK_MUCH.equals(Configure.PackingType)) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_packing_order_much, parent, false);
        }

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Order order = orderList.get(position);

        switch (order.getRegDevice()) {
            case Constants.DEVICES_TYPE_1:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor1);
                holder.regDeviceText.setText(Configure.saleTypeTitle1);
                break;
            case Constants.DEVICES_TYPE_2:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor2);
                holder.regDeviceText.setText(Configure.saleTypeTitle2);
                break;
            case Constants.DEVICES_TYPE_3:
                holder.layout.setBackgroundColor(Configure.deliveryBackgroundColor);
                holder.regDeviceText.setText(Configure.deliveryTitle);
                break;
            case Constants.DEVICES_TYPE_4:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor4);
                holder.regDeviceText.setText(Configure.saleTypeTitle4);
                break;
            case Constants.DEVICES_TYPE_5:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor5);
                holder.regDeviceText.setText(Configure.saleTypeTitle5);
                break;
            case Constants.DEVICES_TYPE_6:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor6);
                holder.regDeviceText.setText(Configure.saleTypeTitle6);
                break;
            case Constants.DEVICES_TYPE_7:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor7);
                holder.regDeviceText.setText(Configure.saleTypeTitle7);
                break;
            case Constants.DEVICES_TYPE_8:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor8);
                holder.regDeviceText.setText(Configure.saleTypeTitle8);
                break;
            case Constants.DEVICES_TYPE_9:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor9);
                holder.regDeviceText.setText(Configure.saleTypeTitle9);
                break;
            case Constants.DEVICES_TYPE_10:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor10);
                holder.regDeviceText.setText(Configure.saleTypeTitle10);
                break;
            default:
                holder.layout.setBackgroundColor(Configure.saleTypeBackgroundColor1);
                holder.regDeviceText.setText("");
                break;
        }
        if (order.isRevert() == 1) {
            holder.layout.setBackgroundColor(Configure.revertBackgroundColor);
        }
        // 취소 표시
        if (order.isCancel() == 1) {
            holder.layout.setBackgroundColor(Configure.cancelBackgroundColor);
        }


        holder.callNum.setText(order.getCallNum());

        if(Configure.takeType != null && Configure.takeType.equals("1")){
            holder.takeText.setText(order.getKitchenMemo());
            holder.takeText.setVisibility(VISIBLE);
        } else {
            holder.takeText.setBackgroundResource(R.drawable.rounded_corners_25dp);
            GradientDrawable takeDrawable = (GradientDrawable) holder.takeText.getBackground();
            holder.takeText.setTextColor(ContextCompat.getColor(context, R.color.white));
            switch (order.getSaleType()) {
                case "T":
                    takeDrawable.setColor(Configure.takeTextBackgroundColor);
                    holder.takeText.setTextColor(0xff143005);
                    holder.takeText.setText("포장");
                    holder.takeText.setVisibility(VISIBLE);
                    break;
                case "H":
                case "D":
                case "C":
                default:
                    holder.takeText.setVisibility(View.INVISIBLE);
                    break;
            }
        }


        int sum = 0;
        for (int i = 0; i < order.getList().size(); i++) {
            sum += order.getList().get(i).getQty();
        }
        holder.menuAmountText.setText(String.valueOf(sum));

        Date now = new Date();
        Date diffDate = new Date(now.getTime() - order.getRegDate().getTime());
        long sec = diffDate.getTime() / 1000 % 60;
        long min = diffDate.getTime() / (1000 * 60);
        holder.timeText.setText(String.format("%02d", min) + ":" + String.format("%02d", sec));

        if ((diffDate.getTime() / 1000) > Configure.delaySecond4) {
            if(Configure.delayType != null && Configure.delayType.equals("1")) {
                holder.layout.setBackgroundColor(Configure.delayColor4);
                holder.timeText.setTextColor(colorWhite);
            } else {
                holder.timeText.setTextColor(Configure.delayColor4);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond3) {
            if(Configure.delayType != null && Configure.delayType.equals("1")) {
                holder.layout.setBackgroundColor(Configure.delayColor3);
                holder.timeText.setTextColor(colorWhite);
            } else {
                holder.timeText.setTextColor(Configure.delayColor3);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond2) {
            if(Configure.delayType != null && Configure.delayType.equals("1")) {
                holder.layout.setBackgroundColor(Configure.delayColor2);
                holder.timeText.setTextColor(colorWhite);
            } else {
                holder.timeText.setTextColor(Configure.delayColor2);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond1) {
            if(Configure.delayType != null && Configure.delayType.equals("1")) {
                holder.layout.setBackgroundColor(Configure.delayColor1);
                holder.timeText.setTextColor(colorWhite);
            } else {
                holder.timeText.setTextColor(Configure.delayColor1);
            }
        } else {
            holder.timeText.setTextColor(colorWhite);
            holder.recyclerView.setBackgroundColor(ContextCompat.getColor(context, R.color.black));
        }

        holder.layout.setOnClickListener(v -> {
            onClickListener.onClickListener(order, position);
        });

        holder.completeWithNoCallButton.setOnClickListener(view -> {
            onClickListener.onClickCompleteNoCall(order);
        });

        RxRefreshTimeEvent.getInstance().getBus()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(str -> {
                    if (holder.timeText != null && order != null)
                        refreshTime(holder.layout, holder.timeText, order);
                });

        adapter = new PackingOrderMenuListAdapter(context);
        LinearLayoutManager lm = new LinearLayoutManager(context);
        lm.setOrientation(LinearLayoutManager.VERTICAL);

        if(Constants.PACK_MUCH.equals(Configure.PackingType)) {
            holder.recyclerView.addOnItemTouchListener(mScrollTouchListener);
        }

        holder.recyclerView.setLayoutManager(lm);
        holder.recyclerView.setAdapter(adapter);
        adapter.setDataList(order.getList());
        adapter.notifyDataSetChanged();
    }

    public void refreshTime(ConstraintLayout layout, TextView timeText, Order order) {

        Date now = new Date();
        Date diffDate = new Date(now.getTime() - order.getRegDate().getTime());
        long sec = diffDate.getTime() / 1000 % 60;
        long min = diffDate.getTime() / (1000 * 60);
        timeText.setText(String.format("%02d", min) + ":" + String.format("%02d", sec));
        if ((diffDate.getTime() / 1000) > Configure.delaySecond4) {
            if(Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor4);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor4);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond3) {
            if(Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor3);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor3);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond2) {
            if(Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor2);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor2);
            }
        } else if ((diffDate.getTime() / 1000) > Configure.delaySecond1) {
            if(Configure.delayType != null && Configure.delayType.equals("1")) {
                layout.setBackgroundColor(Configure.delayColor1);
                timeText.setTextColor(colorWhite);
            } else {
                timeText.setTextColor(Configure.delayColor1);
            }
        } else {
            switch (order.getRegDevice()) {
                case Constants.DEVICES_TYPE_1:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor1);
                    break;
                case Constants.DEVICES_TYPE_2:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor2);
                    break;
                case Constants.DEVICES_TYPE_3:
                    layout.setBackgroundColor(Configure.deliveryBackgroundColor);
                    break;
                case Constants.DEVICES_TYPE_4:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor4);
                    break;
                case Constants.DEVICES_TYPE_5:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor5);
                    break;
                case Constants.DEVICES_TYPE_6:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor6);
                    break;
                case Constants.DEVICES_TYPE_7:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor7);
                    break;
                case Constants.DEVICES_TYPE_8:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor8);
                    break;
                case Constants.DEVICES_TYPE_9:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor9);
                    break;
                case Constants.DEVICES_TYPE_10:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor10);
                    break;
                default:
                    layout.setBackgroundColor(Configure.saleTypeBackgroundColor1);
                    break;
            }
            if (order.isRevert() == 1) {
                layout.setBackgroundColor(Configure.revertBackgroundColor);
            }
            // 취소 표시
            if (order.isCancel() == 1) {
                layout.setBackgroundColor(Configure.cancelBackgroundColor);
            }
            timeText.setTextColor(colorWhite);
        }
    }

    @Override
    public int getItemCount() {
        if (orderList == null)
            return 0;
        return orderList.size();
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public void removeItem(Order order) {
        if(orderList.size() > 0) {
            orderList.remove(order);
//            notifyItemRangeChanged(position, getItemCount());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layout)
        ConstraintLayout layout;
        @BindView(R.id.callNum)
        public TextView callNum;
        @BindView(R.id.takeText)
        public TextView takeText;
        @BindView(R.id.menuAmountText)
        public TextView menuAmountText;
        @BindView(R.id.timeText)
        public TextView timeText;
        @BindView(R.id.regDeviceText)
        public TextView regDeviceText;
        @BindView(R.id.recyclerView)
        public RecyclerView recyclerView;
        @BindView(R.id.completeWithNoCallButton)
        public Button completeWithNoCallButton;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    RecyclerView.OnItemTouchListener mScrollTouchListener = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            int action = e.getAction();
            switch (action) {
                case MotionEvent.ACTION_MOVE:
                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                    break;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    };
}
