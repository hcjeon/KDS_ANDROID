package com.kgict.kds.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kgict.kds.R;
import com.kgict.kds.object.OrderMenu;

import org.apache.commons.collections4.MapUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CallNumAdapter extends RecyclerView.Adapter<CallNumAdapter.ViewHolder> {

    Context context;
    List<Map<String, String>> callHistory;
    OnClickListener onClickListener;

    public interface OnClickListener {
        void onClickListener(String callNum);
    }

    public CallNumAdapter(Context context, List<Map<String, String>> callHistory, OnClickListener onClickListener) {
        this.context = context;
        this.callHistory = callHistory;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_call_num, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Map data = callHistory.get(position);

        String callNum = MapUtils.getString(data, "callNum", "");
        if(callNum.length() > 0)
            holder.callNumText.setText(callNum);

        holder.layout.setOnClickListener(v -> {
            onClickListener.onClickListener(callNum);
        });
    }

    @Override
    public int getItemCount() {
        return callHistory.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.layout)
        LinearLayout layout;
        @BindView(R.id.callNumText)
        TextView callNumText;
        @BindView(R.id.divider)
        View divider;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
