package com.kgict.kds.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.R;
import com.kgict.kds.object.Order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.VISIBLE;

public class CompleteOrderAdapter extends RecyclerView.Adapter<CompleteOrderAdapter.ViewHolder> {

    Context context;
    List<Order> orderList = new ArrayList<>();
    PackingOrderMenuListAdapter adapter;
    OnClickListener onClickListener;

    public interface OnClickListener {
        void onClickListener(Order order);
    }

    public CompleteOrderAdapter(Context context, OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_packing_order_complete, parent, false);

        if(Constants.PACK_MUCH.equals(Configure.PackingType)) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_packing_order_much, parent, false);
        }

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Order order = orderList.get(position);

        switch (order.getRegDevice()) {
            case Constants.DEVICES_TYPE_1:
                holder.regDeviceText.setText(Configure.saleTypeTitle1);
                break;
            case Constants.DEVICES_TYPE_2:
                holder.regDeviceText.setText(Configure.saleTypeTitle2);
                break;
            case Constants.DEVICES_TYPE_3:
                holder.regDeviceText.setText(Configure.deliveryTitle);
                break;
            case Constants.DEVICES_TYPE_4:
                holder.regDeviceText.setText(Configure.saleTypeTitle4);
                break;
            case Constants.DEVICES_TYPE_5:
                holder.regDeviceText.setText(Configure.saleTypeTitle5);
                break;
            case Constants.DEVICES_TYPE_6:
                holder.regDeviceText.setText(Configure.saleTypeTitle6);
                break;
            case Constants.DEVICES_TYPE_7:
                holder.regDeviceText.setText(Configure.saleTypeTitle7);
                break;
            case Constants.DEVICES_TYPE_8:
                holder.regDeviceText.setText(Configure.saleTypeTitle8);
                break;
            case Constants.DEVICES_TYPE_9:
                holder.regDeviceText.setText(Configure.saleTypeTitle9);
                break;
            case Constants.DEVICES_TYPE_10:
                holder.regDeviceText.setText(Configure.saleTypeTitle10);
                break;
            default:
                holder.regDeviceText.setText("");
                break;
        }

        holder.callNum.setText(order.getCallNum());
        if(Configure.takeType != null && Configure.takeType.equals("1")){
            holder.takeText.setText(order.getKitchenMemo());
            holder.takeText.setVisibility(VISIBLE);
        } else {
            holder.takeText.setBackgroundResource(R.drawable.rounded_corners_25dp);
            GradientDrawable takeDrawable = (GradientDrawable) holder.takeText.getBackground();
            holder.takeText.setTextColor(ContextCompat.getColor(context, R.color.white));
            switch (order.getSaleType()) {
                case "T":
                    takeDrawable.setColor(Configure.takeTextBackgroundColor);
                    holder.takeText.setTextColor(0xff143005);
                    holder.takeText.setText("포장");
                    holder.takeText.setVisibility(VISIBLE);
                    break;
                case "H":
                case "D":
                case "C":
                default:
                    holder.takeText.setVisibility(View.INVISIBLE);
                    break;
            }
        }

        int sum = 0;
        for (int i = 0; i < order.getList().size(); i++) {
            sum += order.getList().get(i).getQty();
        }
        holder.menuAmountText.setText(String.valueOf(sum));

        try {
            SimpleDateFormat dt = new SimpleDateFormat("HH:mm:ss");
            Date endDate = new Date(order.getEndDt().getTime());
            holder.timeText.setText(dt.format(endDate));
            holder.timeText.setTextColor(ContextCompat.getColor(context, R.color.fontWhite));
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.layout.setOnClickListener(v -> {
            onClickListener.onClickListener(order);
        });

        adapter = new PackingOrderMenuListAdapter(context);
        LinearLayoutManager lm = new LinearLayoutManager(context);
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        holder.recyclerView.setLayoutManager(lm);

        if(Constants.PACK_MUCH.equals(Configure.PackingType)) {
            holder.recyclerView.addOnItemTouchListener(mScrollTouchListener);
        }

        holder.recyclerView.setAdapter(adapter);
        adapter.setDataList(order.getList());
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (orderList == null)
            return 0;
        return orderList.size();
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        @BindView(R.id.layout)
        ConstraintLayout layout;
        @BindView(R.id.callNum)
        public TextView callNum;
        @BindView(R.id.takeText)
        public TextView takeText;
        @BindView(R.id.menuAmountText)
        public TextView menuAmountText;
        @BindView(R.id.timeText)
        public TextView timeText;
        @BindView(R.id.regDeviceText)
        public TextView regDeviceText;
        @BindView(R.id.recyclerView)
        public RecyclerView recyclerView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    RecyclerView.OnItemTouchListener mScrollTouchListener = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            int action = e.getAction();
            switch (action) {
                case MotionEvent.ACTION_MOVE:
                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                    break;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    };
}
