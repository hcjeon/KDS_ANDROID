package com.kgict.kds.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.kgict.kds.MainActivity;
import com.kgict.kds.fragment.OrderFragment;
import com.kgict.kds.network.NetworkService;
import com.kgict.kds.utils.Dlog;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PageAdapter extends FragmentStatePagerAdapter {

    List<OrderFragment> orderFragments = new ArrayList<>();
    MainActivity activity;

    public PageAdapter(MainActivity mainActivity) {
        super(mainActivity.getSupportFragmentManager());
        activity = mainActivity;

        for(int i=0; i<4; i++) {
            OrderFragment fragment = new OrderFragment();
            fragment.setPageIndex(i);
            fragment.setParentAdapter(this);
            orderFragments.add(fragment);
        }
    }

/*
    @Override
    public Fragment getItem(int position) {
        OrderFragment orderFragment = new OrderFragment();
        orderFragment.setPageIndex(position);
        orderFragment.setParentAdapter(this);
        orderFragments.add(orderFragment);
        return orderFragment;
    }
*/

    @Override
    public int getCount() {
        return orderFragments.size();
    }

    public void fetchOrder(boolean isSound) {

        notifyDataSetChanged();
        if(orderFragments.size() == 0) {
            Dlog.i(">>>> orderFragments size 0000" );
            return;
        }

        Dlog.i("orderFramgnet size >> " + orderFragments.size());

        for (int i = 0; i < orderFragments.size(); i++) {
//            if (activity.getCurrentPage() == i) {
                orderFragments.get(i).fetchOrder(isSound);
//            }
        }
    }

    public List<OrderFragment> getOrderFragments() {
        return orderFragments;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    public void deletePage(int position) {
        orderFragments.remove(position);
    }


    @Override
    public Fragment getItem(int position) {
        return orderFragments.get(position);
    }
}