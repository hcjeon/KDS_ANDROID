package com.kgict.kds.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kgict.kds.Configure;
import com.kgict.kds.R;
import com.kgict.kds.object.ItemGrp;
import com.kgict.kds.object.OrderMenu;
import com.kgict.kds.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryAdapter extends RecyclerView.Adapter<SummaryAdapter.RootHolder> {

    Context context;
    List<OrderMenu> dataList;

    class MenuViewHolder extends RootHolder {

        public MenuViewHolder(View itemView) {
            super(itemView);
        }
    }

    class RootHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.layout)
        LinearLayout layout;
        @BindView(R.id.qty)
        TextView qtyText;
        @BindView(R.id.name)
        TextView nameText;

        public RootHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public SummaryAdapter(Context context, List<OrderMenu> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RootHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;

        if (Configure.itemOrderType.equals("1"))
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_summary_menu_2, parent, false);
        else
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_summary_menu, parent, false);
        // set the view's size, margins, paddings and layout parameters
        RootHolder vh = new RootHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RootHolder holder, int position) {
        final OrderMenu orderMenu = dataList.get(position);
        ItemGrp category = Utils.getItemGrp(orderMenu.getItemGrpCd());

        if (orderMenu.getQty() < 0)
            return;

        if (category != null && category.getColor() != -1) {
            holder.qtyText.setTextColor(category.getColor());
        } else {
            holder.qtyText.setTextColor(ContextCompat.getColor(context, R.color.blue));
        }

        if (category != null && category.getColor() != -1) {
            holder.qtyText.setTextColor(category.getColor());
            holder.nameText.setTextColor(category.getColor());
        } else {
            if(Configure.summaryMenuType.equals("2")) {
                holder.qtyText.setTextColor(ContextCompat.getColor(context, R.color.fontBlack));
                holder.nameText.setTextColor(ContextCompat.getColor(context, R.color.fontBlack));
            } else {
                holder.qtyText.setTextColor(ContextCompat.getColor(context, R.color.fontWhite));
                holder.nameText.setTextColor(ContextCompat.getColor(context, R.color.fontWhite));
            }
        }

        holder.qtyText.setText(String.valueOf(orderMenu.getQty()));
        holder.nameText.setText(orderMenu.getName());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
