package com.kgict.kds.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kgict.kds.Configure;
import com.kgict.kds.Constants;
import com.kgict.kds.R;
import com.kgict.kds.object.ItemGrp;
import com.kgict.kds.utils.Dlog;
import com.kgict.kds.utils.Utils;
import com.kgict.kds.object.OrderMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.RootHolder> {

    Context context;
    List<OrderMenu> dataList;

    int fontColor = 0;

    class MenuViewHolder extends RootHolder {

        public MenuViewHolder(View itemView) {
            super(itemView);
        }
    }

    class RootHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.layout)
        LinearLayout layout;
        @BindView(R.id.sub)
        TextView subText;
        @BindView(R.id.qty)
        TextView qtyText;
        @BindView(R.id.name)
        TextView nameText;
        @BindView(R.id.divider)
        View divider;

        public RootHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public MenuListAdapter(Context context) {
        this.context = context;
    }

    public MenuListAdapter(Context context, List<OrderMenu> dataList) {
        this.context = context;
        this.dataList = dataList;
        fontColor = Configure.menuFontColor;
    }

    @NonNull
    @Override
    public RootHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v;

        if (Configure.itemOrderType.equals("1"))
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_order_menu_2, parent, false);
        else
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_order_menu, parent, false);
        // set the view's size, margins, paddings and layout parameters
        RootHolder vh = new RootHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RootHolder holder, int position) {
        final OrderMenu orderMenu = dataList.get(position);
        ItemGrp category = Utils.getItemGrp(orderMenu.getItemGrpCd());

        holder.subText.setTextSize(Configure.menuFontSize);
        holder.qtyText.setTextSize(Configure.menuFontSize);
        holder.nameText.setTextSize(Configure.menuFontSize);

        if (orderMenu.getMenuType() >= 2) {
            holder.subText.setVisibility(View.VISIBLE);
        } else {
            holder.subText.setVisibility(View.GONE);
        }

        holder.subText.setTextColor(fontColor);

        // 메뉴 색상
        if (orderMenu.getQty() < 0) {
            holder.qtyText.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.nameText.setTextColor(ContextCompat.getColor(context, R.color.red));
        } else {
            if (category != null && category.getColor() != -1) {
                holder.qtyText.setTextColor(category.getColor());
                holder.nameText.setTextColor(category.getColor());
            } else {
                holder.qtyText.setTextColor(fontColor);
                holder.nameText.setTextColor(fontColor);
            }
        }

        if (Configure.itemSingleType.equals("0") && orderMenu.getQty() == 1) {
            holder.qtyText.setVisibility(View.INVISIBLE);
        } else {
            holder.qtyText.setVisibility(View.VISIBLE);
            holder.qtyText.setText(String.valueOf(orderMenu.getQty()));
        }

        String saleType = orderMenu.getSaleType();
        if (saleType == null)
            saleType = " ";

        String name = "";
        if (Configure.takeType != null && Configure.takeType.equals("1")) {
            if (orderMenu.getName() != null)
                name = saleType + " " + orderMenu.getName();
            else
                name = saleType + " " + orderMenu.getItemCode();
        } else {
            if (orderMenu.getName() != null)
                name = orderMenu.getName();
            else
                name = orderMenu.getItemCode();
        }

        holder.nameText.setText(name);

        // 구분선 넣기
        if ((position + 1 < dataList.size() && dataList.get(position + 1).getMenuType() > 2)) {
            holder.divider.setVisibility(View.INVISIBLE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);
        }
    }

    public void setFontColor(int color) {
        fontColor = color;
    }

    @Override
    public int getItemCount() {
        if (dataList != null)
            return dataList.size();
        else
            return 0;
    }

    public void setDataList(List<OrderMenu> dataList) {
        // 카테고리별 순서 정렬
        if (Configure.categoryOrderFl != null && Configure.categoryOrderFl.equals("1")) {
            List<OrderMenu> menuList = new ArrayList<>();
            Boolean[] isInsertList = new Boolean[dataList.size()];
            for (int i = 0; i < isInsertList.length; i++)
                isInsertList[i] = false;

            for (int i = 0; i < Configure.categoryList.size(); i++) {
                ItemGrp category = Configure.categoryList.get(i);
                for (int j = 0; j < dataList.size(); j++) {
                    OrderMenu orderMenu = dataList.get(j);
                    if (category.getCode().equals(orderMenu.getItemGrpCd()) && isInsertList[j] == false) {
                        menuList.add(orderMenu);
                        isInsertList[j] = true;
                        for(int k = j+1; k< dataList.size(); k++) {
                            if(dataList.size() > k && dataList.get(k).getMenuType() == Constants.MENU_TYPE_CONDIMENT) {
                                OrderMenu condimentMenu = dataList.get(k);

                                menuList.add(condimentMenu);
                                isInsertList[k] = true;
                            } else {
                                break;
                            }
                        }

                    }
                }
            }

            for (int i = 0; i < isInsertList.length; i++) {
                if (isInsertList[i] == false) {
                    menuList.add(dataList.get(i));
                }
            }
            this.dataList = menuList;
        } else {
            this.dataList = dataList;
        }
    }
}
