package com.kgict.kds;

public class Constants {

    // KDS 타입
    public static final int KDS_TYPE_COUNTER = 0;
    public static final int KDS_TYPE_KITCHEN = 1;
    public static final int KDS_TYPE_PACKING = 2;

    // 프리퍼런스
    public static final String PREF_KDS_ID = "kdsId";
    public static final String PREF_CONTROLLER_IP = "controllerIp";
    public static final String PREF_OLD_VERSION = "oldVersion";

    public static final int MENU_TYPE_CONDIMENT = 3;

    // 네트워크 상태
    public static final int NETWORK_STATE_FAIL = 0;
    public static final int NETWORK_STATE_SUCCESS = 1;
    public static final int NETWORK_STATE_LOGIC_ERROR = 2;

    // 주문채널
    public static final int DEVICES_TYPE_1 = 0;
    public static final int DEVICES_TYPE_2 = 1;
    public static final int DEVICES_TYPE_3 = 2;
    public static final int DEVICES_TYPE_4 = 3;
    public static final int DEVICES_TYPE_5 = 4;
    public static final int DEVICES_TYPE_6 = 5;
    public static final int DEVICES_TYPE_7 = 6;
    public static final int DEVICES_TYPE_8 = 7;
    public static final int DEVICES_TYPE_9 = 8;
    public static final int DEVICES_TYPE_10 = 9;


    // 범프키 Keycode 매핑
    public static final int KEY_BACK = 4;

    public static final int KEY_NUM_1 = 145;
    public static final int KEY_NUM_2 = 146;
    public static final int KEY_NUM_3 = 147;
    public static final int KEY_NUM_4 = 148;
    public static final int KEY_NUM_5 = 149;
    public static final int KEY_NUM_6 = 150;
    public static final int KEY_NUM_7 = 151;
    public static final int KEY_NUM_8 = 152;
    public static final int KEY_NUM_9 = 153;
    public static final int KEY_NUM_0 = 144;

    public static final int KEY_KEYBOARD_NUM_1 = 8;
    public static final int KEY_KEYBOARD_NUM_2 = 9;
    public static final int KEY_KEYBOARD_NUM_3 = 10;
    public static final int KEY_KEYBOARD_NUM_4 = 11;
    public static final int KEY_KEYBOARD_NUM_5 = 12;
    public static final int KEY_KEYBOARD_NUM_6 = 13;
    public static final int KEY_KEYBOARD_NUM_7 = 14;
    public static final int KEY_KEYBOARD_NUM_8 = 15;
    public static final int KEY_KEYBOARD_NUM_9 = 16;
    public static final int KEY_KEYBOARD_NUM_0 = 7;

    public static final int KEY_DIC_UP = 19;
    public static final int KEY_DIC_LEFT = 21;
    public static final int KEY_DIC_RIGHT = 22;
    public static final int KEY_DIC_DOWN = 20;
    public static final int KEY_DIC_CENTER = 160;

    public static final int KEY_FUNC_F1 = 131;
    public static final int KEY_FUNC_F2 = 132;
    public static final int KEY_FUNC_F3 = 133;
    public static final int KEY_FUNC_F4 = 134;
    public static final int KEY_FUNC_F5 = 135;
    public static final int KEY_FUNC_F6 = 136;

    public static final int DISPLAY_TYPE_BLANK = 1000;
    public static final int DISPLAY_TYPE_ORDER_NORMAL = 1001;
    public static final int DISPLAY_TYPE_ORDER_END = 1002;
    public static final int DISPLAY_TYPE_ORDER_START = 1003;
    public static final int DISPLAY_TYPE_ORDER_CENTER = 1004;
    public static final int DISPLAY_TYPE_SUMMARY = 1005;
    public static final int DISPLAY_TYPE_NONE = 9999;

    //packingType 분기용
    public static final String PACK_BASIC = "PACK_BASIC";
    public static final String PACK_MUCH = "PACK_MUCH";

}
