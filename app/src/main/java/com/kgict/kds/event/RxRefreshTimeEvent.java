package com.kgict.kds.event;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class RxRefreshTimeEvent {
    private static RxRefreshTimeEvent mInstance;
    private BehaviorSubject<Integer> mSubject;

    public RxRefreshTimeEvent() {
        this.mSubject = BehaviorSubject.create();
    }

    public static RxRefreshTimeEvent getInstance() {
        if(mInstance == null) {
            mInstance = new RxRefreshTimeEvent();
        }
        return mInstance;
    }

    public void sendBus(Integer position) {
        mSubject.onNext(position);
    }

    public Observable<Integer> getBus() {
        return mSubject;
    }
}
