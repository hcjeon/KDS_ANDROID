package com.kgict.kds.event;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class RxKeyEvent {

    private static RxKeyEvent mInstance;
    private BehaviorSubject<Integer> mSubject;

    public RxKeyEvent() {
        mSubject = BehaviorSubject.create();
    }

    public static RxKeyEvent getInstance() {
        if(mInstance == null) {
            mInstance = new RxKeyEvent();
        }
        return mInstance;
    }

    public void sendKeyCode(int keyCode) {
        mSubject.onNext(keyCode);
    }

    public Observable<Integer> getBus() {
        return mSubject;
    }
}
