package com.kgict.kds.event;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class RxRefreshEvent {

    private static RxRefreshEvent mInstance;
    private BehaviorSubject<Object> mSubject;

    public RxRefreshEvent() {
        mSubject = BehaviorSubject.create();
    }

    public static RxRefreshEvent getInstance() {
        if(mInstance == null) {
            mInstance = new RxRefreshEvent();
        }
        return mInstance;
    }

    public void sendBus(Object object) {
        mSubject.onNext(object);
    }

    public Observable<Object> getBus() {
        return mSubject;
    }


}
