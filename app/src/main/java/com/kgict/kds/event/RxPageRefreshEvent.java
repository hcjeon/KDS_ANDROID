package com.kgict.kds.event;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class RxPageRefreshEvent {
    private static RxPageRefreshEvent mInstance;
    private BehaviorSubject<Integer> mSubject;

    public RxPageRefreshEvent() {
        this.mSubject = BehaviorSubject.create();
    }

    public static RxPageRefreshEvent getInstance() {
        if(mInstance == null) {
            mInstance = new RxPageRefreshEvent();
        }
        return mInstance;
    }


    public void sendBus(Integer position) {
        mSubject.onNext(position);
    }

    public Observable<Integer> getBus() {
        return mSubject;
    }

}
