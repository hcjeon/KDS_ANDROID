package com.kgict.kds.event;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class RxNetworkStateEvent {

    private static RxNetworkStateEvent mInstance;
    private BehaviorSubject<Integer> mSubject;

    public RxNetworkStateEvent() {
        mSubject = BehaviorSubject.create();
    }

    public static RxNetworkStateEvent getInstance() {
        if(mInstance == null) {
            mInstance = new RxNetworkStateEvent();
        }
        return mInstance;
    }

    public void sendNetworkState(int state) {
        mSubject.onNext(state);
    }

    public Observable<Integer> getBus() {
        return mSubject;
    }
}
