package com.kgict.kds.utils;

import android.app.Activity;
import android.util.TypedValue;

import com.kgict.kds.Configure;
import com.kgict.kds.R;
import com.kgict.kds.object.ItemGrp;

public class Utils {
  private static Activity mActivity;

  public static void setActivity(Activity activity) {
    mActivity = activity;
  }

  public static int dimenToPx(int resourceId) {
    return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mActivity.getResources().getDimensionPixelSize(resourceId), mActivity.getResources().getDisplayMetrics()));
  }

  public static ItemGrp getItemGrp(String code) {
    for(ItemGrp itemGrp : Configure.categoryList) {
      if(itemGrp.getCode().equals(code)) {
        return itemGrp;
      }
    }
    return null;
  }
}
