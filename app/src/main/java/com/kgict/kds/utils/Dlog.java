package com.kgict.kds.utils;

import android.util.Log;

import com.kgict.kds.App;


/**
 * Created by Cheol on 2016-08-19.
 */
public class Dlog {

    static final String TAG = "ICT_KDS";


    /** Log Level Error **/
    public static final void e(String message) {
        if (App.DEBUG) Log.e(TAG, buildLogMsg(message));
    }
    /** Log Level Error **/
    public static final void e(String message, Throwable e) {
        if (App.DEBUG) Log.e(TAG, buildLogMsg(message), e);
    }

    /** Log Level Warning **/
    public static final void w(String message) {
        if (App.DEBUG) Log.w(TAG, buildLogMsg(message));
    }
    /** Log Level Information **/
    public static final void i(String message) {
        if (App.DEBUG) Log.i(TAG, buildLogMsg(message));
    }
    /** Log Level Debug **/
    public static final void d(String message) {
        if (App.DEBUG) Log.d(TAG, buildLogMsg(message));
    }
    /** Log Level Verbose **/
    public static final void v(String message) {
        if (App.DEBUG) Log.v(TAG, buildLogMsg(message));
    }


    public static String buildLogMsg(String message) {

        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        sb.append(ste.getFileName().replace(".java", ""));
        sb.append("::");
        sb.append(ste.getMethodName());
        sb.append("]");
        sb.append(message);

        return sb.toString();

    }

}