package com.kgict.kds;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatButton;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.internal.LinkedTreeMap;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.kgict.kds.network.NetworkService;
import com.kgict.kds.network.UpdateCheckRequest;
import com.kgict.kds.network.UpdateConfirm;
import com.kgict.kds.network.UpdateService;
import com.kgict.kds.network.VersionService;
import com.kgict.kds.object.ItemGrp;
import com.kgict.kds.object.KdsConfigure;
import com.kgict.kds.object.StoreConfigure;
import com.kgict.kds.utils.Dlog;
import com.kgict.kds.utils.Utils;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class SplashActivity extends RxAppCompatActivity {

    public App mApp;

    @BindView(R.id.btKdsId)
    LinearLayout btKdsId;
    @BindView(R.id.inputKdsId)
    TextView inputKdsId;
    @BindView(R.id.textMacAddress)
    TextView textMacAddress;
    @BindView(R.id.btControllerIp)
    LinearLayout btControllerIp;
    @BindView(R.id.inputControllerIp)
    TextView inputControllerIp;
    @BindView(R.id.textVersion)
    TextView textVersion;
    @BindView(R.id.btOk)
    AppCompatButton btOk;
    @BindView(R.id.textInfo)
    TextView textInfo;

    ObjectMapper mapper;

    AlertDialog loadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);
        mApp = (App) getApplication();
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Utils.setActivity(this);

        loadingDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("로딩중")
                .setCancelable(false).build();

        setListener();

        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        Dlog.i("Permission Granted");

                        if (!networkCheck()) {
                            textInfo.setText("인터넷이 연결돼있지 않습니다. 네트워크를 확인해주세요.");
                            fetchData();
                            return;
                        }

//                        Dlog.i(mApp.getPrefs().getAll().toString());
//                        if(mApp.getPrefs().getString(Constants.PREF_OLD_VERSION, "1.0.0").equals(mApp.getVersionName())) {
//                        }

                        startKds();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Toast.makeText(SplashActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(loadingDialog != null){
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    public void startKds() {
        loadingDialog.show();
        String kdsIdString = mApp.prefs.getString(Constants.PREF_KDS_ID, "");

        // Mac Address 주소 값 받아오기
        if (StringUtils.isBlank(Configure.macAddress))
            getMacAddress();

        if (kdsIdString.length() == 0) {
            fetchData();
            loadingDialog.dismiss();
            textInfo.setText("KDS ID를 설정해주세요.\nID는 컴퓨터에서 ControllerIP:8088/Basic에 접속하면 확인할 수 있습니다.");
            return;
        }

        long kdsId = Integer.parseInt(kdsIdString);

        String controllerIp = mApp.prefs.getString(Constants.PREF_CONTROLLER_IP, Configure.BASE_URL);
        if (controllerIp == null || controllerIp.length() == 0) {
            fetchData();
            loadingDialog.dismiss();
            textInfo.setText("Controller IP를 설정해주세요.");
            return;
        }

        Configure.BASE_URL = controllerIp;
        Dlog.i(Configure.BASE_URL);

        mApp.setRetrofit(new Retrofit.Builder()
                .baseUrl("http://" + controllerIp + ":" + Configure.controllerPort + "/")
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                                .client(mApp.createOkHttpClient())
                .build());

        mApp.networkService = mApp.getRetrofit().create(NetworkService.class);

        mApp.setRetrofitGson(new Retrofit.Builder()
                .baseUrl("http://sims.inicis.com:7878/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(mApp.createOkHttpClient())
                .build());

        Dlog.i("(startKds) Update_Confirm_Url : " + mApp.getRetrofitGson().baseUrl());
        mApp.updateService = mApp.getRetrofitGson().create(UpdateService.class);


        mApp.getNetworkService().getKdsConfigure(kdsId)
                .subscribeOn(Schedulers.io())
                .map(kdsConfigure -> {
                    applyKdsConfigure(kdsConfigure);
                    return true;
                })
                .subscribe(kdsConfigure -> {
                    getStoreData(kdsId);
                }, throwable -> {
                    throwable.printStackTrace();
                    getStoreData(kdsId);
                });

        /*
        checkUpdate()
                .subscribeOn(Schedulers.io())
                .flatMap(map -> {   // KDS 설정 가져오기
                    Dlog.i(map.toString());
                    return mApp.getNetworkService().getKdsConfigure(kdsId)
                            .compose(bindToLifecycle())
                            .observeOn(Schedulers.io());
                })
                .map(kdsConfigure -> {  // KDS 설정 적용
                    Dlog.i("kdsConfigure >>>  " + kdsConfigure.toString());
                    // 재부팅 후 부팅 표시 변환
                    if (kdsConfigure.getRebootFl().equals("1")) {
                        mApp.getNetworkService().updateRebootFlag(String.valueOf(kdsConfigure.getKdsId()), "0")
                                .compose(bindToLifecycle())
                                .subscribe();
                    }
                    // 버전이름 업데이트
                    mApp.getNetworkService().updateKdsVersion(String.valueOf(kdsConfigure.getKdsId()), mApp.getVersionName())
                            .compose(bindToLifecycle())
                            .subscribe();
                    return kdsConfigure;
                })
                .map(kdsConfigure -> {
                    applyKdsConfigure(kdsConfigure);
                    return true;
                })
                .flatMap(aBoolean -> {
                    return mApp.getNetworkService().getCategoryList("")
                            .compose(bindToLifecycle())
                            .observeOn(Schedulers.io());
                })
                .map(itemGrps -> {
                    Dlog.i("itemGrp >>> " + itemGrps.toString());
                    if (itemGrps == null)
                        Configure.categoryList = new ArrayList<>();
                    else {
                        Collections.sort(itemGrps, CategoryCompare);
                        Configure.categoryList = itemGrps;
                    }
                    return true;
                })
                .flatMap(aBoolean -> {  // 매장 설정 가져오기
                    return mApp.getNetworkService().getStoreConfigure(1)
                            .compose(bindToLifecycle())
                            .observeOn(Schedulers.io());
                })
                .map(storeConfigure -> {
                    Dlog.i("storeConfigure >>> " + storeConfigure.toString());
                    applyStoreConfigure(storeConfigure);
                    return true;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        onNext -> {
                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }, onError -> {
                            Dlog.i("onError >> " + onError.getMessage());
                            if (onError.getMessage().contains("NeedUpdate>>")) {
                                String fileString = onError.getMessage().substring(12, onError.getMessage().length());
                                JSONObject updateObj = new JSONObject(fileString);
                                showUpdateDialog(updateObj.getString("url"), updateObj.getString("fileName"));
                            } else if (onError.getMessage().contains("failed to connect") || onError.getMessage().contains("Failed to connect")) {
                                textInfo.setText("Controller IP를 확인해주세요.\n" + onError.getMessage());
                                onError.printStackTrace();
                                fetchData();
                                loadingDialog.dismiss();
                            } else if(onError.getMessage().contains("noKDScode")){
                                textInfo.setText("KDS No를 확인해주세요.\n" + onError.getMessage());
                                onError.printStackTrace();
                                fetchData();
                                loadingDialog.dismiss();
                            }else {
                                mApp.showToast(onError.getMessage());
                                textInfo.setText(onError.getMessage());
                                onError.printStackTrace();
                                loadingDialog.dismiss();
                            }
                        }
                );
        */
    }
    public void getStoreData (long kdsId) {
        mApp.setRetrofitGson(new Retrofit.Builder()
                .baseUrl(Configure.UpdateUrl + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(mApp.createOkHttpClient())
                .build());

        mApp.versionService = mApp.getRetrofitGson().create(VersionService.class);

        checkUpdate().subscribeOn(Schedulers.io())
                .subscribe(onNext -> {
                    getNetworkData(kdsId);
                }, onError -> {
                    Dlog.i("onError >> " + onError.getMessage());
                    if (onError.getMessage().contains("NeedUpdate>>")) {
                        String fileString = onError.getMessage().substring(12);
                        JSONObject updateObj = new JSONObject(fileString);
                        showUpdateDialog(updateObj.getString("url"), updateObj.getString("fileName"));
                    } else {
                        getNetworkData(kdsId);
                    }
                });
    }

    public void getNetworkData(long kdsId) {
        mApp.getNetworkService().getKdsConfigure(kdsId)
                .compose(bindToLifecycle())
                .observeOn(Schedulers.io())
                .map(kdsConfigure -> {  // KDS 설정 적용
                    Dlog.i("kdsConfigure >>>  " + kdsConfigure.toString());
                    // 재부팅 후 부팅 표시 변환
                    if (kdsConfigure.getRebootFl().equals("1")) {
                        mApp.getNetworkService().updateRebootFlag(String.valueOf(kdsConfigure.getKdsId()), "0")
                                .compose(bindToLifecycle())
                                .subscribe();
                    }
                    // 버전이름 업데이트
                    mApp.getNetworkService().updateKdsVersion(String.valueOf(kdsConfigure.getKdsId()), mApp.getVersionName())
                            .compose(bindToLifecycle())
                            .subscribe();
                    return kdsConfigure;
                })
                .map(kdsConfigure -> {
                    applyKdsConfigure(kdsConfigure);
                    return true;
                })
                .flatMap(aBoolean -> {
                    return mApp.getNetworkService().getCategoryList("")
                            .compose(bindToLifecycle())
                            .observeOn(Schedulers.io());
                })
                .map(itemGrps -> {
                    Dlog.i("itemGrp >>> " + itemGrps.toString());
                    if (itemGrps == null)
                        Configure.categoryList = new ArrayList<>();
                    else {
                        Collections.sort(itemGrps, CategoryCompare);
                        Configure.categoryList = itemGrps;
                    }
                    return true;
                })
                .flatMap(aBoolean -> {  // 매장 설정 가져오기
                    return mApp.getNetworkService().getStoreConfigure(1)
                            .compose(bindToLifecycle())
                            .observeOn(Schedulers.io());
                })
                .map(storeConfigure -> {
                    Dlog.i("storeConfigure >>> " + storeConfigure.toString());
                    applyStoreConfigure(storeConfigure);
                    return true;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        onNext -> {
                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }, onError -> {
                            Dlog.i("onError >> " + onError.getMessage());
                            if (onError.getMessage().contains("NeedUpdate>>")) {
                                String fileString = onError.getMessage().substring(12, onError.getMessage().length());
                                JSONObject updateObj = new JSONObject(fileString);
                                showUpdateDialog(updateObj.getString("url"), updateObj.getString("fileName"));
                            } else if (onError.getMessage().contains("failed to connect") || onError.getMessage().contains("Failed to connect")) {
                                textInfo.setText("Controller IP를 확인해주세요.\n" + onError.getMessage());
                                onError.printStackTrace();
                                fetchData();
                                loadingDialog.dismiss();
                            } else if(onError.getMessage().contains("noKDScode")){
                                textInfo.setText("KDS No를 확인해주세요.\n" + onError.getMessage());
                                onError.printStackTrace();
                                fetchData();
                                loadingDialog.dismiss();
                            }else {
                                mApp.showToast(onError.getMessage());
                                textInfo.setText(onError.getMessage());
                                onError.printStackTrace();
                                loadingDialog.dismiss();
                            }
                        }
                );
    }

    public void setListener() {
        btKdsId.setOnClickListener(v -> {
            showInputDialog("KDS ID", Constants.PREF_KDS_ID, inputKdsId, InputType.TYPE_CLASS_NUMBER);
        });

        btControllerIp.setOnClickListener(v -> {
            showInputDialog("Controller IP", Constants.PREF_CONTROLLER_IP, inputControllerIp, -1);
        });

        btOk.setOnClickListener(v -> {

            textInfo.setText("");

            // 네트워크 체크
            if (!networkCheck()) {
                textInfo.setText("인터넷이 연결돼있지 않습니다. 네트워크를 확인해주세요.");
                fetchData();
                return;
            }

            startKds();
        });
    }

    /**
     * 화면 데이터 갱신
     */
    public void fetchData() {
        String kdsId = mApp.getPrefs().getString(Constants.PREF_KDS_ID, "");
        inputKdsId.setText(kdsId);

        String controllerIP = mApp.getPrefs().getString(Constants.PREF_CONTROLLER_IP, Configure.BASE_URL);
        inputControllerIp.setText(controllerIP);

        // 맥주소
        WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String address = info.getMacAddress();
        textMacAddress.setText(address);

        // 버전 정보
        textVersion.setText(mApp.getVersionName());
    }

    /**
     * 업데이트 확인
     */
    public Single<Map> checkUpdate() {
        return mApp.versionService.getLastVersion(new UpdateCheckRequest(Configure.solutionCode, Configure.macAddress, mApp.getVersionName(), Configure.brandCode, Configure.storeCode, Configure.storeName))
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .map(s -> {
                    Dlog.i("checkVersion Response >> " + s);
                    List<Object> headArr = (List) MapUtils.getObject(s, "Header", null);
                    LinkedTreeMap headMap = (LinkedTreeMap) headArr.get(0);
                    if (headMap.get("RES_CD").equals("0000")
                            && headMap.get("UPDATE_REQUIRED") != null) {
                        if (headMap.get("UPDATE_REQUIRED").equals("Y")) {
                            Dlog.i("패치 필요");
                            LinkedTreeMap filesMap = (LinkedTreeMap) ((List) MapUtils.getObject(s, "Files", null)).get(0);
                            String downLoadUrl = (String) filesMap.get("URL");
                            String fileName = (String) filesMap.get("FILE_NAME");
                            JSONObject urlParam = new JSONObject();
                            urlParam.put("url", downLoadUrl);
                            urlParam.put("fileName", fileName);
                            Dlog.i("강제 에러 >> " + urlParam);
                            confirmUpdate();
                            throw new Exception("NeedUpdate>>" + urlParam);
                        }
                    } else {
                        Dlog.i("패치 필요없음");
                        mApp.showToast((String) headMap.get("RES_MSG"));
                        throw new Exception("Update Error");
                    }
                    return s;
                });
    }

    public void confirmUpdate(){
        Dlog.i("(confirmUpdate) 진입 ");
        mApp.updateService.VersionConfirm(new UpdateConfirm(Configure.solutionCode, Configure.macAddress, String.valueOf(Configure.kdsType), mApp.getVersionName(), Configure.brandCode, Configure.storeCode, Configure.storeName))
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .map(s -> {
                    Dlog.i("confirmVersion Response >> " + s);
                    List<Object> headArr = (List) MapUtils.getObject(s, "Header", null);
                    LinkedTreeMap headMap = (LinkedTreeMap) headArr.get(0);
                    if (headMap.get("RES_CD").equals("0000")) {
                    } else {
                        mApp.showToast((String) headMap.get("RES_MSG"));
                        throw new Exception("Confirm Error");
                    }

                    Dlog.i("(confirmUpdate)  종료 ");
                    return s;
                });
    }

    /**
     * 설정 적용
     *
     * @param kdsConfigure
     */
    public void applyKdsConfigure(KdsConfigure kdsConfigure) {
        try{
            // 매장용, 주방용, 팩킹용
            String kdsType = kdsConfigure.getKdsClass();

            if (kdsType.equals("1")) {
                Configure.kdsType = Constants.KDS_TYPE_KITCHEN;
            } else if (kdsType.equals("2")) {
                Configure.kdsType = Constants.KDS_TYPE_PACKING;
            } else {
                Configure.kdsType = Constants.KDS_TYPE_COUNTER;
            }

            if (kdsConfigure.getKdsStoreCd().length() > 0)
                Configure.storeCode = kdsConfigure.getKdsStoreCd();
            if (kdsConfigure.getKdsUpdateUrl().length() > 0)
                Configure.UpdateUrl = kdsConfigure.getKdsUpdateUrl();
            if ( kdsConfigure.getBrandCd().length() > 0)
                Configure.brandCode = kdsConfigure.getBrandCd();
            if (kdsConfigure.getKdsStoreNm().length() > 0)
                Configure.storeName = kdsConfigure.getKdsStoreNm();

            if (Configure.kdsType == Constants.KDS_TYPE_PACKING) {
                // 인터페이스 타입
                Configure.interfaceType = "T";

                //packing type
                Configure.PackingType = "T".equals(kdsConfigure.getInterfaceType()) ? Constants.PACK_MUCH : Constants.PACK_BASIC;

            } else {
                // 인터페이스 타입
                Configure.interfaceType = kdsConfigure.getInterfaceType();

                //packing type
                Configure.PackingType = Constants.PACK_BASIC;
            }

            // DID설정
            if (kdsConfigure.getDidFl().equals("1")) {
                Configure.isUseDid = true;
                if(kdsConfigure.getDidIp().length() > 0)
                    Configure.DID_URL = kdsConfigure.getDidIp();
                if(kdsConfigure.getDidPort1().length()>0)
                    Configure.callNumPort = Integer.parseInt(kdsConfigure.getDidPort1());
                if(kdsConfigure.getDidPort1().length()>0)
                    Configure.callNumPort2 = Integer.parseInt(kdsConfigure.getDidPort2());
            }

            // 화면 분할수
            switch (kdsConfigure.getDisplayCnt()) {
                case 10:
                    Configure.orderColumnCount = 5;
                    Configure.orderRowCount = 2;
                    break;
                case 12:
                    Configure.orderColumnCount = 6;
                    Configure.orderRowCount = 2;
                    break;
                case 16:
                    Configure.orderColumnCount = 8;
                    Configure.orderRowCount = 2;
                    break;
                default:
                    Configure.orderColumnCount = 6;
                    Configure.orderRowCount = 2;
                    break;
            }

            // 메뉴 + 수량 순서
            Configure.itemOrderType = kdsConfigure.getItemOrderType();
            // 메뉴 수량 1 표시
            Configure.itemSingleType = kdsConfigure.getSingleItemFl();

            // 메뉴명 폰트 사이즈
            Configure.menuFontSizeType = (int) kdsConfigure.getFontSize();
            switch (Configure.menuFontSizeType) {
                case 0:
                    Configure.menuFontSize = 17;
                    break;
                case 1:
                    Configure.menuFontSize = 19;
                    break;
                case 2:
                    Configure.menuFontSize = 21;
                    break;
                case 3:
                    Configure.menuFontSize = 23;
                    break;
            }

            // 주문채널 배경색
            if (kdsConfigure.getSaleTypeColor1() != null && kdsConfigure.getSaleTypeColor1().length() == 6)
                Configure.saleTypeBackgroundColor1 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor1());
            if (kdsConfigure.getSaleTypeColor2() != null && kdsConfigure.getSaleTypeColor2().length() == 6)
                Configure.saleTypeBackgroundColor2 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor2());
            if (kdsConfigure.getSaleTypeColor3() != null && kdsConfigure.getSaleTypeColor3().length() == 6)
                Configure.deliveryBackgroundColor = Color.parseColor("#" + kdsConfigure.getSaleTypeColor3());
            if (kdsConfigure.getSaleTypeColor4() != null && kdsConfigure.getSaleTypeColor4().length() == 6)
                Configure.saleTypeBackgroundColor4 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor4());
            if (kdsConfigure.getSaleTypeColor5() != null && kdsConfigure.getSaleTypeColor5().length() == 6)
                Configure.saleTypeBackgroundColor5 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor5());
            if (kdsConfigure.getSaleTypeColor6() != null && kdsConfigure.getSaleTypeColor6().length() == 6)
                Configure.saleTypeBackgroundColor6 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor6());
            if (kdsConfigure.getSaleTypeColor7() != null && kdsConfigure.getSaleTypeColor7().length() == 6)
                Configure.saleTypeBackgroundColor7 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor7());
            if (kdsConfigure.getSaleTypeColor8() != null && kdsConfigure.getSaleTypeColor8().length() == 6)
                Configure.saleTypeBackgroundColor8 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor8());
            if (kdsConfigure.getSaleTypeColor9() != null && kdsConfigure.getSaleTypeColor9().length() == 6)
                Configure.saleTypeBackgroundColor9 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor9());
            if (kdsConfigure.getSaleTypeColor10() != null && kdsConfigure.getSaleTypeColor10().length() == 6)
                Configure.saleTypeBackgroundColor10 = Color.parseColor("#" + kdsConfigure.getSaleTypeColor10());

            // 메뉴명색깔
            String fontColor = kdsConfigure.getFontColor();
            Configure.menuFontColor = Color.parseColor("#" + fontColor);

            // 복원색깔
            String rvBackgroundColor = kdsConfigure.getRvColor();
            Configure.revertBackgroundColor = Color.parseColor("#" + rvBackgroundColor);

            // 취소색깔
            String cancelBackgroundColor = kdsConfigure.getCancelColor();
            Configure.cancelBackgroundColor = Color.parseColor("#" + cancelBackgroundColor);

            // 메뉴별 포장 표시
            Configure.takeType = kdsConfigure.getTakeType();

            // 지연시간 및 색깔 가져오기
            Configure.delayType = kdsConfigure.getDelayType();
            Configure.delaySecond1 = Integer.valueOf(kdsConfigure.getDelaySec1());
            Configure.delayColor1 = Color.parseColor("#" + kdsConfigure.getDelayColor1());
            Configure.delayColorBg1 = Color.parseColor("#50" + kdsConfigure.getDelayColor1());
            Configure.delaySecond2 = Integer.valueOf(kdsConfigure.getDelaySec2());
            Configure.delayColor2 = Color.parseColor("#" + kdsConfigure.getDelayColor2());
            Configure.delayColorBg2 = Color.parseColor("#50" + kdsConfigure.getDelayColor2());
            Configure.delaySecond3 = Integer.valueOf(kdsConfigure.getDelaySec3());
            Configure.delayColor3 = Color.parseColor("#" + kdsConfigure.getDelayColor3());
            Configure.delayColorBg3 = Color.parseColor("#50" + kdsConfigure.getDelayColor3());
            Configure.delaySecond4 = Integer.valueOf(kdsConfigure.getDelaySec4());
            Configure.delayColor4 = Color.parseColor("#" + kdsConfigure.getDelayColor4());
            Configure.delayColorBg4 = Color.parseColor("#50" + kdsConfigure.getDelayColor4());

            // 썸머리 표기 방식
            Configure.summaryType = String.valueOf(kdsConfigure.getUseSummaryFl());

            // 썸머리 메뉴 표기 방식
            Configure.summaryMenuType = kdsConfigure.getSummaryMenuType();

            // 주문번호 표시
            Configure.useOrderNum = kdsConfigure.getUseOrderNum();

            // 주방 프린터 출력 여부
            Configure.useKitchenPrint = kdsConfigure.getUseKitchenPrint();

            // 카테고리 정렬 적용 여부
            Configure.categoryOrderFl = kdsConfigure.getCategoryOrderFl();
        }
        catch (Exception ex){

        }
    }

    public void applyStoreConfigure(StoreConfigure storeConfigure) {
        try{
            Configure.deliveryTitle = storeConfigure.getSaleTypeNm3();

            Configure.storeName = storeConfigure.getStoreNm();
            if (storeConfigure.getSaleTypeFl1() != null && storeConfigure.getSaleTypeFl1().equals("1"))
                Configure.saleTypeTitle1 = storeConfigure.getSaleTypeNm1();
            if (storeConfigure.getSaleTypeFl2() != null && storeConfigure.getSaleTypeFl2().equals("1"))
                Configure.saleTypeTitle2 = storeConfigure.getSaleTypeNm2();
            if (storeConfigure.getSaleTypeFl4() != null && storeConfigure.getSaleTypeFl4().equals("1"))
                Configure.saleTypeTitle4 = storeConfigure.getSaleTypeNm4();
            if (storeConfigure.getSaleTypeFl5() != null && storeConfigure.getSaleTypeFl5().equals("1"))
                Configure.saleTypeTitle5 = storeConfigure.getSaleTypeNm5();
            if (storeConfigure.getSaleTypeFl6() != null && storeConfigure.getSaleTypeFl6().equals("1"))
                Configure.saleTypeTitle6 = storeConfigure.getSaleTypeNm6();
            if (storeConfigure.getSaleTypeFl7() != null && storeConfigure.getSaleTypeFl7().equals("1"))
                Configure.saleTypeTitle7 = storeConfigure.getSaleTypeNm7();
            if (storeConfigure.getSaleTypeFl8() != null && storeConfigure.getSaleTypeFl8().equals("1"))
                Configure.saleTypeTitle8 = storeConfigure.getSaleTypeNm8();
            if (storeConfigure.getSaleTypeFl9() != null && storeConfigure.getSaleTypeFl9().equals("1"))
                Configure.saleTypeTitle9 = storeConfigure.getSaleTypeNm9();
            if (storeConfigure.getSaleTypeFl10() != null && storeConfigure.getSaleTypeFl10().equals("1"))
                Configure.saleTypeTitle10 = storeConfigure.getSaleTypeNm10();

            Configure.interval = storeConfigure.getKdsRefreshCycle();
            Configure.summaryTitle = storeConfigure.getSummaryTitle();
            Configure.logoFl = storeConfigure.getLogoFl();

            //딜리버리 사용 여부
            Configure.deliveryFl = storeConfigure.getDeliveryFl();
            //딜리버리 대행 사용 여부
            Configure.deliveryAgencyFl = storeConfigure.getDeliveryAgencyFl();
        }
        catch (Exception ex){

        }
    }

    /**
     * 업데이트 다이얼로그
     */
    public void showUpdateDialog(String url, String pFileName) {

        /*
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("KDS APK 업데이트");
        builder.setMessage("최신 버전이 있습니다. 업데이트를 하시겠습니까?");
        builder.setCancelable(false);
        builder.setPositiveButton("업데이트", (dialog, which) -> {
            // 업데이트시 로직
            // 1. 사이트에 접속
            String filePath = url;
            Dlog.i("업데이트 파일 Path >> " + filePath);

            mApp.getNetworkStreamService().downloadFileWithProgress(filePath)
                    .compose(bindToLifecycle())
                    .subscribeOn(Schedulers.io())
                    .flatMap(responseBody -> {
                        // 파일 쓰기
                        Dlog.i("contentLength >> " + responseBody.contentLength());
                        Dlog.i("contentType >> " + responseBody.contentType());

                        String fileName = FilenameUtils.getName(pFileName);
                        Dlog.i("fileName >> " + fileName);

                        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile(), fileName);

                        BufferedSink sink = Okio.buffer(Okio.sink(file));
                        sink.writeAll(responseBody.source());
                        sink.close();

                        return Flowable.just(file);
                    })
                    .subscribe(file -> {
                        // 완료 단계
                        //다운로드 받은 패키지를 인스톨한다.
                        Dlog.i("file >>" + file.getAbsolutePath());
                        Intent intent = new Intent(Intent.ACTION_VIEW);

                        if (Build.VERSION.SDK_INT >= 24) {
                            // Android Nougat ( 7.0 ) and later
                            installApk(file);
                            System.out.println("SDK_INT 24 이상 ");
                        } else {
                            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            moveTaskToBack(true);
                            finish();
                            android.os.Process.sendSignal(android.os.Process.myPid(), android.os.Process.SIGNAL_KILL);
                        }
                    });
        });
        builder.show();
        */

        try{
            mApp.getNetworkStreamService().downloadFileWithProgress(url)
                    .compose(bindToLifecycle())
                    .subscribeOn(Schedulers.io())
                    .flatMap(responseBody -> {
                        // 파일 쓰기
                        Dlog.i("contentLength >> " + responseBody.contentLength());
                        Dlog.i("contentType >> " + responseBody.contentType());

                        String fileName = FilenameUtils.getName(pFileName);
                        Dlog.i("fileName >> " + fileName);

                        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile(), fileName);

                        BufferedSink sink = Okio.buffer(Okio.sink(file));
                        sink.writeAll(responseBody.source());
                        sink.close();

                        return Flowable.just(file);
                    })
                    .subscribe(file -> {
                        // 완료 단계
                        //다운로드 받은 패키지를 인스톨한다.
                        Dlog.i("file >>" + file.getAbsolutePath());
                        Intent intent = new Intent(Intent.ACTION_VIEW);

                        mApp.getPrefEditor().putString(Constants.PREF_OLD_VERSION, mApp.getVersionName());
                        mApp.getPrefEditor().commit();

                        confirmUpdate();

                        if (Build.VERSION.SDK_INT >= 24) {
                            // Android Nougat ( 7.0 ) and later
                            installApk(file);
                            System.out.println("SDK_INT 24 이상 ");
                        } else {
                            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            moveTaskToBack(true);
                            finish();
                            android.os.Process.sendSignal(android.os.Process.myPid(), android.os.Process.SIGNAL_KILL);
                        }
                    });
        }
        catch (Exception ex){

        }
    }

    public void installApk(File file) {
        Uri uri = FileProvider.getUriForFile(getBaseContext(), BuildConfig.APPLICATION_ID + ".fileprovider", file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        getBaseContext().startActivity(intent);
    }

    /**
     * 입력 다이얼로그
     *
     * @param title
     * @param prefKey
     * @param textView
     */
    public void showInputDialog(String title, String prefKey, TextView textView, int inputType) {
        try{
            final EditText edittext = new EditText(this);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title);
            if (inputType > 0)
                edittext.setInputType(inputType);
            edittext.setPadding(Utils.dimenToPx(R.dimen._5sdp), 0, 0, Utils.dimenToPx(R.dimen._2sdp));
            edittext.setText(textView.getText().toString());
            edittext.setSelection(edittext.length());
            edittext.setSingleLine();
            builder.setView(edittext);
            builder.setPositiveButton("확인",
                    (dialog, which) -> {
                        textView.setText(edittext.getText().toString());
                        mApp.getPrefEditor().putString(prefKey, edittext.getText().toString());
                        mApp.getPrefEditor().commit();
                    });
            builder.setNegativeButton("취소",
                    (dialog, which) -> {

                    });
            builder.show();
        }
        catch (Exception ex){

        }
    }

    /**
     * 네트워크 체크
     */
    public boolean networkCheck() {
        try{
            ConnectivityManager cm =
                    (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            if (!isConnected) {
                return false;
            }
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    Comparator<ItemGrp> CategoryCompare = (o1, o2) -> {
        if (o1.getOrder() < o2.getOrder())
            return -1;
        else if (o1.getOrder() > o2.getOrder())
            return 1;
        return 0;
    };

    public void getMacAddress(){
        try{
            WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            String address = info.getMacAddress();
            textMacAddress.setText(address);
            Configure.macAddress = address;
        }
        catch (Exception ex){
        }
    }

    // Log File Create
    public void LogCreate(String str){
        String str_Path_Full = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KDS_LOG.txt";
        File file = new File(str_Path_Full);
        if(file.exists() == false){
            try {
                file.createNewFile();
            }catch (IOException e){

            }
        }
        try{
            BufferedWriter bfw = new BufferedWriter(new FileWriter(str_Path_Full, true));
            bfw.write(str);
            bfw.write("\n");
            bfw.flush();
            bfw.close();
        }
        catch (FileNotFoundException e){
        } catch (IOException e){
        }
    }
}
