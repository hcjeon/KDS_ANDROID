package com.kgict.kds;

import com.kgict.kds.object.ItemGrp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Configure {

//    public static int kdsType = Constants.KDS_TYPE_COUNTER;     // 0:카운터 1:주방 2:팩킹
//    public static int kdsType = Constants.KDS_TYPE_KITCHEN;
  public static int kdsType = Constants.KDS_TYPE_PACKING;

  public static String brandCode = "KFC";
  public static String storeName = "KFC매장";
  public static String storeCode = "1410999";
  public static String UpdateUrl = "http://update.inicis.com:5573";
  public static String macAddress;

  public static List<ItemGrp> categoryList;

//    public static String BASE_URL = "192.168.1.61";  // 내컴퓨터
//    public static String BASE_URL = "http://61.250.198.140:8080";   // KG
    public static String BASE_URL = "192.168.100.111";  // KFC

    public static String controllerPort = "8080";

//    public static String callNumIp = "61.250.198.140";  // KG
    public static boolean isUseDid = false;
    public static String DID_URL = "192.168.100.111";  // KFC
    public static int callNumPort = 7000;
    public static int callNumPort2 = 7979;

    public static boolean isDummy = false;

    public static int interval = 1500;
//    public static int interval = 5000;

    public static String solutionCode = "KFC_KDS";
    public static String saleYmd = "1999-01-01";
    public static String summaryTitle = "SUMMARY";

    public static int keyInterval = 2000;
    public static int keyInterval2 = 3000;

    public static String logoFl = "0";

    public static int orderColumnCount = 6;      // 한 행의 주문 개수
    public static int orderRowCount = 2;         // 한 열의 주문 개수
    public static int menuMaximumCount = 9;       // 한주문당 최대메뉴개수
    public static String summaryType = "1";     // Summary 표시 여부
    public static String useOrderNum = "0";     // 주문번호 표시
    public static String useKitchenPrint = "0";     // 주방출력 표시
    public static String summaryMenuType = "0";     // Summary 표시 여부
    public static String categoryOrderFl = "0";     // 카테고리 정렬 사용 여부 0:미사용 1:사용
    public static String takeType = "0";       // 메뉴별 포장 표시 0: 미사용 1:사용
    public static int deliveryFl = 0; // 딜리버리 사용여부, 1인 경우에만 패킹KDS에 주문 정보 표시
    public static int deliveryAgencyFl = 0; // 딜리버리 대행 사용 여부 1인 경우에만 요청/픽업 버튼 활성화

    public static String backgroundColor = "FF000000";       // 밑바닥 배경색

    public static String itemOrderType = "1";       // 0:수량+메뉴 1:메뉴+수량
    public static String itemSingleType = "1";       // 0:표시안함 1:표시함
    public static String interfaceType = "B";       // B:범프키 T:터치, PACKING용일경우 B:스크롤좌우, T:스크롤상하

    public static int deliveryBackgroundColor = 0xffee851c;    // 배달 글씨배경 색깔
    public static String deliveryTitle = "배달";    // 배달 글씨배경 색깔
    public static int saleTypeBackgroundColor1 = 0xff376ddb;    // 판매형태1 배경(포스)
    public static String saleTypeTitle1 = "POS";            // 판매형태1 이름(포스)
    public static int saleTypeBackgroundColor2 = 0xff308143;    // 키오스크메뉴의 배경색
    public static String saleTypeTitle2 = "KIOSK";    // 판매형태2의 이름(키오스크)
    public static int saleTypeBackgroundColor4 = 0xff008da4;    // 판매형태4의 배경(드라이브쓰루)
    public static String saleTypeTitle4 = "DT";    // 판매형태4의 이름(드라이브쓰루)
    public static int saleTypeBackgroundColor5 = 0xff8b32d8;    // 모바일(징거벨)메뉴의 배경색
    public static String saleTypeTitle5 = "모바일";    // 판매형태4의 이름(드라이브쓰루)
    public static int saleTypeBackgroundColor6 = 0xffffffff;    // 판매형태6의 배경색
    public static String saleTypeTitle6 = "";    // 판매형태6의 이름
    public static int saleTypeBackgroundColor7 = 0xffffffff;
    public static String saleTypeTitle7 = "";
    public static int saleTypeBackgroundColor8 = 0xffffffff;
    public static String saleTypeTitle8 = "";
    public static int saleTypeBackgroundColor9 = 0xffffffff;
    public static String saleTypeTitle9 = "";
    public static int saleTypeBackgroundColor10 = 0xffffffff;
    public static String saleTypeTitle10 = "";
    public static int cancelBackgroundColor = 0xfffb4646;    // 취소메뉴의 배경색
    public static int revertBackgroundColor = 0xfff7b3f8;    // 복원메뉴의 배경색

    public static int menuFontSizeType = 7;       // 메뉴글씨 크기
    public static float menuFontSize = 7;       // 메뉴글씨 수치
    public static int menuFontColor = 0xffffffff;       // 메뉴글씨 색깔

    public static int takeTextBackgroundColor = 0xff68de25;    // 포장 글씨배경 색깔

    public static int driveTextBackgroundColor = 0xffff3910;    // 드라이브쓰루 글씨배경 색깔

    public static String delayType = "0";       // 0: 텍스트 1:전체
    public static int delaySecond1 = 1 * 60;            // 지연 알람
    public static int delayColor1 = 0xffffff00;
    public static int delayColorBg1 = 0x17ffff00;
    public static int delaySecond2 = 2 * 60;
    public static int delayColor2 = 0xff078f00;
    public static int delayColorBg2 = 0x17078f00;
    public static int delaySecond3 = 4 * 60;
    public static int delayColor3 = 0xffff7676;
    public static int delayColorBg3 = 0x17ff7676;
    public static int delaySecond4 = 8 * 60;
    public static int delayColor4 = 0xffff0000;
    public static int delayColorBg4 = 0x17ff0000;

    //packingType 분기용
    public static String PackingType = Constants.PACK_BASIC;

    public static int getOrderMaxAmount() {
        if (summaryType.equals("2"))
            return orderColumnCount * orderRowCount - 1;
        else
            return orderColumnCount * orderRowCount;
    }
}
